## Opencenter集成Socket Log功能说明

本版本已经集成了Slog的功能，详细配置请查看原作者的说明文档

SocketLog适合Ajax调试和API调试， 用SocketLog来做微信和API的调试。

## 使用方法
 * 首先，请在chrome浏览器上安装好插件。
 * 安装服务端`npm install -g socketlog-server` , 运行命令 `socketlog-server` 即可启动服务。 将会在本地起一个websocket服务 ，监听端口是1229 。 如果想服务后台运行： `socketlog-server > /dev/null &` 我们提供公用的服务端，需要去申请client_id : http://slog.thinkphp.cn/
 * 如果你的服务器有防火墙，请开启`1229`和`1116`两个端口，这两个端口是socketlog要使用的。
 * 用slog函数发送日志， 支持多种日志类型：

        slog('msg','log');  //一般日志
        slog('msg','error'); //错误日志
        slog('msg','info'); //信息日志
        slog('msg','warn'); //警告日志
        slog('msg','trace');// 输入日志同时会打出调用栈
        slog('msg','alert');//将日志以alert方式弹出
        slog('msg','log','color:red;font-size:20px;');//自定义日志的样式，第三个参数为css样式

## 配置
   请阅读程序的入口文件`index.php,api.php,admin.php`的最开始的几行代码
   ThinkPHP `Db.class.php`文件的`debug`方法已经被修改，在调试模式下会输出SQL语句
   
~~~~ php  
define('SOCKET_LOG', true);//启用slog，sdebug函数,主要用于兼容代码，因为代码中如果引用了slog函数，但是没有包含slog.function.php文件会出错
include './slog.function.php';
slog(array(
    'enable' => true,//是否打印日志的开关
    'host' => 'localhost',//websocket服务器地址，默认localhost
    'port' => '1229',//websocket服务器端口，默认端口是1229
    'error_handler' => true,
//    'optimize' => true,
//    'show_included_files' => true,
    //调试微信和API必须设置 force_client_ids，否则浏览器收不到监听日志
    'force_client_ids' => array('zengqs_760522'),//日志强制记录到配置的client_id,可以发送到多个用户,默认为空
    //    'allow_client_ids'=>array() //限制允许读取日志的client_id，默认为空,表示所有人都可以获得日志。
), 'config');
~~~~
