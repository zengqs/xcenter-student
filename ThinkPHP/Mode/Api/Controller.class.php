<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK IT ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006-2013 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: liu21st <liu21st@gmail.com>
// +----------------------------------------------------------------------
namespace Think;
use Com\ErrCode;
use Com\TPWechat;

/**
 * ThinkPHP 控制器基类 抽象类
 */
abstract class Controller {

   /**
     * 架构函数
     * @access public
     */
    public function __construct() {
        //控制器初始化
        if(method_exists($this,'_initialize'))
            $this->_initialize();
    }

    /**
     * 魔术方法 有不存在的操作的时候执行
     * @access public
     * @param string $method 方法名
     * @param array $args 参数
     * @return mixed
     */
    public function __call($method,$args) {
        if( 0 === strcasecmp($method,ACTION_NAME.C('ACTION_SUFFIX'))) {
            if(method_exists($this,'_empty')) {
                // 如果定义了_empty操作 则调用
                $this->_empty($method,$args);
            }else{
                E(L('_ERROR_ACTION_').':'.ACTION_NAME,815);
            }
        }else{
            E(__CLASS__.':'.$method.L('_METHOD_NOT_EXIST_'));
            return;
        }
    }

    function login()
    {

        $params['mp_id'] = $map['mp_id'] = get_mpid();
        $this->assign('mp_id', $params['mp_id']);

        $mid = get_ucuser_mid();   //获取粉丝用户mid，一个神奇的函数，没初始化过就初始化一个粉丝
        if ($mid === false) {
            $this->error('只可在微信中访问');
        }
        $user = get_mid_ucuser($mid);                    //获取公众号粉丝用户信息
        $this->assign('user', $user);

        $url = 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
        sdebug($url, '授权的URL');

//        $surl = get_shareurl();
//        if (!empty($surl)) {
//            $this->assign('share_url', $surl);
//        }

        $appinfo = get_mpid_appinfo($params ['mp_id']);   //获取公众号信息
        $this->assign('appinfo', $appinfo);

        $options['appid'] = $appinfo['appid'];    //初始化options信息
        $options['appsecret'] = $appinfo['secret'];
        $options['encodingaeskey'] = $appinfo['encodingaeskey'];
        $weObj = new TPWechat($options);

        $auth = $weObj->checkAuth();


        $js_ticket = $weObj->getJsTicket();

        sdebug($js_ticket, '$js_ticket');

        if (!$js_ticket) {
            $this->error('获取js_ticket失败！错误码：' . $weObj->errCode . ' 错误原因：' . ErrCode::getErrText($weObj->errCode));
        }
        $js_sign = $weObj->getJsSign($url);
        sdebug($js_sign, '$js_sign');


        $this->assign('js_sign', $js_sign); //用于WEB方式调用JSSDK功能

        if (IS_POST) {

            $aMobile = I('post.mobile', '', 'op_t');
            $aPassword = I('post.password', '', 'op_t');
            $aRemember = I('post.remember', 0, 'intval');

            sdebug($_POST, '$_POST');

            //微信端登录时检测是否在pc端有帐号，如果有pc端帐号，并且微信端密码和pc端不同，就把pc端密码复制给微信端帐号，以支持pc端注册的帐号直接在微信端登录
            $umap['mobile'] = $aMobile;
            $member = UCenterMember()->where($umap)->find();

            sdebug($member, '查找微信用户');

            if (empty ($member)) {                                 //在pc端没注册，注册一个pc端帐号

            } else {                                                     //已经通过网站注册过帐号
                if ($member['password'] != $user['password']) {
                    $data['mid'] = $mid;
                    $data['uid'] = $member['id'];                            //将UCenterMember表的id写入ucuser表uid字段
                    $data['mobile'] = $aMobile;
                    $data['password'] = $member['password'];              //同步加密后的密码
                    $ucuser = M('ucuser');
                    $ucuser->save($data);
                }
            }

            $ucuser = D('Ucuser/Ucuser');
            $res = $ucuser->login($mid, $aMobile, $aPassword, $aRemember);
            if ($res > 0) {
                $this->success('登录成功', U('Mobile/Index/index'));
            } else {
                $this->error($ucuser->getError());
            }
        } else { //显示登录页面
            $this->display();
        }
    }

    /**
     * Ajax方式返回数据到客户端
     * @access protected
     * @param mixed $data 要返回的数据
     * @param String $type AJAX返回数据格式
     * @return void
     */
    protected function ajaxReturn($data,$type='') {
        if(empty($type)) $type  =   C('DEFAULT_AJAX_RETURN');
        switch (strtoupper($type)){
            case 'JSON' :
                // 返回JSON数据格式到客户端 包含状态信息
                header('Content-Type:application/json; charset=utf-8');
                exit(json_encode($data));
            case 'XML'  :
                // 返回xml格式数据
                header('Content-Type:text/xml; charset=utf-8');
                exit(xml_encode($data));
            case 'JSONP':
                // 返回JSON数据格式到客户端 包含状态信息
                header('Content-Type:application/json; charset=utf-8');
                $handler  =   isset($_GET[C('VAR_JSONP_HANDLER')]) ? $_GET[C('VAR_JSONP_HANDLER')] : C('DEFAULT_JSONP_HANDLER');
                exit($handler.'('.json_encode($data).');');
            case 'EVAL' :
                // 返回可执行的js脚本
                header('Content-Type:text/html; charset=utf-8');
                exit($data);
        }
    }

    /**
     * Action跳转(URL重定向） 支持指定模块和延时跳转
     * @access protected
     * @param string $url 跳转的URL表达式
     * @param array $params 其它URL参数
     * @param integer $delay 延时跳转的时间 单位为秒
     * @param string $msg 跳转提示信息
     * @return void
     */
    protected function redirect($url,$params=array(),$delay=0,$msg='') {
        $url    =   U($url,$params);
        redirect($url,$delay,$msg);
    }

}
