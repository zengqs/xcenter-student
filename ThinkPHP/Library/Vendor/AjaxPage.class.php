<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK IT ]
// +----------------------------------------------------------------------
// | Copyright (c) 2009 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: liu21st <liu21st@gmail.com>
// +----------------------------------------------------------------------
// $Id: Page.class.php 2712 2012-02-06 10:12:49Z liu21st $
namespace Vendor;
class AjaxPage
{
    // 分页栏每页显示的页数
    public $rollPage = 5;
    // 页数跳转时要带的参数
    public $parameter;
    // 默认列表每页显示行数
    public $listRows = 20;
    // 起始行数
    public $firstRow;
    // 分页总页面数
    public $totalPages;
    // 总行数
    public $totalRows;
    // 当前页数
    public $nowPage;
    // 分页的栏的总页数
    public $coolPages;
    // 分页显示定制  %nextPage% 下*页   %downPage% 上*页  %nowPage%/%totalPage% 页  %first% 第一页 %upPage% 上一页  %linkPage% 中间页数 %downPage% 下一页 %end% 最后一页 %totalRow%  %header% 总条数
    protected $config = array('first' => '第一页', 'prev' => '上一页', 'next' => '下一页', 'last' => '最后一页', 'header' => '条记录', 'theme' => ' %nowPage%/%totalPage% 页 %first% %upPage% %linkPage% %downPage% %end% &nbsp 共 %totalRow% %header%');
    // 默认分页变量名
    protected $varPage;


    /**
     * 曾青松 修改支持自定义的分页变量
     * AjaxPage constructor.
     * @param $totalRows
     * @param string $listRows
     * @param $ajax_func 客户端js 分页函数，如 var page=function(p) {...}
     * @param string $parameter
     * @param string $varPage  分页变量
     */
    public function __construct($totalRows, $listRows = '', $ajax_func='get_page', $parameter = '' ,$varPage= '')
    {
        $this->totalRows = $totalRows;
        $this->ajax_func = $ajax_func;
        $this->parameter = $parameter;
//        $this->varPage = C('VAR_PAGE') ? C('VAR_PAGE') : 'p';
        if (!empty($varPage)) {
            $this->varPage =$varPage;
        }else{
            $this->varPage = C('VAR_PAGE') ? C('VAR_PAGE') : 'p';
        }
        if (!empty($listRows)) {
            $this->listRows = intval($listRows);
        }
        $this->totalPages = ceil($this->totalRows / $this->listRows);     //总页数
        $this->coolPages = ceil($this->totalPages / $this->rollPage);
        $this->nowPage = !empty($_GET[$this->varPage]) ? intval($_GET[$this->varPage]) : 1;
        if (!empty($this->totalPages) && $this->nowPage > $this->totalPages) {
            $this->nowPage = $this->totalPages;
        }
        $this->firstRow = $this->listRows * ($this->nowPage - 1);
    }

    public function setConfig($name, $value)
    {
        if (isset($this->config[$name])) {
            $this->config[$name] = $value;
        }
    }


    public function show()
    {
        if (0 == $this->totalRows) return '';
        $p = $this->varPage;
        $nowCoolPage = ceil($this->nowPage / $this->rollPage);
        $url = $_SERVER['REQUEST_URI'] . (strpos($_SERVER['REQUEST_URI'], '?') ? '' : "?") . $this->parameter;
        $parse = parse_url($url);
        if (isset($parse['query'])) {
            parse_str($parse['query'], $params);
            unset($params[$p]);
            $url = $parse['path'] . '?' . http_build_query($params);
        }
        //上下翻页字符串
        $upRow = $this->nowPage - 1;
        $downRow = $this->nowPage + 1;
        if ($upRow > 0) {
            $upPage = "<a id='big' href='javascript:" . $this->ajax_func . "(" . $upRow . ")'>" . $this->config['prev'] . "</a>";
        } else {
            $upPage = "";
        }

        if ($downRow <= $this->totalPages) {
            $downPage = "<a id='big' href='javascript:" . $this->ajax_func . "(" . $downRow . ")'>" . $this->config['next'] . "</a>";
        } else {
            $downPage = "";
        }
        // << < > >>
        if ($nowCoolPage == 1) {
            $theFirst = "";
            $prePage = "";
        } else {
            $preRow = $this->nowPage - $this->rollPage;
            $prePage = "<a id='big' href='javascript:" . $this->ajax_func . "(" . $preRow . ")'>上" . $this->rollPage . "页</a>";
            $theFirst = "<a id='big' href='javascript:" . $this->ajax_func . "(1)' >" . $this->config['first'] . "</a>";
        }
        if ($nowCoolPage == $this->coolPages) {
            $nextPage = "";
            $theEnd = "";
        } else {
            $nextRow = $this->nowPage + $this->rollPage;
            $theEndRow = $this->totalPages;
            $nextPage = "<a id='big' href='javascript:" . $this->ajax_func . "(" . $nextRow . ")' >下" . $this->rollPage . "页</a>";
            $theEnd = "<a id='big' href='javascript:" . $this->ajax_func . "(" . $theEndRow . ")' >" . $this->config['last'] . "</a>";
        }
        // 1 2 3 4 5
        $linkPage = "";
        for ($i = 1; $i <= $this->rollPage; $i++) {
            $page = ($nowCoolPage - 1) * $this->rollPage + $i;
            if ($page != $this->nowPage) {
                if ($page <= $this->totalPages) {
                    $linkPage .= "&nbsp;<a id='big' href='javascript:" . $this->ajax_func . "(" . $page . ")'>&nbsp;" . $page . "&nbsp;</a>";
                } else {
                    break;
                }
            } else {
                if ($this->totalPages != 1) {
                    $linkPage .= "&nbsp;<span class='current'>" . $page . "</span>";
                }
            }
        }
        $pageStr = str_replace(
            array('%header%', '%nowPage%', '%totalRow%', '%totalPage%', '%upPage%', '%downPage%', '%first%', '%prePage%', '%linkPage%', '%nextPage%', '%end%'),
            array($this->config['header'], $this->nowPage, $this->totalRows, $this->totalPages, $upPage, $downPage, $theFirst, $prePage, $linkPage, $nextPage, $theEnd), $this->config['theme']);
        return $pageStr;
    }

}

?>
