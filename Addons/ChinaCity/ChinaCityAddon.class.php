<?php
// +----------------------------------------------------------------------
// | i友街 [ 新生代贵州网购社区 ]
// +----------------------------------------------------------------------
// | Copyright (c) 2014 http://www.iyo9.com All rights reserved.
// +----------------------------------------------------------------------
// | Author: i友街 <iyo9@iyo9.com> <http://www.iyo9.com>
// +----------------------------------------------------------------------
//
namespace Addons\ChinaCity;

use Common\Controller\Addon;

/**
 * 中国省市区三级联动插件
 *
 * @author i友街
 */
class ChinaCityAddon extends Addon
{

    public $info = array(
        'name'        => 'ChinaCity',
        'title'       => '中国省市区三级联动',
        'description' => '中国省市区三级联动插件。',
        'status'      => 1,
        'author'      => 'i友街',
        'version'     => '2.0',
    );

    /**
     * 安装数据库表及初始数据
     *
     * @return bool
     */
    public function install()
    {
        /* 先判断插件需要的钩子是否存在 */
        $this->initHook('J_China_City', $this->info['description']);

        $install_sql = './Addons/ChinaCity/install.sql';
        if (file_exists($install_sql)) {
            execute_sql_file($install_sql);
        }

        return true;
    }

    /**
     * 卸载时删除数据库表
     *
     * @return bool
     */
    public function uninstall()
    {
        //卸载钩子
        $uninstall_sql = './Addons/ChinaCity/uninstall.sql';
        if (file_exists($uninstall_sql)) {
            execute_sql_file($uninstall_sql);
        }

        return true;
    }


    //实现的J_China_City钩子方法
    public function J_China_City($param)
    {
        //修改;曾青松，增加支持中显示2级地址
        $param['show_level'] = is_null($param['show_level']) ? 3 : intval($param['show_level'], 3);

        $this->assign('param', $param);
        $this->display('chinacity');
    }
}
