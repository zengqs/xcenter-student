### 中国城市三级数据联动

升级说明

增加选择联动级别，支持1-3级

---

**用法**

     默认显示三级数据，并输入初始数据进行初始化
     {:hook('J_China_City',array('province'=>$data['province'],'city'=>$data['city'],'district'=>$data['district']))}
  
    只显示省和市两级 
    {:hook('J_China_City',array('show_level'=>'2','province'=>$data['province'],'city'=>$data['city']))}