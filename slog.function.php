<?php
require 'slog.php';
use think\org\Slog;

//方便正式发行代码的时候去掉调试信息
if (!defined('SOCKET_LOG') || SOCKET_LOG === false) {
    if (!function_exists('sdebug')) {
        function sdebug($log, $tip = '', $file = __FILE__, $line = __LINE__, $type = 'log', $css = '')
        {
            //DONOTHING
        }
    }

    if (!function_exists('slog')) {
        function slog($log, $type = 'log', $css = '')
        {
            //DONOTHING
        }
    }

} else {
    if (!function_exists('sdebug')) {
        /**
         * 增加调试输出的结果，方便信息出错的位置
         *
         * @param        $log
         * @param string $tip
         * @param string $file
         * @param int    $line
         * @param string $type
         * @param string $css
         *
         * @throws Exception
         */
        function sdebug($log, $tip = '', $file = __FILE__, $line = __LINE__, $type = 'log', $css = '')
        {
            slog(sprintf("%s:%s", $file, $line));
            if (!empty($tip)) {
                slog(sprintf("[%s]", $tip), $type, $css);
            }
            slog($log, $type, $css);
        }
    }

    if (!function_exists('slog')) {
        function slog($log, $type = 'log', $css = '')
        {
            if (is_string($type)) {
                $type = preg_replace_callback('/_([a-zA-Z])/', create_function('$matches', 'return strtoupper($matches[1]);'), $type);
                if (method_exists('\think\org\Slog', $type) || in_array($type, Slog::$log_types)) {
                    return call_user_func(array('\think\org\Slog', $type), $log, $css);
                }
            }

            if (is_object($type) && 'mysqli' == get_class($type)) {
                return Slog::mysqlilog($log, $type);
            }

            if (is_resource($type) && ('mysql link' == get_resource_type($type) || 'mysql link persistent' == get_resource_type($type))) {
                return Slog::mysqllog($log, $type);
            }


            if (is_object($type) && 'PDO' == get_class($type)) {
                return Slog::pdolog($log, $type);
            }

            throw new Exception($type . ' is not SocketLog method');
        }
    }
}
