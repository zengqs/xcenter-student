<?php

require_once(APP_PATH . 'Admin/Common/function.php');



function get_status($status){
    $status_arr=array(
        '0'=>'停用',
        '1'=>'激活'
    );

    return $status_arr[$status];
}

function get_staff_status($status){
    $status_arr=array(
        '0'=>'离职',
        '1'=>'在职'
    );

    return $status_arr[$status];
}

function get_yes_no($status){
    $status_arr=array(
        '0'=>'否',
        '1'=>'是'
    );

    return $status_arr[$status];
}
