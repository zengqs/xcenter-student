// 修改指定表的字段
function changeTableVal(table, id_name, id_value, field, obj) {
    var value = $(obj).val();
    $.ajax({
        url: Think.U("Libraryadmin/Base/changeTableVal", {
            table: table,
            id_name: id_name,
            id_value: id_value,
            field: field,
            value: value
        }),
        success: function (data) {
            layer.msg('更新成功', {icon: 1, time: 1000});
        }
    });
}

// 修改指定表的指定字段值
function changeTableValYesNo(table, id_name, id_value, field, obj) {
    var src = "";
    if ($(obj).attr('src').indexOf("cancel.png") > 0) {
        src = Think.ROOT + 'Application/Libraryadmin/Static/images/yes.png';
        var value = 1;

    } else {
        src = Think.ROOT + 'Application/Libraryadmin/Static/images/cancel.png';
        var value = 0;
    }
    $.ajax({
        url: Think.U('Libraryadmin/Base/changeTableVal', {
            table: table,
            id_name: id_name,
            id_value: id_value,
            field: field,
            value: value
        }),
        success: function (data) {
            $(obj).attr('src', src);
            layer.msg('更新成功', {icon: 1, time: 1000});
        }
    });
}