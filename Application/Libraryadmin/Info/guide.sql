
delete from `ocenter_menu` where  `module`='Libraryadmin';
INSERT INTO `ocenter_menu` (`title`, `pid`, `sort`, `url`, `hide`, `tip`, `group`, `is_dev`, `icon`, `module`) VALUES
  ('Libraryadmin', 0, 0, 'Libraryadmin/Staff/staffList', 1, '', '', 0, '', 'Libraryadmin');

SET @tmp_id = 0;
SELECT @tmp_id := id
FROM `ocenter_menu`
WHERE title = 'Libraryadmin' AND `module`='Libraryadmin';

INSERT INTO `ocenter_menu` (`title`, `pid`, `sort`, `url`, `hide`, `tip`, `group`, `is_dev`, `icon`, `module`) VALUES
  ('修改数据库表的值', @tmp_id, 1, 'Libraryadmin/Base/changeTableVal', 1, '', '内容管理', 0, '', 'Libraryadmin');

INSERT INTO `ocenter_menu` (`title`, `pid`, `sort`, `url`, `hide`, `tip`, `group`, `is_dev`, `icon`, `module`) VALUES
  ('员工', @tmp_id, 1, 'Libraryadmin/Staff/staffList', 0, '', '内容管理', 0, '', 'Libraryadmin'),
  ('员工列表', @tmp_id, 1, 'Libraryadmin/Staff/staffInfo', 1, '', '', 0, '', 'Libraryadmin'),
  ('编辑服务信息', @tmp_id, 1, 'Libraryadmin/Staff/addEditStaff', 1, '', '', 0, '', 'Libraryadmin'),
  ('激活/停用服务', @tmp_id, 1, 'Libraryadmin/Staff/changeStaffStatus', 1, '', '', 0, '', 'Libraryadmin'),
  ('获取服务列表', @tmp_id, 1, 'Libraryadmin/Staff/ajaxStaffList', 1, '', '', 0, '', 'Libraryadmin');
