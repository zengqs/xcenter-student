<?php
return array(
    // 预先加载的标签库
    'TAGLIB_PRE_LOAD' => 'OT\\TagLib\\Article,OT\\TagLib\\Think',

    /* 主题设置 */
    'DEFAULT_THEME' => 'default', // 默认模板主题名称

    /* 模板相关配置 */
    'TMPL_PARSE_STRING' => array(

        //管理后台的配置，不能修改，用于后台管理界面的继承
        '__STATIC__' => __ROOT__ . '/Public/static',
        '__ADDONS__' => __ROOT__ . '/Public/Admin/Addons',

        '__ADMIN_BTS__'    => __ROOT__ . '/Application/Admin/Static/bootstrap',
        '__ADMIN_ALTE__'   => __ROOT__ . '/Application/Admin/Static/adminlte',

        '__ADMIN_IMG__'    => __ROOT__ . '/Application/Admin/Static/images',
        '__ADMIN_CSS__'    => __ROOT__ . '/Application/Admin/Static/css',
        '__ADMIN_JS__'     => __ROOT__ . '/Application/Admin/Static/js',

        //兼容旧版本
        '__ZUI__'     => __ROOT__ . '/Application/zui',

        //核心库
        '__CORE_IMAGE__'    => __ROOT__ . '/Application/Core/Static/images',
        '__CORE_CSS__'      => __ROOT__ . '/Application/Core/Static/css',
        '__CORE_JS__'       => __ROOT__ . '/Application/Core/Static/js',

        //模块相关的模板常量
        '__ROOT__'        => __ROOT__,
        '__APPLICATION__' => __ROOT__ . '/Application',
        '__MODULE__' => __ROOT__ . '/Application/' . MODULE_NAME,
        '__MODULE_STATIC__' => __ROOT__ . '/Application/' . MODULE_NAME . '/Static',
        '__LIBS__' => __ROOT__ . '/Application/' . MODULE_NAME . '/Static/libs',
        '__IMG__'  => __ROOT__ . '/Application/' . MODULE_NAME . '/Static/images',
        '__CSS__'  => __ROOT__ . '/Application/' . MODULE_NAME . '/Static/css',
        '__JS__'   => __ROOT__ . '/Application/' . MODULE_NAME . '/Static/js',
    ),
);
