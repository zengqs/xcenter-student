<?php

namespace Workshop\Controller;

use Admin\Builder\AdminConfigBuilder;
use Admin\Builder\AdminListBuilder;
use Admin\Controller\AdminController;
use Vendor\AjaxPage;

//后台管理界面必须引入这个文件
require_once(APP_PATH . MODULE_NAME . '/Common/admin.php');


class SlideadminController extends AdminbaseController
{

    function _initialize()
    {
        parent::_initialize();
    }

    public function indexList()
    {
        //Linux下，魔板文件名如果不是小写需要写出完整的文件名
        $this->display('indexlist');
    }

    public function ajaxindexList()
    {
        if (IS_AJAX) {
            $p = I('get.p', 1, 'intval');
            $where = ' 1 = 1 '; // 搜索条件
            // 关键词搜索，模糊匹配
            $key_word = I('post.key_word') ? trim(I('post.key_word')) : '';
            if ($key_word) {
                $where = "$where and (title like '%$key_word%')";
            }

            $time_select = I('post.time_select');
            if ($time_select == 'on') {
                $d1 = $this->begin;
                $d2 = $this->end;
                $where .= " AND (`update_time` between $d1 and $d2)";
            }

            $model = D('Slide');
            $count = $model->where($where)->count();
            $Page = new AjaxPage($count, C('LIST_ROWS'), 'get_page', '', 'p');

            $show = $Page->show();
            $this->assign('page', $show);// 赋值分页输出

            $orderby1 = I('orderby1');
            $orderby2 = I('orderby2');
            if (!empty($orderby1) && !empty($orderby2)) {
                $order_str = '`' . $orderby1 . '` ' . $orderby2;
            } else {
                $order_str = array();
            }

            $indexList = $model->where($where)->order($order_str)->page($p, C('LIST_ROWS'))->select();
            $this->assign('indexList', $indexList);

            $html = $this->fetch(); //生成表格的HTML代码
            $this->ajaxReturn($html);
        }
    }

    //显示商户的信息，查询页面
    public function indexInfo()
    {
        $index = array();
        $id = I('id', 0, 'intval');
        if ($id) {
            $index = D('Slide')->field(true)->find($id);
//            sdebug($index, '$index');
        }
        $this->assign('slide', $index); //当前编辑或者新增的报价单
        $this->display();
    }

    /**
     * 编辑、新增页面
     */
    public function addEditindex()
    {

        $id = I('id', 0, 'intval');
        if (IS_POST) { //保存模式
            $title = I('title');
            if (!$title) {
                $this->error('请输入标题');
            }

            $model = D('Slide');
            $model->create();
            if ($_GET['id']) {
                $model->save();
            } else {
                $model->add();
            }

            $this->success("操作成功!", U('indexList'));
            exit;
        } else { //显示模式
            if ($id) {
                $index = D('Slide')->find($id);
            } else {
                $index = array();
            }
            $this->assign('slide', $index); //当前编辑或者新增的对象
            $this->display();
        }
    }

    public function delindex()
    {
        $id = I('id', 0, 'intval');
        if ($id) {
            $status = D('Slide')->delete($id);
            $this->ajaxReturn(array('status' => $status));
        }
        $this->ajaxReturn(array('status' => 0,'msg'=>'参数错误'));
    }

}