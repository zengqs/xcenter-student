<?php

namespace Workshop\Controller;

class AboutController extends HomebaseController
{
    public function _initialize()
    {
        parent::_initialize();//父类的初始化操作
        $config = array();
        $config['slog'] = modC('ABOUT_SLOG', '', 'Configadmin');
        $config['content'] = html(modC('ABOUT_CONTENT', '', 'Configadmin'));
        //合并父类读取的配置文件
        $this->config=array_merge($this->config, $config);
    }

    public function about()
    {
        $this->display();
    }

    //获取最近的新闻列表
    public function ajaxLastNews()
    {
        $data = D('News')->getLastNews(3);
        $this->ajaxReturn(array('list' => $data));
    }
}