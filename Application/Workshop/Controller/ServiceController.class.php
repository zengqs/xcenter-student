<?php

namespace Workshop\Controller;

use Think\Controller;

class ServiceController extends HomebaseController
{



    public function service(){
        $this->display();
    }


    public function ajaxGetServices(){
//        $data=array(
//            array('title'=>'Service Title','description'=>'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Praesent vestibulum molestie lacus. Aenean nonummy hendrerit mauris. Phasellus porta. Fusce suscipit varius mi.'),
//            array('title'=>'Service Title','description'=>'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Praesent vestibulum molestie lacus. Aenean nonummy hendrerit mauris. Phasellus porta. Fusce suscipit varius mi.'),
//            array('title'=>'Service Title','description'=>'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Praesent vestibulum molestie lacus. Aenean nonummy hendrerit mauris. Phasellus porta. Fusce suscipit varius mi.'),
//            array('title'=>'Service Title','description'=>'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Praesent vestibulum molestie lacus. Aenean nonummy hendrerit mauris. Phasellus porta. Fusce suscipit varius mi.'),
//        );

        $data=D('Service')->getTopServices(4);
//        sdebug($data);
        $this->ajaxReturn(array('list'=>$data));
    }

    public function ajaxGetSpecials(){
//        $data=array(
//            array('title'=>'专辑标题','description'=>'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Praesent vestibulum molestie lacus. Aenean nonummy hendrerit mauris. Phasellus porta. Fusce suscipit varius mi.'),
//            array('title'=>'专辑标题 Title','description'=>'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Praesent vestibulum molestie lacus. Aenean nonummy hendrerit mauris. Phasellus porta. Fusce suscipit varius mi.'),
//            array('title'=>'专辑标题 Title','description'=>'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Praesent vestibulum molestie lacus. Aenean nonummy hendrerit mauris. Phasellus porta. Fusce suscipit varius mi.'),
//        );
//        sdebug($data);
        $data=D('Service')->getLastServices(3);
//        sdebug($data);

        $this->ajaxReturn(array('list'=>$data));
    }

}