<?php

namespace Workshop\Controller;

use Think\Controller;

class StaffController extends HomebaseController
{

    public function staff(){
        $this->display();
    }

    public function ajaxGetStaffs(){
//        $data=array(
//            array('title'=>'Service Title','abstract'=>'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Praesent vestibulum molestie lacus. Aenean nonummy hendrerit mauris. Phasellus porta. Fusce suscipit varius mi.'),
//            array('title'=>'Service Title','abstract'=>'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Praesent vestibulum molestie lacus. Aenean nonummy hendrerit mauris. Phasellus porta. Fusce suscipit varius mi.'),
//            array('title'=>'Service Title','abstract'=>'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Praesent vestibulum molestie lacus. Aenean nonummy hendrerit mauris. Phasellus porta. Fusce suscipit varius mi.'),
//            array('title'=>'Service Title','abstract'=>'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Praesent vestibulum molestie lacus. Aenean nonummy hendrerit mauris. Phasellus porta. Fusce suscipit varius mi.'),
//        );

        $data=D('Staff')->getTopStaffs(4);
//        sdebug($data,'Staff');
        $this->ajaxReturn(array('list'=>$data));
    }

}