<?php

namespace Workshop\Controller;

use Think\Controller;

class IndexController extends HomebaseController
{

    public function index()
    {
        $slides = D('Slide')->getTopSlides(3);
        $this->assign('slides', $slides);
        $this->display();
    }

    //最近的项目
    public function ajaxLastProjects()
    {
        $data = D('Project')->getLastProjects(12);
        $this->ajaxReturn(array('list' => $data));
    }

    //客户评价
    public function ajaxGetTestimonials(){
        $data=D('Testimonials')->limit(1)->select();
        $this->ajaxReturn(array('list' => $data));
    }

}