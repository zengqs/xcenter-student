<?php

namespace Workshop\Controller;

use Admin\Builder\AdminConfigBuilder;
use Admin\Builder\AdminListBuilder;
use Admin\Controller\AdminController;
use Vendor\AjaxPage;

//后台管理界面必须引入这个文件
require_once(APP_PATH . MODULE_NAME . '/Common/admin.php');


class StaffadminController extends AdminbaseController
{

    function _initialize()
    {
        parent::_initialize();
    }

    public function staffList()
    {
        //Linux下，魔板文件名如果不是小写需要写出完整的文件名
        $this->display('stafflist');
    }

    public function ajaxstaffList()
    {
        if (IS_AJAX) {
            $p = I('get.p', 1, 'intval');
            $where = ' 1 = 1 '; // 搜索条件
            // 关键词搜索，模糊匹配
            $key_word = I('post.key_word') ? trim(I('post.key_word')) : '';
            if ($key_word) {
                $where = "$where and (name like '%$key_word%')";
            }

            $time_select = I('post.time_select');
            if ($time_select == 'on') {
                $d1 = $this->begin;
                $d2 = $this->end;
                $where .= " AND (`update_time` between $d1 and $d2)";
            }

            $model = D('staff');
            $count = $model->where($where)->count();
            $Page = new AjaxPage($count, C('LIST_ROWS'), 'get_page', '', 'p');

            $show = $Page->show();
            $this->assign('page', $show);// 赋值分页输出

            $orderby1 = I('orderby1');
            $orderby2 = I('orderby2');
            if (!empty($orderby1) && !empty($orderby2)) {
                $order_str = '`' . $orderby1 . '` ' . $orderby2;
            } else {
                $order_str = array();
            }

            $staffList = $model->where($where)->order($order_str)->page($p, C('LIST_ROWS'))->select();
            $this->assign('staffList', $staffList);

            $html = $this->fetch(); //生成表格的HTML代码
            $this->ajaxReturn($html);
        }
    }

    //显示商户的信息，查询页面
    public function staffInfo()
    {
        $staff = array();
        $id = I('id', 0, 'intval');
        if ($id) {
            $staff = D('staff')->field(true)->find($id);
            sdebug($staff, '$staff');
        }
     
        $this->assign('staff', $staff); //当前编辑或者新增的报价单
        $this->display();
    }

    /**
     * 编辑、新增页面
     */
    public function addEditstaff()
    {

        $id = I('id', 0, 'intval');
        if (IS_POST) { //保存模式
            $name = $_POST['name'];
            echo ($name);
            if (!$name) {
                $this->error('请输入姓名');
            }

            $model = D('staff');
            $model->create();
            if ($_GET['id']) {
                $model->save();
            } else {
                $model->add();
            }

            $this->success("操作成功!", U('staffList'));
            exit;
        } else { //显示模式
            if ($id) {
                $staff = D('staff')->find($id);
            } else {
                $staff = array();
            }
            $this->assign('staff', $staff); //当前编辑或者新增的对象
            $this->display();
        }
    }

    public function delstaff()
    {
        $id = I('id', 0, 'intval');
        if ($id) {
            $status = D('staff')->delete($id);
            $this->ajaxReturn(array('status' => $status));
        }
        $this->ajaxReturn(array('status' => 0,'msg'=>'参数错误'));
    }

}