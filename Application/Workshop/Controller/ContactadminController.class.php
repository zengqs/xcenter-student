<?php

namespace Workshop\Controller;

use Admin\Builder\AdminConfigBuilder;
use Admin\Builder\AdminListBuilder;
use Admin\Controller\AdminController;
use Vendor\AjaxPage;

//后台管理界面必须引入这个文件
require_once(APP_PATH . MODULE_NAME . '/Common/admin.php');


class ContactadminController extends AdminbaseController
{

    function _initialize()
    {
        parent::_initialize();
    }

    public function contactList()
    {
        //Linux下，魔板文件名如果不是小写需要写出完整的文件名
        $this->display('contactlist');
    }

    public function ajaxContactList()
    {
        if (IS_AJAX) {
            $p = I('get.p', 1, 'intval');
            $where = ' 1 = 1 '; // 搜索条件
            // 关键词搜索，模糊匹配
            $name = I('post.name') ? trim(I('post.name')) : '';
            if ($name) {
                $where = "$where and (title like '%$name%')";
            }
            $model = D('Contact');
            $count = $model->where($where)->count();
            $Page = new AjaxPage($count, C('LIST_ROWS'), 'get_page', '', 'p');

            $show = $Page->show();
            $this->assign('page', $show);// 赋值分页输出

            $orderby1 = I('orderby1');
            $orderby2 = I('orderby2');
            if (!empty($orderby1) && !empty($orderby2)) {
                $order_str = '`' . $orderby1 . '` ' . $orderby2;
            } else {
                $order_str = array();
            }

            $contactList = $model->where($where)->order($order_str)->page($p, C('LIST_ROWS'))->select();
            $this->assign('contactList', $contactList);

            $html = $this->fetch(); //生成表格的HTML代码
            $this->ajaxReturn($html);
        }
    }

    //显示商户的信息，查询页面
    public function contactInfo()
    {
        $contact = array();
        $id = I('id', 0, 'intval');
        if ($id) {
            $contact = D('Contact')->field(true)->find($id);
            sdebug($contact, '$contact');
        }
        $this->assign('contact', $contact); //当前编辑或者新增的报价单
        $this->display();
    }

    /**
     * 编辑、新增页面
     */
    public function addEditcontact()
    {

        $id = I('id', 0, 'intval');
        if (IS_POST) { //保存模式
            $title = I('title');
            if (!$title) {
                $this->error('请输入标题');
            }

            $model = D('Contact');
            $model->create();
            if ($_GET['id']) {
                $model->save();
            } else {
                $model->add();
            }

            $this->success("操作成功!", U('contactList'));
            exit;
        } else { //显示模式
            if ($id) {
                $contact = D('contact')->find($id);
            } else {
                $contact = array();
            }
            $this->assign('contact', $contact); //当前编辑或者新增的对象
            $this->display();
        }
    }

    public function delcontact()
    {
        $id = I('id', 0, 'intval');
        if ($id) {
            $status = D('Contact')->delete($id);
            $this->ajaxReturn(array('status' => $status));
        }
        $this->ajaxReturn(array('status' => 0,'msg'=>'参数错误'));
    }

}