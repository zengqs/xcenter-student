<?php


namespace Workshop\Controller;

use Think\Controller;

class ContactController extends HomebaseController
{

    public function contact()
    {

        $this->display();
    }

    public function addContact()
    {
        if (IS_POST) { //保存模式

            $model = D('Contact');
            $info = $model->create();
            if (!$info) {
                $this->ajaxReturn(
                    array(
                        'code' => 2,//表示出错
                        'data' => $info,
                        'msg' => $model->getError()
                    )

                );
            }
            if ($lastInsId = $model->add()) {
                $this->ajaxReturn(
                    array(
                        'code' => 0,//操作正常进行
                        'data' => $model->where(array('id' => $lastInsId))->find(),
                        'msg' => '您的问题已收到'
                    )
                );
            } else {
                $this->ajaxReturn(
                    array(
                        'code' => 3,//错误
                        'data' => $info,
                        'msg' => $model->getError()
                    )
                );
            }
        }


    }
}