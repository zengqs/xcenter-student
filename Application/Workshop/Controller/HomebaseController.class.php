<?php

namespace Workshop\Controller;
use Think\Controller;

class HomebaseController extends Controller
{
    protected $config=array();
    public function __construct() {
        parent::__construct();
        $this->assign('config',$this->config);
    }

    public function _initialize()
    {
        $config=array();
        //读取配置文件
        $config['email']=modC('CONTACT_EMAIL','','Configadmin');
        $config['phone']=modC('CONTACT_PHONE','','Configadmin');
        $config['address']=modC('CONTACT_ADDRESS','','Configadmin');
        $config['fax']=modC('CONTACT_FAX','','Configadmin');
        $config['copyright']=modC('COPYRIGHT','','Configadmin');
        $config['aboutus']=modC('ABOUTUS','','Configadmin');
        //读取通用配置数据
        $this->config=$config;
    }
}