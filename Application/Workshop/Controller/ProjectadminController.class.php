<?php

namespace Workshop\Controller;

use Admin\Builder\AdminConfigBuilder;
use Admin\Builder\AdminListBuilder;
use Admin\Controller\AdminController;
use Vendor\AjaxPage;

//后台管理界面必须引入这个文件
require_once(APP_PATH . MODULE_NAME . '/Common/admin.php');


class ProjectadminController extends AdminbaseController
{

    function _initialize()
    {
        parent::_initialize();
    }

    public function projectList()
    {
        //Linux下，魔板文件名如果不是小写需要写出完整的文件名
        $this->display('projectlist');
    }

    public function ajaxProjectList()
    {
        if (IS_AJAX) {
            $p = I('get.p', 1, 'intval');
            $where = ' 1 = 1 '; // 搜索条件
            // 关键词搜索，模糊匹配
            $key_word = I('post.key_word') ? trim(I('post.key_word')) : '';
            if ($key_word) {
                $where = "$where and (title like '%$key_word%')";
            }

            $time_select = I('post.time_select');
            if ($time_select == 'on') {
                $d1 = $this->begin;
                $d2 = $this->end;
                $where .= " AND (`update_time` between $d1 and $d2)";
            }

            $model = D('Project');
            $count = $model->where($where)->count();
            $Page = new AjaxPage($count, C('LIST_ROWS'), 'get_page', '', 'p');

            $show = $Page->show();
            $this->assign('page', $show);// 赋值分页输出

            $orderby1 = I('orderby1');
            $orderby2 = I('orderby2');
            if (!empty($orderby1) && !empty($orderby2)) {
                $order_str = '`' . $orderby1 . '` ' . $orderby2;
            } else {
                $order_str = array();
            }

            $projectList = $model->where($where)->order($order_str)->page($p, C('LIST_ROWS'))->select();
            $this->assign('projectList', $projectList);

            $html = $this->fetch(); //生成表格的HTML代码
            $this->ajaxReturn($html);
        }
    }

    //显示商户的信息，查询页面
    public function projectInfo()
    {
        $project = array();
        $id = I('id', 0, 'intval');
        if ($id) {
            $project = D('Project')->field(true)->find($id);
            sdebug($project, '$project');
        }
        $this->assign('project', $project); //当前编辑或者新增的报价单
        $this->display();
    }

    /**
     * 编辑、新增页面
     */
    public function addEditProject()
    {

        $id = I('id', 0, 'intval');
        if (IS_POST) { //保存模式
            $title = I('title');
            if (!$title) {
                $this->error('请输入标题');
            }

            $model = D('Project');
            $model->create();
            if ($_GET['id']) {
                $model->save();
            } else {
                $model->add();
            }

            $this->success("操作成功!", U('projectList'));
            exit;
        } else { //显示模式
            if ($id) {
                $project = D('Project')->find($id);
            } else {
                $project = array();
            }
            $this->assign('project', $project); //当前编辑或者新增的对象
            $this->display();
        }
    }

    public function delProject()
    {
        $id = I('id', 0, 'intval');
        if ($id) {
            $status = D('Project')->delete($id);
            $this->ajaxReturn(array('status' => $status));
        }
        $this->ajaxReturn(array('status' => 0,'msg'=>'参数错误'));
    }

}
