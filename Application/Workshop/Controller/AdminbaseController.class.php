<?php

namespace Workshop\Controller;

use Admin\Controller\AdminController;

//后台管理界面必须引入这个文件
require_once(APP_PATH . MODULE_NAME . '/Common/admin.php');


class AdminbaseController extends AdminController
{

    //查询的起止时间
    public $begin;
    public $end;

    public function _initialize()
    {
        parent::_initialize();
        $timegap = I('timegap');
        $gap = I('gap', 7);
        if ($timegap) {
            $gap = explode(' - ', $timegap);
            $begin = $gap[0];
            $end = $gap[1];
        } else {
            $lastweek = date('Y-m-d', strtotime("-3 month"));//3月前
            $lastday = strtotime(date('Ymd')); //今天24时时间
            $begin = I('begin', $lastweek);
            $end = I('end', date('Y-m-d', $lastday));
        }
        $this->begin = strtotime($begin);
        $this->end = strtotime($end);

        $this->assign('timegap', date('Y-m-d', $this->begin) . ' - ' . date('Y-m-d', $this->end));
        $this->begin = strtotime("$begin 00:00:00");
        $this->end = strtotime("$end 23:59:59");
    }

    /**
     * ajax 修改指定表数据字段  一般修改状态 比如 是否推荐 是否开启 等 图标切换的
     * table,id_name,id_value,field,value
     */
    public function changeTableVal()
    {
        $table = I('table'); // 表名
        $id_name = I('id_name'); // 表主键id名
        $id_value = I('id_value'); // 表主键id值
        $field = I('field'); // 修改哪个字段
        $value = I('value'); // 修改字段值
        // 根据条件保存修改的数据
        M($table)->where("$id_name = $id_value")
            ->save(array($field => $value));
        sdebug(M()->getLastSql());
    }
}
