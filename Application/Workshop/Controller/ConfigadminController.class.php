<?php

namespace Workshop\Controller;

use Admin\Builder\AdminConfigBuilder;

//后台管理界面必须引入这个文件
require_once(APP_PATH . MODULE_NAME . '/Common/admin.php');


class ConfigadminController extends AdminbaseController
{

    function _initialize()
    {
        parent::_initialize();
    }

    public function config()
    {

        $builder = new AdminConfigBuilder();
        $data = $builder->handleConfig();

        //配置文件
        $builder->title("模块参数设置");

        //$builder->group('通用的消息回复设置', "NEED_VERIFY{$mp_id},KAITONGMA_COVER{$mp_id},JIHUOMA_COVER{$mp_id},UCENTER_COVER{$mp_id}");
        $builder->keyText('CONTACT_ADDRESS', 'Address')
            ->keyText('CONTACT_PHONE', 'Phone')
            ->keyText('CONTACT_FAX', 'Fax')
            ->keyText('CONTACT_EMAIL', 'Email')
            ->keyTextArea('ABOUTUS', 'About us')
            ->keyText('COPYRIGHT', 'Copyright','页脚版权申明文字。');

        $builder->keyText('ABOUT_SLOG', 'Slog','About页面的宣传口号',array('width' => '600px'))
            ->keyEditor('ABOUT_CONTENT', 'Content','关于页面中关于我们的详细介绍，支持HTML标签。','',array('width'=>'100%','height'=>'300px'));

        $builder->buttonSubmit()
            ->buttonBack()
            ->data($data);
        $builder->display();
    }

}
