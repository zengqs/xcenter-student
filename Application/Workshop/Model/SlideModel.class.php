<?php

namespace Workshop\Model;

use Think\Model;

class SlideModel extends Model
{

    protected $tableName = 'workshop_slide';
    protected $_auto = array(
        array('status', '1', self::MODEL_INSERT),
        array('create_time', 'time', self::MODEL_INSERT, 'function'), // 对update_time字段在更新的时候写入当前时间戳
        array('update_time', 'time', self::MODEL_BOTH, 'function'), // 对update_time字段在更新的时候写入当前时间戳
    );

    /**
     * 获取排序值最靠前的n条服务项目记录
     * @param $n 获取的服务项目数量
     * @return mixed 包含服务项目的列表或者为NULL
     *
     */
    function getTopSlides($n){
        $list=$this->where(array('status'=>1))->order('sort desc')->limit($n)->select();
        foreach ($list as &$item){
            $item['cover_url']=get_image_by_id($item['cover']);
        }
        sdebug($list,'Call index Model: getTopindexs',__FILE__,__LINE__);
        return $list;
    }

}

?>
