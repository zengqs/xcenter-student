<?php

namespace Workshop\Model;

use Think\Model;

class StaffModel extends Model
{

    protected $tableName = 'workshop_staff';
    protected $_auto = array(
        array('status', '0', self::MODEL_INSERT),
        array('create_time', 'time', self::MODEL_INSERT, 'function'), // 对update_time字段在更新的时候写入当前时间戳
        array('update_time', 'time', self::MODEL_UPDATE, 'function'), // 对update_time字段在更新的时候写入当前时间戳
    );

    /**
     * 获取排序值最靠前的n条记录
     * @param $n 获取的记录数量
     * @return mixed 列表或者为NULL
     *
     */
    function getTopStaffs($n){
        $list=$this->where(array('status'=>1))->order('sort desc')->limit($n)->select();
        foreach ($list as &$item){
            $item['cover_url']=get_image_by_id($item['cover']);
        }
        return $list;
    }

}

?>
