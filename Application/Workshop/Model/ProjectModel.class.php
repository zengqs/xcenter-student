<?php

namespace Workshop\Model;

use Think\Model;

class ProjectModel extends Model
{

    protected $tableName = 'workshop_project';
    protected $_auto = array(
        array('status', '1', self::MODEL_INSERT),
        array('create_time', 'time', self::MODEL_INSERT, 'function'), // 对update_time字段在更新的时候写入当前时间戳
        array('update_time', 'time', self::MODEL_BOTH, 'function'), // 对update_time字段在更新的时候写入当前时间戳
    );


    function getTopProjects($n){
        $list=$this->where(array('status'=>1))->order('sort desc')->limit($n)->select();
        foreach ($list as &$item){
            $item['cover_url']=get_image_by_id($item['cover']);
        }
        return $list;
    }


    function getLastProjects($n){
        $list=$this->where(array('status'=>1))->order('update_time desc')->limit($n)->select();
        foreach ($list as &$item){
            $item['cover_url']=get_image_by_id($item['cover']);
        }
        return $list;
    }
}

?>
