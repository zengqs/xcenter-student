<?php

namespace Workshop\Model;

use Think\Model;

class ContactModel extends Model
{
    protected $tableName = 'workshop_contact';
//    protected $_map = array(
//        'userName' =>'name', // 把表单中userName映射到数据表的name字段
//        'userEmail'  =>'email', // 把表单中的userEmail映射到数据表的email字段
//        'userPhone'  =>'phone', // 把表单中的userPhone映射到数据表的phone字段
//        'userMsg'  =>'content', // 把表单中的userMsg映射到数据表的content字段
//    );
    protected $_validate = array(
        array('name','require','必须填写姓名',0),
        array('email','require','邮箱必须填写，否则可能无法联系到您',0),
       // array('email','email','邮箱格式不正确',0),
        array('phone','require','手机号必须填写，否则可能无法联系到您',0),
       // array('phone','number','手机格式不正确',0),
    );

}
?>
