<?php
/**
 * 后台管理模块的通用函数，需要在后台管理模块中手动引入
 * require_once(APP_PATH . MODULE_NAME . '/Common/admin.php');
 *
 */
require_once(APP_PATH . 'Admin/Common/function.php');

/**
 * @param $arr
 * @param $key_name
 * @return array
 * 将数据库中查出的列表以指定的 id 作为数组的键名
 */
if (!(function_exists('convert_arr_key'))) {
    function convert_arr_key($arr, $key_name)
    {
        $arr2 = array();
        foreach ($arr as $key => $val) {
            $arr2[$val[$key_name]] = $val;
        }
        return $arr2;
    }
}

/**
 * 获取数组中的某一列
 * @param type $arr 数组
 * @param type $key_name 列名
 * @return type  返回那一列的数组
 */
if (!(function_exists('get_arr_column'))) {
    function get_arr_column($arr, $key_name)
    {
        $arr2 = array();
        foreach ($arr as $key => $val) {
            $arr2[] = $val[$key_name];
        }
        return $arr2;
    }
}


function get_status($status){
    $status_arr=array(
        '0'=>'停用',
        '1'=>'激活'
    );

    return $status_arr[$status];
}

function get_yes_no($status){
    $status_arr=array(
        '0'=>'否',
        '1'=>'是'
    );

    return $status_arr[$status];
}