<?php
return array(
    'SERVICE_TITLE'=>'服务',
    'SERVICE_SERVICES' => '服务项目',
    'SERVICE_LAST_SERVICES' => '最新服务项目',
    'STAFF_TITLE'=>'员工',
    'STAFF_SERVICES' => '师资力量',
    'ABOUT_TITLE'=>'关于',
    'ABOUT_SERVICES' => '关于学校',
    'ABOUT_LAST_SERVICES' => '关于员工',
    'INDEX_TITLE'=>'首页',
    'INDEX_LAST_SERVICES' => '最新项目',
    'CONTACT_TITLE' => '联系我们'
);