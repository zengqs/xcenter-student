
delete from `ocenter_menu` where  `module`='Workshop';
INSERT INTO `ocenter_menu` (`title`, `pid`, `sort`, `url`, `hide`, `tip`, `group`, `is_dev`, `icon`, `module`) VALUES
  ('Workshop', 0, 0, 'Workshop/Workshop/config', 1, '', '', 0, '', 'Workshop');


SET @tmp_id = 0;
SELECT @tmp_id := id
FROM `ocenter_menu`
WHERE title = 'Workshop' AND `module`='Workshop';

INSERT INTO `ocenter_menu` (`title`, `pid`, `sort`, `url`, `hide`, `tip`, `group`, `is_dev`, `icon`, `module`) VALUES
  ('模块设置', @tmp_id, 1, 'Workshop/Configadmin/config', 0, '', '系统设置', 0, '', 'Workshop');

INSERT INTO `ocenter_menu` (`title`, `pid`, `sort`, `url`, `hide`, `tip`, `group`, `is_dev`, `icon`, `module`) VALUES
  ('修改数据库表的值', @tmp_id, 1, 'Workshop/Adminbase/changeTableVal', 1, '', '内容管理', 0, '', 'Workshop');

INSERT INTO `ocenter_menu` (`title`, `pid`, `sort`, `url`, `hide`, `tip`, `group`, `is_dev`, `icon`, `module`) VALUES
  ('服务', @tmp_id, 1, 'Workshop/Serviceadmin/ServiceList', 0, '', '内容管理', 0, '', 'Workshop'),
  ('服务列表', @tmp_id, 1, 'Workshop/Serviceadmin/serviceInfo', 1, '', '', 0, '', 'Workshop'),
  ('编辑服务信息', @tmp_id, 1, 'Workshop/Serviceadmin/addEditService', 1, '', '', 0, '', 'Workshop'),
  ('激活/停用服务', @tmp_id, 1, 'Workshop/Serviceadmin/changeServiceStatus', 1, '', '', 0, '', 'Workshop'),
  ('获取服务列表', @tmp_id, 1, 'Workshop/Serviceadmin/ajaxServiceList', 1, '', '', 0, '', 'Workshop');


INSERT INTO `ocenter_menu` (`title`, `pid`, `sort`, `url`, `hide`, `tip`, `group`, `is_dev`, `icon`, `module`) VALUES
  ('项目', @tmp_id, 1, 'Workshop/Projectadmin/projectList', 0, '', '内容管理', 0, '', 'Workshop'),
  ('服务列表', @tmp_id, 1, 'Workshop/Projectadmin/projectInfo', 1, '', '', 0, '', 'Workshop'),
  ('编辑服务信息', @tmp_id, 1, 'Workshop/Projectadmin/addEditProject', 1, '', '', 0, '', 'Workshop'),
  ('激活/停用服务', @tmp_id, 1, 'Workshop/Projectadmin/changeProjectStatus', 1, '', '', 0, '', 'Workshop'),
  ('获取服务列表', @tmp_id, 1, 'Workshop/Projectadmin/ajaxProjectList', 1, '', '', 0, '', 'Workshop');



INSERT INTO `ocenter_menu` (`title`, `pid`, `sort`, `url`, `hide`, `tip`, `group`, `is_dev`, `icon`, `module`) VALUES
  ('新闻', @tmp_id, 1, 'Workshop/Newsadmin/newsList', 0, '', '内容管理', 0, '', 'Workshop'),
  ('新闻列表', @tmp_id, 1, 'Workshop/Newsadmin/newsInfo', 1, '', '', 0, '', 'Workshop'),
  ('编辑新闻信息', @tmp_id, 1, 'Workshop/Newsadmin/addEditNews', 1, '', '', 0, '', 'Workshop'),
  ('激活/停用新闻', @tmp_id, 1, 'Workshop/Newsadmin/changeNewsStatus', 1, '', '', 0, '', 'Workshop'),
  ('获取新闻列表', @tmp_id, 1, 'Workshop/Newsadmin/ajaxNewsList', 1, '', '', 0, '', 'Workshop');



INSERT INTO `ocenter_menu` (`title`, `pid`, `sort`, `url`, `hide`, `tip`, `group`, `is_dev`, `icon`, `module`) VALUES
  ('员工', @tmp_id, 1, 'Workshop/Staffadmin/staffList', 0, '', '内容管理', 0, '', 'Workshop'),
  ('员工列表', @tmp_id, 1, 'Workshop/Staffadmin/staffInfo', 1, '', '', 0, '', 'Workshop'),
  ('编辑服务信息', @tmp_id, 1, 'Workshop/Staffadmin/addEditStaff', 1, '', '', 0, '', 'Workshop'),
  ('激活/停用服务', @tmp_id, 1, 'Workshop/Staffadmin/changeStaffStatus', 1, '', '', 0, '', 'Workshop'),
  ('获取服务列表', @tmp_id, 1, 'Workshop/Staffadmin/ajaxStaffList', 1, '', '', 0, '', 'Workshop');

INSERT INTO `ocenter_menu` (`title`, `pid`, `sort`, `url`, `hide`, `tip`, `group`, `is_dev`, `icon`, `module`) VALUES
  ('留言', @tmp_id, 1, 'Workshop/Contactadmin/contactList', 0, '', '内容管理', 0, '', 'Workshop'),
  ('留言列表', @tmp_id, 1, 'Workshop/Contactadmin/contactInfo', 1, '', '', 0, '', 'Workshop'),
  ('编辑留言信息', @tmp_id, 1, 'Workshop/Contactadmin/addEditContact', 1, '', '', 0, '', 'Workshop'),
  ('激活/停用服务', @tmp_id, 1, 'Workshop/Contactadmin/changeContactStatus', 1, '', '', 0, '', 'Workshop'),
  ('获取留言列表', @tmp_id, 1, 'Workshop/Contactadmin/ajaxContactList', 1, '', '', 0, '', 'Workshop');


INSERT INTO `ocenter_menu` (`title`, `pid`, `sort`, `url`, `hide`, `tip`, `group`, `is_dev`, `icon`, `module`) VALUES
  ('滚动广告', @tmp_id, 1, 'Workshop/Slideadmin/indexList', 0, '', '内容管理', 0, '', 'Workshop'),
  ('项目列表', @tmp_id, 1, 'Workshop/Slideadmin/indexInfo', 1, '', '', 0, '', 'Workshop'),
  ('编辑服务信息', @tmp_id, 1, 'Workshop/Slideadmin/addEditIndex', 1, '', '', 0, '', 'Workshop'),
  ('激活/停用服务', @tmp_id, 1, 'Workshop/Slideadmin/changeIndexStatus', 1, '', '', 0, '', 'Workshop'),
  ('获取服务列表', @tmp_id, 1, 'Workshop/Slideadmin/ajaxIndexList', 1, '', '', 0, '', 'Workshop');
