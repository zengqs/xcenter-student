DROP TABLE IF EXISTS `ocenter_workshop_service`;
CREATE TABLE IF NOT EXISTS `ocenter_workshop_service` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(50) NOT NULL,
  `abstract` TEXT DEFAULT NULL COMMENT '摘要',
  `description` TEXT DEFAULT NULL COMMENT '内容',
  `cover` int(11) DEFAULT NULL COMMENT '封面图片的ID',
  `status` tinyint(2) NOT NULL DEFAULT 1,
  `sort` int(6) NOT NULL DEFAULT 0,
  `update_time` int(11) NOT NULL DEFAULT 0,
  `create_time` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='服务项目表';

DROP TABLE IF EXISTS `ocenter_workshop_staff`;
CREATE TABLE IF NOT EXISTS `ocenter_workshop_staff` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(50) NOT NULL,
  `abstract` TEXT DEFAULT NULL COMMENT '摘要',
  `description` TEXT DEFAULT NULL COMMENT '内容',
  `cover` int(11) DEFAULT NULL COMMENT '封面图片的ID',
  `status` tinyint(2) NOT NULL DEFAULT 1,
  `sort` int(6) NOT NULL DEFAULT 0,
  `update_time` int(11) NOT NULL DEFAULT 0,
  `create_time` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='员工表';

DROP TABLE IF EXISTS `ocenter_workshop_news`;
CREATE TABLE IF NOT EXISTS `ocenter_workshop_news` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(50) NOT NULL,
  `abstract` TEXT DEFAULT NULL COMMENT '摘要',
  `description` TEXT DEFAULT NULL COMMENT '内容',
  `cover` int(11) DEFAULT NULL COMMENT '封面图片的ID',
  `status` tinyint(2) NOT NULL DEFAULT 1,
  `sort` int(6) NOT NULL DEFAULT 0,
  `update_time` int(11) NOT NULL DEFAULT 0,
  `create_time` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='News';

DROP TABLE IF EXISTS `ocenter_workshop_project`;
CREATE TABLE IF NOT EXISTS `ocenter_workshop_project` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(50) NOT NULL,
  `abstract` TEXT DEFAULT NULL COMMENT '摘要',
  `description` TEXT DEFAULT NULL COMMENT '内容',
  `cover` int(11) DEFAULT NULL COMMENT '封面图片的ID',
  `status` tinyint(2) NOT NULL DEFAULT 1,
  `sort` int(6) NOT NULL DEFAULT 0,
  `update_time` int(11) NOT NULL DEFAULT 0,
  `create_time` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='News';


DROP TABLE IF EXISTS `ocenter_workshop_slide`;
CREATE TABLE IF NOT EXISTS `ocenter_workshop_slide` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(50) NOT NULL,
  `abstract` TEXT DEFAULT NULL COMMENT '摘要',
  `href` varchar(255) DEFAULT NULL COMMENT 'URL',
  `cover` int(11) DEFAULT NULL COMMENT '封面图片的ID',
  `status` tinyint(2) NOT NULL DEFAULT 1,
  `sort` int(6) NOT NULL DEFAULT 0,
  `update_time` int(11) NOT NULL DEFAULT 0,
  `create_time` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='首页幻灯片';

INSERT INTO ocenter_workshop_slide(title,cover) VALUES ('Slide 1',1);
INSERT INTO ocenter_workshop_slide(title,cover) VALUES ('Slide 2',2);
INSERT INTO ocenter_workshop_slide(title,cover) VALUES ('Slide 3',3);




DROP TABLE IF EXISTS `ocenter_workshop_contact`;
CREATE TABLE IF NOT EXISTS `ocenter_workshop_contact` (
  `id` mediumint(50) unsigned NOT NULL AUTO_INCREMENT COMMENT '联系人id',
  `name` varchar(30) NOT NULL COMMENT '姓名',
  `email` varchar(255) NOT NULL COMMENT '邮箱',
  `phone` varchar(11) NOT NULL DEFAULT '0' COMMENT '号码',
  `content` varchar(255) NOT NULL DEFAULT '无' COMMENT '内容',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COMMENT='联系我们';


DROP TABLE IF EXISTS `ocenter_workshop_testimonials`;
CREATE TABLE IF NOT EXISTS `ocenter_workshop_testimonials` (
  `id` mediumint(50) unsigned NOT NULL AUTO_INCREMENT COMMENT '联系人id',
  `name` varchar(30) NULL COMMENT '姓名',
  `content` TEXT NULL COMMENT '内容',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COMMENT='客户评价' ;


INSERT INTO ocenter_workshop_testimonials(name,content) VALUES ('西门崔雪','团队由学校与软件企业共建，以培养学生创新创业能力为主要目标，成员以学生为主，承接安卓、IOS、Web应用和微信公众号的应用开发，承接MOODLE课程资源平台的建设与维护服务。');


INSERT INTO ocenter_workshop_service(id,title,abstract,cover) VALUES (1,'服务项目1','服务项目1的内容',1);
INSERT INTO ocenter_workshop_service(id,title,abstract,cover) VALUES (2,'服务项目2','服务项目2的内容',1);
INSERT INTO ocenter_workshop_service(id,title,abstract,cover) VALUES (3,'服务项目3','服务项目3的内容',1);
INSERT INTO ocenter_workshop_service(id,title,abstract,cover) VALUES (4,'服务项目4','服务项目4的内容',1);
INSERT INTO ocenter_workshop_service(id,title,abstract,cover) VALUES (5,'服务项目5','服务项目5的内容',1);


INSERT INTO ocenter_workshop_staff(id,title,abstract,cover) VALUES (1,'员工1','服务项目1的内容',1);
INSERT INTO ocenter_workshop_staff(id,title,abstract,cover) VALUES (2,'员工1','服务项目2的内容',1);
INSERT INTO ocenter_workshop_staff(id,title,abstract,cover) VALUES (3,'员工1','服务项目3的内容',1);
INSERT INTO ocenter_workshop_staff(id,title,abstract,cover) VALUES (4,'员工1','服务项目4的内容',1);
INSERT INTO ocenter_workshop_staff(id,title,abstract,cover) VALUES (5,'员工1','服务项目5的内容',1);

INSERT INTO ocenter_workshop_news(id,title,abstract,cover) VALUES (1,'服务项目1','服务项目1的内容',1);
INSERT INTO ocenter_workshop_news(id,title,abstract,cover) VALUES (2,'服务项目2','服务项目2的内容',1);
INSERT INTO ocenter_workshop_news(id,title,abstract,cover) VALUES (3,'服务项目3','服务项目3的内容',1);
INSERT INTO ocenter_workshop_news(id,title,abstract,cover) VALUES (4,'服务项目4','服务项目4的内容',1);
INSERT INTO ocenter_workshop_news(id,title,abstract,cover) VALUES (5,'服务项目5','服务项目5的内容',1);


INSERT INTO ocenter_workshop_project(id,title,abstract,cover) VALUES (1,'服务项目1','服务项目1的内容',1);
INSERT INTO ocenter_workshop_project(id,title,abstract,cover) VALUES (2,'服务项目2','服务项目2的内容',1);
INSERT INTO ocenter_workshop_project(id,title,abstract,cover) VALUES (3,'服务项目3','服务项目3的内容',1);
INSERT INTO ocenter_workshop_project(id,title,abstract,cover) VALUES (4,'服务项目4','服务项目4的内容',1);
INSERT INTO ocenter_workshop_project(id,title,abstract,cover) VALUES (5,'服务项目5','服务项目5的内容',1);