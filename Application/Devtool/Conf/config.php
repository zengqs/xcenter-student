<?php
// +----------------------------------------------------------------------
// | OneThink [ WE CAN DO IT JUST THINK IT ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013 http://www.onethink.cn All rights reserved.
// +----------------------------------------------------------------------
// | Author: 麦当苗儿 <zuojiazi@vip.qq.com> <http://www.thinkphp.cn>
// +----------------------------------------------------------------------

/**
 * 前台配置文件
 * 所有除开系统级别的前台配置
 */

return array(

    // 预先加载的标签库
    'TAGLIB_PRE_LOAD' => 'OT\\TagLib\\Article,OT\\TagLib\\Think',

    /* 主题设置 */
    'DEFAULT_THEME' => 'default', // 默认模板主题名称


    /* 模板相关配置 */
    'TMPL_PARSE_STRING' => array(
        //从Admin/Conf/config.php中复制过来，以支持后台模版的继承
        '__STATIC__' => __ROOT__ . '/Public/static',
        '__ADDONS__' => __ROOT__ . '/Public/Admin/Addons',
        '__IMG__'    => __ROOT__ . '/Application/Admin/Static/images',
        '__CSS__'    => __ROOT__ . '/Application/Admin/Static/css',
        '__ZUI__'    => __ROOT__ . '/Public/zui',
        '__JS__'     => __ROOT__ . '/Application/Admin/Static/js',
        '__BTS__'    => __ROOT__ . '/Application/Admin/Static/bootstrap',
        '__ALTE__'   => __ROOT__ . '/Application/Admin/Static/adminlte',


        '__CORE_IMAGE__'=>__ROOT__.'/Application/Core/Static/images',
        '__CORE_CSS__'=>__ROOT__.'/Application/Core/Static/css',
        '__CORE_JS__'=>__ROOT__.'/Application/Core/Static/js',



        //模板常量
        '__ROOT__'        => __ROOT__,
        '__APPLICATION__' => __ROOT__ . '/Application',
        '__MODULE_IMG__' => __ROOT__ . '/Application/Mpbase/Static/images',
        '__MODULE_CSS__' => __ROOT__ . '/Application/Mpbase/Static/css',
        '__MODULE_JS__' => __ROOT__ . '/Application/Mpbase/Static/js',
        '__MODULE_MUI__' => __ROOT__ . '/Application/Mpbase/Static/libs/mui',
        '__MODULE_LIBS__' => __ROOT__ . '/Application/Mpbase/Static/libs',
    ),



    'NEED_VERIFY'=>true,//此处控制默认是否需要审核，该配置项为了便于部署起见，暂时通过在此修改来设定。

);

