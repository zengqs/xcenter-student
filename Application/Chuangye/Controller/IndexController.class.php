<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 15-5-28
 * Time: 下午01:33
 *
 * @author 郑钟良<zzl@ourstu.com>
 */

namespace Chuangye\Controller;


use Think\Controller;

class IndexController extends Controller
{

    function index()
    {
        $this->display();
    }
}
