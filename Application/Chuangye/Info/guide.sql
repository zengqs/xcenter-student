delete from `ocenter_menu` where  `module`='Chuangye';
INSERT INTO `ocenter_menu` (`title`, `pid`, `sort`, `url`, `hide`, `tip`, `group`, `is_dev`, `icon`, `module`) VALUES
  ('创业园', 0, 0, 'Chuangye/Chuangye/index', 1, '', '', 0, '', 'Chuangye');

SET @tmp_id = 0;
SELECT @tmp_id := id
FROM `ocenter_menu`
WHERE title = '创业园' AND `module`='Chuangye';

-- INSERT INTO `ocenter_menu` (`title`, `pid`, `sort`, `url`, `hide`, `tip`, `group`, `is_dev`, `icon`, `module`) VALUES
-- ('文章列表',@tmp_id, '0', 'Chuangye/Chuangye/index', '0', '', '文章管理', '0', '', 'Chuangye'),
-- ('编辑单页文章',@tmp_id, '0', 'Chuangye/Chuangye/editChuangye', '1', '', '', '0', '', 'Chuangye'),
-- ('设置单页文章状态',@tmp_id, '0', 'Chuangye/Chuangye/setChuangyeStatus', '1', '', '', '0', '', 'Chuangye'),
-- ('单页配置',@tmp_id, '0', 'Chuangye/Chuangye/config', '0', '', '配置管理', '0', '', 'Chuangye'),
-- ('单页分类',@tmp_id, '0', 'Chuangye/Chuangye/markdocCategory', '0', '', '配置管理', '0', '', 'Chuangye'),
-- ('编辑分类', @tmp_id, '0', 'Chuangye/Chuangye/editCategory', '1', '', '', '0', '', 'Chuangye'),
-- ('设置分类状态',@tmp_id, '0', 'Chuangye/Chuangye/setCategoryStatus', '1', '', '', '0', '', 'Chuangye');
--
--
-- DELETE FROM  `ocenter_auth_rule` WHERE `module`='Chuangye';
-- INSERT INTO `ocenter_auth_rule` ( `module`, `type`, `name`, `title`, `status`, `condition`) VALUES
--   ( 'Chuangye', 1, 'Chuangye/Chuangye/setCategoryStatus', '设置分类状态', 1, ''),
--   ( 'Chuangye', 1, 'Chuangye/Chuangye/editCategory', '编辑分类', 1, ''),
--   ( 'Chuangye', 1, 'Chuangye/Chuangye/editChuangye', '文档编辑', 1, '');
