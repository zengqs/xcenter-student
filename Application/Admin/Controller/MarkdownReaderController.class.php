<?php
namespace Admin\Controller;

class MarkdownReaderController extends AdminController{
	function getDocument(){
        $docPath=I("post.docPath");
        $fileName=I("post.fileName");
        if (IS_AJAX) {
            $file = sprintf("{$docPath}/{$fileName}");

            sdebug($file,'Markdown文档内容路径');
            $content = file_get_contents($file);
            $content = htmlentities($content);
            $hrefBase = ext_get_site_root_url(true, $docPath) . '/';
            $data = array(
                'hrefBase' => $hrefBase,
                'markdoc'    => $content
            );

//            sdebug($data,'Markdown文档内容');
            $this->ajaxReturn($data);
        }
    }

    public function singlePage()
    {
        $docPath=urldecode(I("get.docPath","","op_t"));
        $fileName=urldecode(I("get.fileName","","op_t"));
        $title=urldecode(I("get.title","","op_t"));

        $this->assign('docPath',$docPath);
        $this->assign('fileName',$fileName);
        $this->assign('title',$title);
        $this->display();
    }

}
