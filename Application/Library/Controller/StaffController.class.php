<?php

namespace Library\Controller;

use Think\Controller;

class StaffController extends Controller
{

    public function index()
    {
        $this->display();
    }

    public function ajaxGetStaffs()
    {
//        $data = array(
//            array('cover_url'=>null,'title' => 'Service Title', 'abstract' => 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Praesent vestibulum molestie lacus. Aenean nonummy hendrerit mauris. Phasellus porta. Fusce suscipit varius mi.'),
//            array('cover_url'=>null,'title' => 'Service Title', 'abstract' => 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Praesent vestibulum molestie lacus. Aenean nonummy hendrerit mauris. Phasellus porta. Fusce suscipit varius mi.'),
//            array('cover_url'=>null,'title' => 'Service Title', 'abstract' => 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Praesent vestibulum molestie lacus. Aenean nonummy hendrerit mauris. Phasellus porta. Fusce suscipit varius mi.'),
//            array('cover_url'=>null,'title' => 'Service Title', 'abstract' => 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Praesent vestibulum molestie lacus. Aenean nonummy hendrerit mauris. Phasellus porta. Fusce suscipit varius mi.'),
//        );
        $data=D('Staff')->getTopStaffs(2);
//        sdebug($data,'Staff');
        $this->ajaxReturn(array('list' => $data));
    }
}