<?php

namespace Library\Controller;

use Think\Controller;

class HomeController extends Controller
{
    public function index()
    {
       $this->display();
    }
}