<?php
return array(
    //模块名
    'name' => 'Library',
    //别名
    'alias' => 'Public Library',
    //版本号
    'version' => '1.0.0',
    //是否商业模块,1是，0，否
    'is_com' => 0,
    //是否显示在导航栏内？  1是，0否
    'show_nav' => 1,
    //模块描述
    'summary' => 'Public Library',
    //开发者
    'developer' => '青伢子',
    //开发者网站
    'website' => 'http://www.zengqs.com',
    //前台入口，可用U函数
    'entry' => 'Library/Home/index',
//    'admin_entry' => 'Library/Staffadmin/staffList',
    'icon' => 'file-text',
    'can_uninstall' => 1
);
