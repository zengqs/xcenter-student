<?php
return array(
    // 预先加载的标签库
    'TAGLIB_PRE_LOAD' => 'OT\\TagLib\\Article,OT\\TagLib\\Think',

    /* 主题设置 */
    'DEFAULT_THEME' => 'default', // 默认模板主题名称

    /* 模板相关配置 */
    'TMPL_PARSE_STRING' => array(

        //兼容旧版本
        '__ZUI__'     => __ROOT__ . '/Application/zui',

        //核心库
        '__CORE_IMAGE__'    => __ROOT__ . '/Application/Core/Static/images',
        '__CORE_CSS__'      => __ROOT__ . '/Application/Core/Static/css',
        '__CORE_JS__'       => __ROOT__ . '/Application/Core/Static/js',

        //模块相关的模板常量
        '__ROOT__'        => __ROOT__,
        '__APPLICATION__' => __ROOT__ . '/Application',
        '__MODULE__' => __ROOT__ . '/Application/' . MODULE_NAME,
        '__MODULE_STATIC__' => __ROOT__ . '/Application/' . MODULE_NAME . '/Static',
        '__LIBS__' => __ROOT__ . '/Application/' . MODULE_NAME . '/Static/libs',
        '__IMG__'  => __ROOT__ . '/Application/' . MODULE_NAME . '/Static/images',
        '__CSS__'  => __ROOT__ . '/Application/' . MODULE_NAME . '/Static/css',
        '__JS__'   => __ROOT__ . '/Application/' . MODULE_NAME . '/Static/js',
    ),
);
