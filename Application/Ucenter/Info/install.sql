
-- ----------------------------
-- Table structure for ocenter_db_customer_delivery_address
-- ----------------------------
DROP TABLE IF EXISTS `ocenter_member_delivery_address`;
CREATE TABLE `ocenter_member_delivery_address` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) NOT NULL COMMENT '用户id',
  `alias` VARCHAR(50) DEFAULT NULL COMMENT '别名，方便记忆',
  `receiver_name` varchar(20) DEFAULT NULL COMMENT '收货人',
  `telephone` varchar(20) DEFAULT NULL COMMENT '收货人电话',
  `province` varchar(50) DEFAULT NULL COMMENT '省(代码)',
  `city` varchar(50) DEFAULT NULL COMMENT '市(代码)',
  `district` varchar(50) DEFAULT NULL COMMENT '区县(代码)',
  `address1` varchar(100) DEFAULT NULL COMMENT '省市区地址',
  `address2` varchar(100) DEFAULT NULL COMMENT '收货地址',
  `is_default` INT(10) DEFAULT 0 COMMENT '是否默认',
  `status` INT(10) DEFAULT 1 COMMENT '状态',
  `create_time` INT(10)  DEFAULT NULL,
  `update_time` INT(10)  DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COMMENT='用户收货地址表';

INSERT INTO `ocenter_member_delivery_address` (`id`, `uid`, `alias`, `receiver_name`, `telephone`, `province`, `city`, `district`, `address1`, `address2`, `is_default`, `status`, `create_time`, `update_time`) VALUES ('1', '1', '家', '曾青松', '18928779564', '440000', '440100', '440113', '广东省广州市番禺区', '南城路698号', '1', '1', NULL, '1468996354');
INSERT INTO `ocenter_member_delivery_address` (`id`, `uid`, `alias`, `receiver_name`, `telephone`, `province`, `city`, `district`, `address1`, `address2`, `is_default`, `status`, `create_time`, `update_time`) VALUES ('2', '1', '单位', '曾青松', '18928779564', '440000', '440100', '440113', '广东省广州市番禺区', '市良路1342号', '0', '1', NULL, '1468996372');


DROP TABLE IF EXISTS `ocenter_member_bank_account`;
CREATE TABLE `ocenter_member_bank_account` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) NOT NULL COMMENT '用户id',
  `alias` VARCHAR(50) DEFAULT NULL COMMENT '账户别名，方便记忆',
  `name` VARCHAR(50)  NOT NULL COMMENT '户名',
  `account` VARCHAR(50)  NOT NULL COMMENT '账号',
  `account_type` int(11) NOT NULL COMMENT '账号类型：1:银联账号（储蓄卡），2，支付宝账号，3：微信支付账号',
  `bank_name` char(200) NOT NULL COMMENT '银行账号，支付宝和微信不填',

  `province` varchar(50) DEFAULT NULL COMMENT '开户行省(代码)，支付宝和微信不填',
  `city` varchar(50) DEFAULT NULL COMMENT '开户行市(代码)，支付宝和微信不填',
  `district` varchar(50) DEFAULT NULL COMMENT '区县(代码)',
  `address` varchar(100) DEFAULT NULL COMMENT '开户行省市地址，支付宝和微信不填',

  `is_default` INT(10) DEFAULT 0 COMMENT '是否默认账号',

  `status` INT(10) DEFAULT 1 COMMENT '状态',
  `update_time` INT(10)  NOT NULL,
  `create_time` INT(10)  NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COMMENT '用户银行账号表';

INSERT INTO `ocenter_member_bank_account` (`id`, `uid`, `alias`, `name`, `account`, `account_type`, `bank_name`, `province`, `city`, `address`, `is_default`, `status`, `update_time`, `create_time`) VALUES ('1', '1', '支付宝帐号', '曾青松', 'qingsongzeng@163.com', '2', '', '', NULL, NULL, '1', '1', '1468996372', '1468996372');



DROP TABLE IF EXISTS `ocenter_member_recharge_log`;
CREATE TABLE `ocenter_member_recharge_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) NOT NULL COMMENT '用户id',
  `channel` VARCHAR(50)  NOT NULL COMMENT '支付通道',
  `title` VARCHAR(50)  NOT NULL COMMENT '订单标题',
  `app_id` VARCHAR(50)  NOT NULL COMMENT 'APPID',
  `total_fee` DOUBLE  DEFAULT 0 COMMENT '充值金额，单位分',
  `bill_no` VARCHAR(50) DEFAULT '' COMMENT '订单编号',
  `account` VARCHAR(50)  NOT NULL COMMENT '账号',

  `module` VARCHAR(50) DEFAULT '' COMMENT '模块',

  `status` INT(10) DEFAULT 1 COMMENT '状态',
  `update_time` INT(10)  NOT NULL,
  `create_time` INT(10)  NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COMMENT '用户充值记录表';
