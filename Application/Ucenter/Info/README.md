# 用户中心扩展功能说明

增加用户的地址和账户管理

- [ ] 增加member_deliver_address表和member_bank_account
- [ ] 在个人中心增加对地址和帐号的管理入口
- [ ] 增加DeliverAddressController和BankAccountController，并增加相应的操作
- [ ] 安装脚本已经集成到Install模块的install.sql文件。


