<?php

namespace Ucenter\Model;


use Think\Model;


/**
 *
 * 用户的银行账号管理
 * Class BankAccountModel
 * @package Fenxiao\Model
 */
class RechargeLogModel extends Model
{

    protected $tableName = 'member_recharge_log';

    protected $_auto = array(
        array('status', '1', self::MODEL_INSERT),
        array('update_time', NOW_TIME, self::MODEL_BOTH),
        array('create_time', NOW_TIME, self::MODEL_INSERT),
    );

    // 自动验证设置
    protected $_validate = array(
        //http://document.thinkphp.cn/manual_3_2.html#auto_validate
        //array(验证字段1,验证规则,错误提示,[验证条件,附加规则,验证时间]),
//        array('province', 'require', '必须指定省！', self::MODEL_BOTH),
//        array('city', 'require', '邮箱格式错误！', self::MODEL_BOTH),
//        array('district', 'require', '必须指定省！', self::MODEL_BOTH),
    );

    /**
     * @param $data
     * @return mixed
     */
    public function appendLogItem($data)
    {
        //要根据订单号检测是否重复插入，入如果重复插入则忽略，因为支付回调函数也会插入日志记录
        $where['module'] = $data['module'];
        $where['bill_no'] = $data['bill_no'];
        $where['uid'] = $data['uid'];
        $item = $this->where($where)->find();
        if ($item) {
            //如果通过其它方式已经插入日志则返回该日志信息
            return $item;
        } else {

            $data['total_fee']= intval($data['total_fee'])/100; //转换为元

            $data['create_time'] = $data['update_time'] = time();
            $data['status'] = 1;
            $record_id= $this->data($data)->add();

            if ($record_id) {
                //记录积分，type=4表示余额

                $scoreModel = new ScoreModel();

                $user=query_user('username',$data['uid']);
                if ($user) {
                   
                    $remark = sprintf('%s在%s充值【余额：+%.2f元】,交易信息【订单号：%s】',$user['username'], date('Y-m-d H:i:s',time()), $data['total_fee'], $data['bill_no']);
                    $uids = array();
                    $uids[0] = $data['uid'];
                    $scoreModel->setUserScore($uids, $data['total_fee'], 4, 'inc', $data['module'], $record_id, $remark);
                }
            }
            return $record_id;
        }
    }
}




