<?php

namespace Ucenter\Model;

use Addons\ChinaCity\Model\DistrictModel;
use Think\Model;


/**
 *
 * 用户的银行账号管理
 * Class BankAccountModel
 * @package Fenxiao\Model
 */
class DeliverAddressModel extends Model
{

    protected $tableName = 'member_delivery_address';

    protected $_auto = array(
        array('status', '1', self::MODEL_INSERT),
        array('update_time', NOW_TIME, self::MODEL_BOTH),
        array('create_time', NOW_TIME, self::MODEL_INSERT),
        array('is_default', 0, self::MODEL_INSERT),
    );

    // 自动验证设置
    protected $_validate = array(
        //http://document.thinkphp.cn/manual_3_2.html#auto_validate
        //array(验证字段1,验证规则,错误提示,[验证条件,附加规则,验证时间]),
        array('province', 'require', '必须指定省！', self::MODEL_BOTH),
        array('city', 'require', '邮箱格式错误！', self::MODEL_BOTH),
        array('district', 'require', '必须指定省！', self::MODEL_BOTH),
    );


    /**
     * 更新地址信息
     * @param int $id
     * @param int $province
     * @param int $city
     * @param int $district
     * @return bool
     */
    public function updateAddress($id = 0, $province = 0, $city = 0, $district = 0)
    {
        $data = $this->find($id);
        $result = false;
        if ($data) {
            //传递的是省市区的代码
            $data['province'] = $province;
            $data['city'] = $city;
            $data['distinct'] = $district;

            $districtModel = new DistrictModel();
            //查表
            $province = $districtModel->where(array('id' => $data['province'], 'level' => 1))->getField('name');
            $city = $districtModel->where(array('id' => $data['city'], 'level' => 2))->getField('name');
            $district = $districtModel->where(array('id' => $data['district'], 'level' => 3))->getField('name');
            $data['address1'] = sprintf('%s%s%s', $province, $city, $district);

            $result = $this->save($data);
        }
        return $result;
    }

    public function clearDefaultAddress($uid)
    {
        //清除默认地址
        return $this->where(array('uid' => $uid))->setField('is_default', 0);
    }
}




