<?php

namespace Ucenter\Controller;


use Think\Controller;

class BankAccountController extends BaseController
{
//->keyMap('account_type', '账号类型', array('1' => '银联账号（储蓄卡）', '2' => '支付宝账号', '3' => '微信支付账号'))

    function index($page = 1)
    {
        $r = C('LIST_ROWS');
        $map = array();

        $map['uid'] = is_login();

        $model = M('member_bank_account');
        $list = $model->where($map)->order('is_default desc,create_time desc')->page($page, $r)->select();
        $totalCount = $model->where($map)->count();
        $this->assign('totalCount', $totalCount);
        $this->assign('list', $list);
        $this->display();
    }


    public function delete()
    {
        $id = I('id', 0, 'intval');
        $row = M('member_bank_account')->find($id);

        if ($row['uid'] == is_login()) {
            M('member_bank_account')->where(array('id' => $id))->delete();
            $this->success('彻底删除成功', U('index'));
        }

    }

    public function edit()
    {

        $uid = get_uid();

        $data = array();
        $aId = I('id', 0);

        $title = $aId ? "编辑" : "新增";
        if (IS_POST ) {
//            $aId = I('post.id', 0);
            $aId && $data['id'] = $aId;

            $uid=get_uid();

            if ($aId) {
                $row = M('member_bank_account')->find($aId);
                if ($row['uid'] != $uid) {
                    $this->error('非法操作，只能编辑用户自己的信息。', U('index'));
                }
            }

            $data['uid']=$uid;
            $data['is_default']=I('is_default')=='on'?1:0;

            $data['alias'] = I('post.alias');
            $data['name'] = I('post.name');
            $data['account'] = I('post.account');
            $data['account_type'] = I('post.account_type');
            $data['bank_name'] = I('post.bank_name');

            $data['province'] = I('post.province');
            $data['city'] = I('post.city');
            $data['district'] = I('post.district');
//            $data['address'] = I('post.address');

            $data['status'] = 1;

            $model=new \Ucenter\Model\BankAccountModel();

            if ($data['is_default']){
                $model->clearDefaultAccount($uid);
            }

            if (!$aId) {//新增
                $data['update_time'] = $data['create_time'] = time();
                $result = $model->data($data)->add();
                $model->updateAddress($result,$data['province'],$data['city'],$data['district']);
            } else {//更新
                $data['update_time'] = time();
                $result = $model->data($data)->save();
                //将地址代码翻译成文字地址
                $model->updateAddress($data['id'],$data['province'],$data['city'],$data['district']);
            }

            if ($result) {
                $this->success($title . '成功！', U('index'));
            } else {
                $this->error($title . '失败！');
            }
        } else {
            //编辑或者新增
            if ($aId) { //编辑
                $data = M('member_bank_account')->find($aId);

                if ($data['uid'] != $uid) {
                    $this->error('非法操作，只能编辑用户自己的信息。', U('index'));
                }

            } else { //新增
                $data['province'] = '440000';//默认省份
                $data['city'] = '440100';//
                $data['uid'] = $uid;
            }
            $this->assign('data', $data);
            $this->display();
        }
    }


}