<?php

namespace Ucenter\Controller;

use Addons\ChinaCity\Model\DistrictModel;
use Admin\Builder\AdminListBuilder;
use Think\Controller;
use Ucenter\Model\DeliverAddressModel;
use User\Model\UcenterMemberModel;

class DeliverAddressController extends BaseController
{

    public function edit()
    {

        $uid = get_uid();

        $data = array();
        $aId = I('id', 0);

        $title = $aId ? "编辑" : "新增";
        if (IS_POST ) {
            $aId && $data['id'] = $aId;

            $uid=get_uid();

            if ($aId) {
                $row = M('member_delivery_address')->find($aId);
                if ($row['uid'] != $uid) {
                    $this->error('非法操作，只能编辑用户自己的信息。', U('index'));
                }
            }

            $data['uid']=$uid;
            $data['is_default']=I('is_default')=='on'?1:0;

            $data['alias'] = I('post.alias');
            $data['receiver_name'] = I('post.receiver_name');
            $data['telephone'] = I('post.telephone');

            $data['province'] = I('post.province');
            $data['city'] = I('post.city');
            $data['district'] = I('post.district');
            $data['address2'] = I('post.address2');

            $data['status'] = 1;

            $model=new DeliverAddressModel();
           
            if ($data['is_default']){
                $model->clearDefaultAddress($uid);
            }

            if (!$aId) {//新增
                $data['update_time'] = $data['create_time'] = time();
                $result = $model->data($data)->add();
                $model->updateAddress($result,$data['province'],$data['city'],$data['district']);
            } else {//更新
                $data['update_time'] = time();
                $result = $model->data($data)->save();
                //将地址代码翻译成文字地址
                $model->updateAddress($data['id'],$data['province'],$data['city'],$data['district']);
            }

            if ($result) {
                $this->success($title . '成功！', U('index'));
            } else {
                $this->error($title . '失败！');
            }
        } else {
            //编辑或者新增
            if ($aId) { //编辑
                $data = M('member_delivery_address')->find($aId);

                if ($data['uid'] != $uid) {
                    $this->error('非法操作，只能编辑用户自己的信息。', U('index'));
                }

            } else { //新增
                $data['province'] = '440000';//默认省份
                $data['city'] = '440100';//
                $data['uid'] = $uid;
            }
            $this->assign('data', $data);
            $this->display();
        }
    }

    public function delete()
    {
        $id = I('id', 0, 'intval');
        $row = M('member_delivery_address')->find($id);

        if ($row['uid'] == is_login()) {
            M('member_delivery_address')->where(array('id' => $id))->delete();
            $this->success('彻底删除成功', U('index'));
        }

    }

    /**
     * 收货地址管理
     * @param null $uid
     * @param int $page
     */
    function index($page = 1)
    {
        $r = C('LIST_ROWS');
        $map = array();

        $map['uid'] = is_login();

        $model = M('member_delivery_address');
        $list = $model->where($map)->order('is_default desc,create_time desc')->page($page, $r)->select();
        $totalCount = $model->where($map)->count();
        $this->assign('totalCount', $totalCount);
        $this->assign('list', $list);
        $this->display();
    }
}