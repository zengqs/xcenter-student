<?php
namespace Common\Widget;

use Think\Controller;

class MarkdownEditorWidget extends Controller
{

    //        {:W('Common/MarkdownEditor/editor',array($field['name'],$field['name'],$field['value'],$field['style']['width'],$field['style']['height'],$field['config']))}
    public function editor($id = 'myeditor', $name = 'content', $default = '', $width = '100%', $height = '500px',$config='')
    {

        if (empty($config)){
            $config='simple';
        }

        if (!in_array($config,array('mini','full','simple'))){
            $config='simple';
        }

        $this->assign('id', $id);
        $this->assign('name', $name);
        $this->assign('default', $default);
        $this->assign('width', $width);
        $this->assign('height', $height);
        $this->assign('config',$config);

        $this->display(T('Application://Common@Widget/markdowneditor'));
    }

    //Markdownyuedu  阅读器
    public function reader($id = 'myeditor', $name = 'content', $default = '', $param = '')
    {

        $default = htmlentities($default);//转换
        $this->assign('id', $id);
        $this->assign('name', $name);
        $this->assign('default', $default);
        $this->assign('param',$param);


        $this->display(T('Application://Common@Widget/markdownreader'));
    }
}
