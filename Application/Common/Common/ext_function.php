<?php
/**
 * Created by PhpStorm.
 * User: zengqs
 * Date: 2016/7/20
 * Time: 0:38
 */


/** 获取当前时间戳，精确到毫秒 */
//使用方法：
//1. 获取当前时间戳(精确到毫秒)：microtime_float()
//2. 时间戳转换时间：microtime_format('Y年m月d日 H时i分s秒 x毫秒', 1270626578.66000000)
//2. 时间戳转换时间：microtime_format('Y:m:d H:i:s.x', 1270626578.66000000)
//        $micro_date = microtime(true);
//        var_dump($micro_date);
//        $micro_date=microtime_float();
//        var_dump($micro_date);
//        $date= microtime_format('Y:m:d H:i:s.x', $micro_date);
//        var_dump($date);

/**
 * 获取当前时间戳，精确到毫秒
 *
 * @return float  等价于microtime(true)
 */
if (!function_exists('microtime_float')) {
    function microtime_float() //等价于microtime(true)
    {
        list($usec, $sec) = explode(" ", microtime());

        return ((float)$usec + (float)$sec);
    }
}


/**
 * 格式化时间戳，精确到毫秒，x代表毫秒
 * $micro_date = microtime(true);
 * $date= microtime_format('Y:m:d H:i:s.x', $micro_date);
 */
if (!function_exists('microtime_format')) {
    function microtime_format($tag, $time)
    {
        list($usec, $sec) = explode(".", $time);
        $date = date($tag, $usec);

        return str_replace('x', $sec, $date);
    }
}


if (!function_exists('ext_get_current_page_url')) {
    function ext_get_current_page_url()
    {
        $pageURL = 'http';

        if ($_SERVER["HTTPS"] == "on") {
            $pageURL .= "s";
        }
        $pageURL .= "://";

        if ($_SERVER["SERVER_PORT"] != "80") {
            $pageURL .= $_SERVER["SERVER_NAME"] . ":" . $_SERVER["SERVER_PORT"] . $_SERVER["REQUEST_URI"];
        } else {
            $pageURL .= $_SERVER["SERVER_NAME"] . $_SERVER["REQUEST_URI"];
        }

        return $pageURL;
    }
}
/**
 * @param bool $include_root 是否包含网站应用部署的子目录，一般设置为false
 * @param string $path 拼接在网站URL下的路由，系统会处理多余的"/"，如：
 *                             ext_get_site_root_url(false,'/Public/images/default_avatar_128_128.jpg') 返回类似于：
 *                             http://www.zengqs.com/Public/images/default_avatar_128_128.jpg
 *
 * @return string 网站运行的实际的http地址,包含部署的子目录，如：http://www.zengqs.com
 */
if (!function_exists('ext_get_site_root_url')) {
    function ext_get_site_root_url($include_root = false, $path = '')
    {
        $http = 'http';

        if ($_SERVER["HTTPS"] == "on") {
            $http .= "s";
        }
        $http .= "://";

        $url = '';
        if ($_SERVER["SERVER_PORT"] != "80") {
            $url .= $_SERVER["SERVER_NAME"] . ":" . $_SERVER["SERVER_PORT"];
        } else {
            $url .= $_SERVER["SERVER_NAME"];
        }

        if ($include_root) {
            $url .= '/' . getRootUrl();
        }

        $url = $url . '/' . $path;
        $url = str_replace('//', '/', $url);
        $url = str_replace('//', '/', $url);

        //要去掉最后一个"/"
        if (substr($url, strlen($url) - 1) == '/') {
            $url = substr($url, 0, strlen($url) - 1);
        }

        return $http . $url;
    }
}

/***
 * @param        $image_id
 * @param string $field
 *
 * @return 返回指定图片ID的完整路径或者数据库表的记录，默认返回完整路径
 * @author 曾青松
 */
if (!function_exists('get_image_by_id')) {
    function get_image_by_id($image_id, $field = 'full_path')
    {
        if (empty($image_id)) {
            return null;
        }
        if (is_numeric($image_id)) {
            $image_id = intval($image_id);
        } else {
            return null;
        }

        $picture = M('picture')->where(array('status' => 1, 'id' => $image_id))->find();
        //$picture = M('Picture')->getById($image_id);    //图片不存在返回NULL

//    sdebug($picture,'Call function get_image_by_id',__FILE__,__LINE__);

        if (!is_null($picture)) {
            $picture['path'] = get_pic_src($picture['path']);

            ////获取当前的域名:
            if ('local' == strtolower($picture['type'])) {
                //echo $_SERVER['SERVER_NAME'];
                $path = $_SERVER['HTTP_HOST'];//获取当前域名

                //$path=str_ireplace(str_replace("/","\\",$_SERVER['PHP_SELF']),'',__FILE__)."\\";
                $picture['full_path'] = 'http://' . str_replace('//', '/', $path . '/' . $picture['path']);
            } else {
                //使用其他的图片上传驱动程序上传的图片，需要调用插件来处理路径
                $picture['full_path'] = $picture['path'];
                //TODO 调用插件来处理，参考getThumbImageById函数
            }

            return empty($field) ? $picture : $picture[$field];
        } else {
            return null;
        }
    }
}

/**
 * @param            $cover_id
 * @param int $width
 * @param string $height
 * @param int $type
 * @param bool|false $replace
 * @param string $no_pic 图片不存在的时候默认的图片
 *
 * @return mixed|string
 */
if (!function_exists('get_thumb_image_by_id')) {
    function get_thumb_image_by_id($cover_id, $width = 100, $height = 'auto', $type = 0, $replace = false, $no_pic = '/Public/images/nopic.png')
    {
        $picture = S('picture_' . $cover_id);
        if (empty($picture)) {
            $picture = M('Picture')->where(array('status' => 1))->getById($cover_id);
            S('picture_' . $cover_id, $picture);
        }
        if (empty($picture)) {
            //return get_pic_src('Public/images/nopic.png');
            //BUGFIXED
            //原始代码不能得到一致大小的缩略图

            $attach = getThumbImage($no_pic, $width, $height, $type, $replace);

            $path = $_SERVER['HTTP_HOST'];//获取当前域名
            return 'http://' . str_replace('//', '/', $path . '/' . get_pic_src($attach['src']));
            // return get_pic_src($attach['src']);
        }

        if ($picture['type'] == 'local') {
            $attach = getThumbImage($picture['path'], $width, $height, $type, $replace);
            // return get_pic_src($attach['src']);

            $path = $_SERVER['HTTP_HOST'];//获取当前域名
            return 'http://' . str_replace('//', '/', $path . '/' . get_pic_src($attach['src']));
            // return  $path. get_pic_src($attach['src']);
        } else {
            $new_img = $picture['path'];
            $name = get_addon_class($picture['type']);
            if (class_exists($name)) {
                $class = new $name();
                if (method_exists($class, 'thumb')) {
                    $new_img = $class->thumb($picture['path'], $width, $height, $type, $replace);
                }
            }

            return get_pic_src($new_img);
        }

    }
}


/**
 * @param        $image_ids
 * @param string $field
 * @param string $no_pic 图片不存在的时候默认的图片
 *
 * @return array|null
 */
if (!function_exists('get_image_by_ids')) {
    function get_image_by_ids($image_ids, $field = 'full_path', $no_pic = '/Public/images/nopic.png')
    {
        if (empty($image_ids)) return null;

        $ids = is_array($image_ids) ? $image_ids : explode(',', $image_ids);


        $list = array();
        foreach ($ids as $id) {
            $pic = get_image_by_id($id, $field, $no_pic);

            if (!is_null($pic)) {
                array_push($list, $pic);
            }

        }

        return $list;
    }
}


if (!function_exists('get_thumb_image_by_ids')) {
    function get_thumb_image_by_ids($image_ids, $width = 100, $height = 'auto', $type = 0, $replace = false)
    {
        if (empty($image_ids)) return null;

        $ids = is_array($image_ids) ? $image_ids : explode(',', $image_ids);
        $list = array();
        foreach ($ids as $id) {
            $pic = get_thumb_image_by_id($id, $width, $height, $type, $replace);
            array_push($list, $pic);
        }

        return $list;
    }
}

if (!function_exists('get_user_address')) {

    function get_user_address($user_IP = null)
    {
        // $user_IP = ($_SERVER["HTTP_VIA"]) ? $_SERVER["HTTP_X_FORWARDED_FOR"] : $_SERVER["REMOTE_ADDR"];
        //  $user_IP = ($user_IP) ? $user_IP : $_SERVER["REMOTE_ADDR"];

        if (is_null($user_IP)) {
            $user_IP = get_client_ip();
        }
        $URL = 'http://ip.taobao.com/service/getIpInfo.php?ip=' . $user_IP;
        $fcontents = file_get_contents("$URL");
        $contents = json_decode($fcontents);

        $address = $contents->data->country . $contents->data->area . $contents->data->region . $contents->data->city;

        //echo '您来自: '.$contents->data->country.$contents->data->area.$contents->data->region.$contents->data->city.'  ip为'.$contents->data->ip;

        return $address;
    }
}

if (!function_exists('downloadImage')) {

    function downloadImage($url)
    {
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_NOBODY, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $package = curl_exec($ch);
        $httpinfo = curl_getinfo($ch);
        curl_close($ch);

        return array_merge(array('data' => $package), array('header' => $httpinfo));
    }
}


if (!function_exists('uploadImage')) {

//上传图片
    function uploadImage($_file)
    {
        $subName = date("Y-m-d", time());
        $config = array(
            'rootPath' => getRootUrl() . 'Uploads/', //保存根路径
            'savePath' => 'ShareOrder/', //保存路径
            'maxSize' => 3145728, // 设置附件上传大小
            'exts' => array('jpg', 'gif', 'png', 'jpeg'), // 设置附件上传类型
            'subName' => $subName,
        );
        $upload = new \Think\Upload($config);// 实例化上传类
        // 上传文件
        $info = $upload->uploadOne($_file);
        if (!$info) {// 上传错误提示错误信息
            // $this->error($upload->getError());
            return array('res' => 0, 'info' => $upload->getError());
        } else {// 上传成功
            // $info['subName'] = $subName."/";
            return array('res' => 1, 'info' => $info);
        }
    }
}


if (!function_exists('check_mobile')) {

//手机验证
    function check_mobile($mobilephone)
    {
        if (preg_match("/^13[0-9]{1}[0-9]{8}$|15[0189]{1}[0-9]{8}$|189[0-9]{8}$/", $mobilephone)) {
            return true;
        } else {
            return false;
        }
    }
}


if (!function_exists('check_email')) {

//邮箱验证
    function check_email($email)
    {
        if (preg_match("/^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/", $email)) {
            return true;
        } else {
            return false;
        }
    }
}


if (!function_exists('array_column')) {
    function array_column(array $input, $columnKey, $indexKey = null)
    {
        $result = array();
        if (null === $indexKey) {
            if (null === $columnKey) {
                $result = array_values($input);
            } else {
                foreach ($input as $row) {
                    $result[] = $row[$columnKey];
                }
            }
        } else {
            if (null === $columnKey) {
                foreach ($input as $row) {
                    $result[$row[$indexKey]] = $row;
                }
            } else {
                foreach ($input as $row) {
                    $result[$row[$indexKey]] = $row[$columnKey];
                }
            }
        }

        return $result;
    }
}

/**
 * @param $arr
 * @param $key_name
 *
 * @return array
 * 将数据库中查出的列表以指定的 id 作为数组的键名
 */
if (!function_exists('convert_arr_key')) {

    function convert_arr_key($arr, $key_name)
    {
        $arr2 = array();
        foreach ($arr as $key => $val) {
            $arr2[$val[$key_name]] = $val;
        }

        return $arr2;
    }
}


/**
 * 执行SQL文件
 */
if (!function_exists('execute_sql_file')) {

    function execute_sql_file($sql_path)
    {
        // 读取SQL文件
        $sql = file_get_contents($sql_path);
        $sql = str_replace("\r", "\n", $sql);
        $sql = explode(";\n", $sql);

        // 替换表前缀
        $orginal = 'ocenter_';
        $prefix = C('DB_PREFIX');
        $sql = str_replace("{$orginal}", "{$prefix}", $sql);

        // 开始安装
        foreach ($sql as $value) {
            $value = trim($value);
            if (empty ($value))
                continue;

            $res = M()->execute($value);
            // dump($res);
            // dump(M()->getLastSql());
        }
    }
}


if (!function_exists('object_array')) {
    /**
     * @param $array
     * 将PHP Std Class Object转换为 Array对象
     * @return array
     */
    function object_array($array)
    {
        if (is_object($array)) {
            $array = (array)$array;
        }
        if (is_array($array)) {
            foreach ($array as $key => $value) {
                $array[$key] = object_array($value);
            }
        }
        return $array;
    }
}


if (!function_exists('arrayToXml')) {


//数组转XML
    function arrayToXml($arr)
    {
        $xml = "<xml>";
        foreach ($arr as $key => $val) {
            if (is_numeric($val)) {
                $xml .= "<" . $key . ">" . $val . "</" . $key . ">";
            } else {
                $xml .= "<" . $key . "><![CDATA[" . $val . "]]></" . $key . ">";
            }
        }
        $xml .= "</xml>";
        return $xml;
    }
}
if (!function_exists('xmlToArray')) {


//将XML转为array
    function xmlToArray($xml)
    {
        //禁止引用外部xml实体
        libxml_disable_entity_loader(true);
        $values = json_decode(json_encode(simplexml_load_string($xml, 'SimpleXMLElement', LIBXML_NOCDATA)), true);
        return $values;
    }
}
