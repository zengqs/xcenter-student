<?php

namespace Common\Model;

/**
 * 全国城市乡镇信息模型
 */
class DistrictModel extends \Think\Model
{
    //调用配置文件中的数据库配置1
    public function _initialize()
    {

    }

    /**
     * @return mixed 返回全国身份信息，包含:id,name,upid字段，使用标准的6位代码，不足6位后缀补0对齐
     *
     */
    public function getProvinceList()
    {
        //使用缓存
        $province_data = S('DISTRICT_PROVINCE_LIST');
        if (empty($province_data)) {
            $order = 'id ASC';
            $map['level'] = 1;
            $map['upid'] = 0;
            $province_data = $this->where($map)->field('id,name,upid')->order($order)->select();
            S('DISTRICT_PROVINCE_LIST', $province_data);
        }

        return $province_data;
    }


    /**
     * @param $pid 省的代码，使用标准的6位代码，不足6位后缀补0对齐
     *
     * @return mixed 指定省份的全部市、区信息
     */
    public function getCityList($pid)
    {
        $order = 'id ASC';
        $map['level'] = 2;
        $map['upid'] = $pid;
        $data = $this->where($map)->field('id,name,upid')->order($order)->select();

        return $data;
    }

    /**
     * @param $cid 城市代码，使用标准的6位代码，不足6位后缀补0对齐
     *
     * @return mixed 获取区县市信息
     */
    public function getDistrict($cid)
    {
        $order = 'id ASC';
        $map['level'] = 3;
        $map['upid'] = $cid;
        $data = $this->where($map)->field('id,name,upid')->order($order)->select();

        return $data;
    }

    /**
     * @param bool $add_all_city_district_item 是否包含输出全部城市和全部区县的选项
     *
     * @return array|mixed 输出3级城市列表的JSON数据，具体参考mui的city.data-3.js文件的格式
     */
    function getCityData3($add_all_city_district_item = true)
    {
        //使用缓存
        if ($add_all_city_district_item) {
            $province_data = S('COMMON_DISTRICT_CITY_DATA_3_ADD_ALL_CITY_DISTRICT_ITEM');

            if (empty($province_data)) {

                $province_data = array();
                $DistrictModel = new DistrictModel();
                $provinceList = $DistrictModel->getProvinceList();
                foreach ($provinceList as $province) {
                    $city_data = array();
                    $city_data[] = array('value' => '', 'text' => '全部城市', 'children' => array(array('value' => '', 'text' => '全部区县')));

                    $cityList = $DistrictModel->getCityList($province['id']);
                    foreach ($cityList as $city) {
                        $districtList = $DistrictModel->getDistrict($city['id']);
                        $district_items = array();
                        $district_items[] = array('value' => '', 'text' => '全部区县');
                        foreach ($districtList as $district) {
                            $district_items[] = array('value' => $district['id'], 'text' => $district['name']);
                        }
                        $city_data[] = array('value' => $city['id'], 'text' => $city['name'], 'children' => $district_items);;
                    }
                    $province_data[] = array('value' => $province['id'], 'text' => $province['name'], 'children' => $city_data);
                }
                S('CITY_DATA_3', $province_data);
            }
        } else {
            $province_data = S('COMMON_DISTRICT_CITY_DATA_3');

            if (empty($province_data)) {

                $province_data = array();
                $DistrictModel = new DistrictModel();
                $provinceList = $DistrictModel->getProvinceList();
                foreach ($provinceList as $province) {
                    $city_data = array();
                    $cityList = $DistrictModel->getCityList($province['id']);
                    foreach ($cityList as $city) {
                        $districtList = $DistrictModel->getDistrict($city['id']);
                        $district_items = array();
                        foreach ($districtList as $district) {
                            $district_items[] = array('value' => $district['id'], 'text' => $district['name']);
                        }
                        $city_data[] = array('value' => $city['id'], 'text' => $city['name'], 'children' => $district_items);;
                    }
                    $province_data[] = array('value' => $province['id'], 'text' => $province['name'], 'children' => $city_data);
                }
                S('CITY_DATA_3', $province_data);
            }
        }


//        sdebug($province_data,'ajaxGetCityData3',__FILE__,__LINE__);
        return $province_data;
    }
}
