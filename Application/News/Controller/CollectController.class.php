<?php
namespace News\Controller;

use Admin\Controller\AdminController;
use News\Logic\CollectLogic;

use Vendor\AjaxPage;


//后台数据采集控制器
class CollectController extends AdminController
{

    /**
     * 采集配置文件
     */
    public function config()
    {

    }

    function qqEduNewsList()
    {

        //系统支持的采集通道
        $categoryList = array(
//            array('id' => 'QqEduNews', 'title' => '腾讯教育频道'),
//            array('id' => 'GzpypEduNews', 'title' => '广州番禺职业技术学院'),
        );
        $this->assign('categoryList', $categoryList);
        $this->display(T('Application://News@Collect/qqedunews_list'));
    }


    function ajaxQQEduNewsList()
    {
        $p = I('get.p', 1, 'intval');

        $where = ' 1 = 1 '; // 搜索条件


        // 关键词搜索，模糊匹配
        $key_word = I('key_word') ? trim(I('key_word')) : '';
        if ($key_word) {
            $where = "$where and (title like '%$key_word%')";
        }

        $model = D('News/News');
        $count = $model->where($where)->count();
        $Page = new AjaxPage($count, C('LIST_ROWS'), 'get_page', '', 'p');

        //搜索条件下 分页赋值
//        foreach ($condition as $key => $val) {
//            $Page->parameter[$key] = urlencode($val);
//        }


        $show = $Page->show();
        if (!empty(I('orderby1')) && !empty(I('orderby2'))) {
            $order_str = '`' . I('orderby1') . '` ' . I('orderby2');
        } else {
            $order_str = array();
        }
        $courseList = $model->where($where)->order($order_str)->page($p, C('LIST_ROWS'))->select();

        $categoryList = D('NewsCategory')->field('id,title')->select();

        $categoryList = convert_arr_key($categoryList, 'id');

        $this->assign('categoryList', $categoryList);
        $this->assign('newsList', $courseList);
        $this->assign('page', $show);// 赋值分页输出

        $html = $this->fetch(T('Application://News@Collect/ajax_qqedunews_list'));
        $this->ajaxReturn($html);
    }

    public function ajaxCollectAgent($id = '')
    {
        //采集的通道
        $data = null;
        if (empty($id)){
            //采集所有的通道
            $model=new CollectLogic();
            $model->collectAllNews();
        }
//        if($id=='QqEduNews'){
//            $model=new CollectLogic();
//            $model->collectQQEduNews();
//        }elseif($id=='GzpypEduNews'){
//            $model=new CollectLogic();
//            $model->collectGzpypEduNews();
//        }

        $this->ajaxReturn(
            array(
                'status' => 1,
                'data'   => null,
            )
        );
    }

}
