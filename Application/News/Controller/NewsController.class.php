<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 15-4-27
 * Time: 上午10:21
 * @author 郑钟良<zzl@ourstu.com>
 */

namespace News\Controller;


use Admin\Builder\AdminConfigBuilder;
use Admin\Builder\AdminListBuilder;
use Admin\Builder\AdminTreeListBuilder;
use Admin\Controller\AdminController;
use Common\Model\ContentHandlerModel;
use News\Tools\CleanHtml;

class NewsController extends AdminController {

    protected $newsModel;
    protected $newsDetailModel;
    protected $newsCategoryModel;

    function _initialize()
    {
        parent::_initialize();
        $this->newsModel = D('News/News');
        $this->newsDetailModel = D('News/NewsDetail');
        $this->newsCategoryModel = D('News/NewsCategory');
    }

    public function newsCategory()
    {
        //显示页面
        $builder = new AdminTreeListBuilder();

        $tree = $this->newsCategoryModel->getTree(0, 'id,title,sort,pid,status');

        $builder->title(L('_CATEGORY_MANAGER_'))
            ->suggest(L('_CATEGORY_MANAGER_SUGGEST_'))
            ->buttonNew(U('News/add'))
            ->data($tree)
            ->display();
    }

    /**分类添加
     * @param int $id
     * @param int $pid
     * @author 郑钟良<zzl@ourstu.com>
     */
    public function add($id = 0, $pid = 0)
    {
        $title=$id?L('_EDIT_'):L('_ADD_');
        if (IS_POST) {
            if ($this->newsCategoryModel->editData()) {
                S('SHOW_EDIT_BUTTON',null);
                $this->success($title.L('_SUCCESS_'), U('News/newsCategory'));
            } else {
                $this->error($title.L('_FAIL_').$this->newsCategoryModel->getError());
            }
        } else {
            $builder = new AdminConfigBuilder();

            if ($id != 0) {
                $data = $this->newsCategoryModel->find($id);
            } else {
                $father_category_pid=$this->newsCategoryModel->where(array('id'=>$pid))->getField('pid');
                if($father_category_pid!=0){
                    $this->error(L('_ERROR_CATEGORY_HIERARCHY_'));
                }
            }
            if($pid!=0){
                $categorys = $this->newsCategoryModel->where(array('pid'=>0,'status'=>array('egt',0)))->select();
            }
            $opt = array();
            foreach ($categorys as $category) {
                $opt[$category['id']] = $category['title'];
            }
            $builder->title($title.L('_CATEGORY_'))
                ->data($data)
                ->keyId()->keyText('title', L('_TITLE_'))
                ->keySelect('pid',L('_FATHER_CLASS_'), L('_FATHER_CLASS_SELECT_'), array('0' =>L('_TOP_CLASS_')) + $opt)->keyDefault('pid',$pid)
                ->keySingleImage('cover',L('_COVER_'))
                ->keyRadio('can_post',L('_PLAY_YN_'),'',array(0=>L('_NO_'),1=>L('_YES_')))->keyDefault('can_post',1)
                ->keyRadio('need_audit',L('_PLAY_YN_AUDIT_'),'',array(0=>L('_NO_'),1=>L('_YES_')))->keyDefault('need_audit',1)
                ->keyInteger('sort',L('_SORT_'))->keyDefault('sort',0)
                ->keyStatus()->keyDefault('status',1)
                ->buttonSubmit(U('News/add'))->buttonBack()
                ->display();
        }

    }

    /**
     * 设置资讯分类状态：删除=-1，禁用=0，启用=1
     * @param $ids
     * @param $status
     * @author 郑钟良<zzl@ourstu.com>
     */
    public function setStatus($ids, $status)
    {
        !is_array($ids)&&$ids=explode(',',$ids);
        if(in_array(1,$ids)){
            $this->error(L('_ERROR_CANNOT_'));
        }
        if($status==0||$status==-1){
            $map['category']=array('in',$ids);
            $this->newsModel->where($map)->setField('category',1);
        }
        $builder = new AdminListBuilder();
        $builder->doSetStatus('newsCategory', $ids, $status);
    }
//分类管理end

    public function config()
    {
        $builder=new AdminConfigBuilder();
        $data=$builder->handleConfig();
        $default_position=<<<str
1:系统首页
2:推荐阅读
4:本类推荐
str;

        $builder->title(L('_NEWS_BASIC_CONF_'))
            ->data($data);

        $builder->keyTextArea('NEWS_SHOW_POSITION',L('_GALLERY_CONF_'))->keyDefault('NEWS_SHOW_POSITION',$default_position)
            ->keyRadio('NEWS_ORDER_FIELD',L('_FRONT_LIST_SORT_'),L('_SORT_RULE_'),array('view'=>L('_VIEWS_'),'create_time'=>L('_CREATE_TIME_'),'update_time'=>L('_UPDATE_TIME_')))->keyDefault('NEWS_ORDER_FIELD','create_time')
            ->keyRadio('NEWS_ORDER_TYPE',L('_LIST_SORT_STYLE_'),'',array('asc'=>L('_ASC_'),'desc'=>L('_DESC_')))->keyDefault('NEWS_ORDER_TYPE','desc')
            ->keyInteger('NEWS_PAGE_NUM','',L('_LIST_IN_PAGE_'))->keyDefault('NEWS_PAGE_NUM','20')

            ->keyText('NEWS_SHOW_TITLE', L('_TITLE_NAME_'), L('_HOME_BLOCK_TITLE_'))->keyDefault('NEWS_SHOW_TITLE',L('_HOT_NEWS_'))
            ->keyText('NEWS_SHOW_COUNT', L('_NEWS_SHOWS_'), L('_TIP_NEWS_ARISE_'))->keyDefault('NEWS_SHOW_COUNT',4)
            ->keyRadio('NEWS_SHOW_TYPE', L('_NEWS_SCREEN_'), '', array('1' => L('_BG_RECOMMEND_'), '0' => L('_EVERYTHING_')))->keyDefault('NEWS_SHOW_TYPE',0)
            ->keyRadio('NEWS_SHOW_ORDER_FIELD', L('_SORT_VALUE_'), L('_TIP_SORT_VALUE_'), array('view' => L('_VIEWS_'), 'create_time' => L('_DELIVER_TIME_'), 'update_time' => L('_UPDATE_TIME_')))->keyDefault('NEWS_SHOW_ORDER_FIELD','view')
            ->keyRadio('NEWS_SHOW_ORDER_TYPE', L('_SORT_TYPE_'), L('_TIP_SORT_TYPE_'), array('desc' => L('_COUNTER_'), 'asc' => L('_DIRECT_')))->keyDefault('NEWS_SHOW_ORDER_TYPE','desc')
            ->keyText('NEWS_SHOW_CACHE_TIME', L('_CACHE_TIME_'),L('_TIP_CACHE_TIME_'))->keyDefault('NEWS_SHOW_CACHE_TIME','600')

            ->group(L('_BASIC_CONF_'), 'NEWS_SHOW_POSITION,NEWS_ORDER_FIELD,NEWS_ORDER_TYPE,NEWS_PAGE_NUM')
            ->group(L('_HOME_SHOW_CONF_'), 'NEWS_SHOW_COUNT,NEWS_SHOW_TITLE,NEWS_SHOW_TYPE,NEWS_SHOW_ORDER_TYPE,NEWS_SHOW_ORDER_FIELD,NEWS_SHOW_CACHE_TIME')
            ->groupLocalComment(L('_LOCAL_COMMENT_CONF_'),'index')
            ->buttonSubmit()->buttonBack()
            ->display();
    }


    //资讯列表start
    public function index($page=1,$r=20)
    {
        $aCate=I('cate',0,'intval');
        if($aCate){
            $cates=$this->newsCategoryModel->getCategoryList(array('pid'=>$aCate));
            if(count($cates)){
                $cates=array_column($cates,'id');
                $cates=array_merge(array($aCate),$cates);
                $map['category']=array('in',$cates);
            }else{
                $map['category']=$aCate;
            }
        }
        $aDead=I('dead',0,'intval');
        if($aDead){
            $map['dead_line']=array('elt',time());
        }else{
            $map['dead_line']=array('gt',time());
        }
        $aPos=I('pos',0,'intval');
        /* 设置推荐位 */
        if($aPos>0){
            $map[] = "position & {$aPos} = {$aPos}";
        }

        $map['status']=1;

        $positions=$this->_getPositions(1);

        list($list,$totalCount)=$this->newsModel->getListByPage($map,$page,'update_time desc','*',$r);
        $category=$this->newsCategoryModel->getCategoryList(array('status'=>array('egt',0)),1);
        $category=array_combine(array_column($category,'id'),$category);
        foreach($list as &$val){
            $val['category']='['.$val['category'].'] '.$category[$val['category']]['title'];
        }
        unset($val);

        $optCategory=$category;
        foreach($optCategory as &$val){
            $val['value']=$val['title'];
        }
        unset($val);

        $builder=new AdminListBuilder();
        $builder->title(L('_NEWS_LIST_'))
            ->data($list)
            ->setSelectPostUrl(U('Admin/News/index'))
            ->select('','cate','select','','','',array_merge(array(array('id'=>0,'value'=>L('_EVERYTHING_'))),$optCategory))
            ->select('','dead','select','','','',array(array('id'=>0,'value'=>L('_NEWS_CURRENT_')),array('id'=>1,'value'=>L('_NEWS_HISTORY_'))))
            ->select(L('_RECOMMENDATIONS_'),'pos','select','','','',array_merge(array(array('id'=>0,'value'=>L('_ALL_DEFECTIVE_'))),$positions))
            ->buttonNew(U('News/editNews'))
            ->keyId()->keyUid()->keyText('title',L('_TITLE_'))->keyText('category',L('_CATEGORY_'))->keyText('description',L('_NOTE_'))->keyText('sort',L('_SORT_'))
            ->keyStatus()->keyTime('dead_line',L('_PERIOD_TO_'))->keyCreateTime()->keyUpdateTime()
            ->keyDoActionEdit('News/editNews?id=###');
        if(!$aDead){
            $builder->ajaxButton(U('News/setDead'),'',L('_SET_EXPIRE_'))->keyDoAction('News/setDead?ids=###',L('_SET_EXPIRE_'))
                ->buttonModalPopup(U('News/changeCategory',array('id'=>$map['id'])), array(),'迁移分类',array('data-title'=>'迁移分类','target-form'=>'ids'));
        }
        $builder->pagination($totalCount,$r)
            ->display();
    }

    //待审核列表
    public function audit($page=1,$r=20)
    {
        $aAudit=I('audit',0,'intval');
        if($aAudit==3){
            $map['status']=array('in',array(-1,2));
        }elseif($aAudit==2){
            $map['dead_line']=array('elt',time());
            $map['status']=2;
        }elseif($aAudit==1){
            $map['status']=-1;
        }else{
            $map['status']=2;
            $map['dead_line']=array('gt',time());
        }
        list($list,$totalCount)=$this->newsModel->getListByPage($map,$page,'update_time desc','*',$r);
        $cates=array_column($list,'category');
        $category=$this->newsCategoryModel->getCategoryList(array('id'=>array('in',$cates),'status'=>1),1);
        $category=array_combine(array_column($category,'id'),$category);
        foreach($list as &$val){
            $val['category']='['.$val['category'].'] '.$category[$val['category']]['title'];
        }
        unset($val);

        $builder=new AdminListBuilder();

        $builder->title(L('_AUDIT_LIST_'))
            ->data($list)
            ->setStatusUrl(U('News/setNewsStatus'))
            ->buttonEnable(null,L('_AUDIT_SUCCESS_'))
            ->buttonModalPopup(U('News/doAudit'),null,L('_AUDIT_UNSUCCESS_'),array('data-title'=>L('_AUDIT_FAIL_REASON_'),'target-form'=>'ids'))
            ->setSelectPostUrl(U('Admin/News/audit'))
            ->select('','audit','select','','','',array(array('id'=>0,'value'=>L('_AUDIT_READY_')),array('id'=>1,'value'=>L('_AUDIT_FAIL_')),array('id'=>2,'value'=>L('_EXPIRE_AND_UNAUDITED_')),array('id'=>3,'value'=>L('_AUDIT_ALL_'))))
            ->keyId()->keyUid()->keyText('title',L('_TITLE_'))->keyText('category',L('_CATEGORY_'))->keyText('description',L('_NOTE_'))->keyText('sort',L('_SORT_'));
        if($aAudit==1){
            $builder->keyText('reason',L('_FAULT_REASON_'));
        }
        $builder->keyTime('dead_line',L('_PERIOD_TO_'))->keyCreateTime()->keyUpdateTime()
            ->keyDoActionEdit('News/editNews?id=###')
            ->pagination($totalCount,$r)
            ->display();
    }

    /**
     * 审核失败原因设置
     * @author 郑钟良<zzl@ourstu.com>
     */
    public function doAudit()
    {
        if(IS_POST){
            $ids=I('post.ids','','text');
            $ids=explode(',',$ids);
            $reason=I('post.reason','','text');
            $res=$this->newsModel->where(array('id'=>array('in',$ids)))->setField(array('reason'=>$reason,'status'=>-1));
            if($res){
                $result['status']=1;
                $result['url']=U('Admin/News/audit');
                //发送消息
                $messageModel=D('Common/Message');
                foreach($ids as $val){
                    $news=$this->newsModel->getData($val);
                    $tip = L('_YOUR_NEWS_').'【'.$news['title'].'】'.L('_FAIL_AND_REASON_').$reason;
                    $messageModel->sendMessage($news['uid'], L('_NEWS_AUDIT_FAIL_'),$tip,  'News/Index/detail',array('id'=>$val), is_login(), 2);
                }
                //发送消息 end
            }else{
                $result['status']=0;
                $result['info']=L('_OPERATE_FAIL_');
            }
            $this->ajaxReturn($result);
        }else{
            $ids=I('ids');
            $ids=implode(',',$ids);
            $this->assign('ids',$ids);
            $this->display(T('News@Admin/audit'));
        }
    }

    public function setNewsStatus($ids,$status=1)
    {
        !is_array($ids)&&$ids=explode(',',$ids);
        $builder = new AdminListBuilder();
        S('news_home_data',null);
        //发送消息
        $messageModel=D('Common/Message');
        foreach($ids as $val){
            $news=$this->newsModel->getData($val);
            $tip = L('_YOUR_NEWS_').'【'.$news['title'].'】'.L('_AUDIT_SUCCESS_').'。';
            $messageModel->sendMessage($news['uid'],L('_NEWS_AUDIT_SUCCESS_'), $tip,  'News/Index/detail',array('id'=>$val), is_login(), 2);
        }
        //发送消息 end
        $builder->doSetStatus('News', $ids, $status);
    }

    private function deleteHtml($str)
    {

        $str = preg_replace("/\s+/", " ", $str); //过滤多余回车
        $str = preg_replace("/<[ ]+/si", "<", $str); //过滤<__("<"号后面带空格)

        $str = preg_replace("/<\!--.*?-->/si", "", $str); //注释
        $str = preg_replace("/<(\!.*?)>/si", "", $str); //过滤DOCTYPE
        $str = preg_replace("/<(\/?html.*?)>/si", "", $str); //过滤html标签
        $str = preg_replace("/<(\/?head.*?)>/si", "", $str); //过滤head标签
        $str = preg_replace("/<(\/?meta.*?)>/si", "", $str); //过滤meta标签
        $str = preg_replace("/<(\/?body.*?)>/si", "", $str); //过滤body标签
        $str = preg_replace("/<(\/?link.*?)>/si", "", $str); //过滤link标签
        $str = preg_replace("/<(\/?form.*?)>/si", "", $str); //过滤form标签
        $str = preg_replace("/cookie/si", "COOKIE", $str); //过滤COOKIE标签

        $str = preg_replace("/<(applet.*?)>(.*?)<(\/applet.*?)>/si", "", $str); //过滤applet标签
        $str = preg_replace("/<(\/?applet.*?)>/si", "", $str); //过滤applet标签

        $str = preg_replace("/<(style.*?)>(.*?)<(\/style.*?)>/si", "", $str); //过滤style标签
        $str = preg_replace("/<(\/?style.*?)>/si", "", $str); //过滤style标签

        $str = preg_replace("/<(title.*?)>(.*?)<(\/title.*?)>/si", "", $str); //过滤title标签
        $str = preg_replace("/<(\/?title.*?)>/si", "", $str); //过滤title标签

        $str = preg_replace("/<(object.*?)>(.*?)<(\/object.*?)>/si", "", $str); //过滤object标签
        $str = preg_replace("/<(\/?objec.*?)>/si", "", $str); //过滤object标签

        $str = preg_replace("/<(noframes.*?)>(.*?)<(\/noframes.*?)>/si", "", $str); //过滤noframes标签
        $str = preg_replace("/<(\/?noframes.*?)>/si", "", $str); //过滤noframes标签

        $str = preg_replace("/<(i?frame.*?)>(.*?)<(\/i?frame.*?)>/si", "", $str); //过滤frame标签
        $str = preg_replace("/<(\/?i?frame.*?)>/si", "", $str); //过滤frame标签

        $str = preg_replace("/<(script.*?)>(.*?)<(\/script.*?)>/si", "", $str); //过滤script标签
        $str = preg_replace("/<(\/?script.*?)>/si", "", $str); //过滤script标签
        $str = preg_replace("/javascript/si", "Javascript", $str); //过滤script标签
        $str = preg_replace("/vbscript/si", "Vbscript", $str); //过滤script标签
        $str = preg_replace("/on([a-z]+)\s*=/si", "On\\1=", $str); //过滤script标签
        $str = preg_replace("/&#/si", "&＃", $str); //过滤script标签，如javAsCript:alert(


        return $str;

    }

    public function editNews()
    {
        $aId=I('id',0,'intval');
        $title=$aId?L('_EDIT_'):L('_ADD_');
        if(IS_POST){
            $aId&&$data['id']=$aId;
            $data['uid']=I('post.uid',get_uid(),'intval');
            $data['title']=I('post.title','','op_t');
            $data['content']=I('post.content','','filter_content');

            //这里需要进一步处理，过滤掉一些标签
            //处理传递回来的html内容
            $html = I('post.content_mobile','','filter_content');

            //删除特定的标记
            $html = $this->deleteHtml($html);

            //清理HTML属性
            $sa = new CleanHtml();
            //$sa->allow = array('id');//允许所有的id属性

            //保留以下特别的属性，否则清除所有的属性
            $sa->exceptions = array(
//                'img' => array('src', 'alt'),
//                'a' => array('href', 'title'),
                'img' => array('src'),
                'a' => array('href'),
                'p' => array(),
                'h1' => array(),
                'h2' => array(),
                'h3' => array(),
                'h4' => array(),
                'h5' => array(),
                'h6' => array(),
                'ul' => array(),
                'ol' => array(),
                'li' => array(),
                'div' => array(),
                'span' => array(),
                'hr' => array(),
                'br' => array(),
                //'iframe' => array('src', 'frameborder'),
            );

            $sa->ignore=array('br');

            $html = $sa->strip($html);
            $html = strip_tags($html,"<h1><h2><h3><h4><h5><h6><p><ul><ol><li><img><span><div>");

            $data['content_mobile'] = $html;


            $data['category']=I('post.category',0,'intval');
            $data['description']=I('post.description','','op_t');
            $data['cover']=I('post.cover',0,'intval');
            $data['view']=I('post.view',0,'intval');
            $data['comment']=I('post.comment',0,'intval');
            $data['collection']=I('post.collection',0,'intval');
            $data['sort']=I('post.sort',0,'intval');
            $data['dead_line']=I('post.dead_line',2147483640,'intval');
            if($data['dead_line']==0){
                $data['dead_line']=2147483640;
            }
            $data['template']=I('post.template','','op_t');
            $data['status']=I('post.status',1,'intval');
            $data['source']=I('post.source','','op_t');
            $data['position']=0;
            $position=I('post.position','','op_t');
            $position=explode(',',$position);
            foreach($position as $val){
                $data['position']+=intval($val);
            }
            $this->_checkOk($data);
            $result=$this->newsModel->editData($data);
            if($result){
                S('news_home_data',null);
                $aId=$aId?$aId:$result;
                $this->success($title.L('_SUCCESS_'),U('News/editNews',array('id'=>$aId)));
            }else{
                $this->error($title.L('_SUCCESS_'),$this->newsModel->getError());
            }
        }else{
            $position_options=$this->_getPositions();
            if($aId){
                $data=$this->newsModel->find($aId);
                $detail=$this->newsDetailModel->find($aId);
                $data['content']=$detail['content'];
                $data['template']=$detail['template'];
                $data['content_mobile']=$detail['content_mobile']; //移动版本

                $position=array();
                foreach($position_options as $key=>$val){
                    if($key&$data['position']){
                        $position[]=$key;
                    }
                }
                $data['position']=implode(',',$position);
            }
            $category=$this->newsCategoryModel->getCategoryList(array('status'=>array('egt',0)),1);
            $options=array();
            foreach($category as $val){
                $options[$val['id']]=$val['title'];
            }
            $builder=new AdminConfigBuilder();
            $builder->title($title.L('_NEWS_'))
                ->data($data)
                ->keyId()
                ->keyReadOnly('uid',L('_PUBLISHER_'))->keyDefault('uid',get_uid())
                ->keyText('title',L('_TITLE_'))
                ->keyEditor('content',L('_CONTENT_'),'','all',array('width' => '1000px', 'height' => '600px'))
                ->keyEditor('content_mobile','移动版本的新闻内容','只支持基本的HTML标签，以方便在移动设备上显示。','',array('width' => '1000px', 'height' => '600px'))

                ->keySelect('category',L('_CATEGORY_'),'',$options)

                ->keyTextArea('description',L('_NOTE_'))
                ->keySingleImage('cover',L('_COVER_'))
                ->keyInteger('view',L('_VIEWS_'))->keyDefault('view',0)
                ->keyInteger('comment',L('_COMMENTS_'))->keyDefault('comment',0)
                ->keyInteger('collection',L('_COLLECTS_'))->keyDefault('collection',0)
                ->keyInteger('sort',L('_SORT_'))->keyDefault('sort',0)
                ->keyTime('dead_line',L('_PERIOD_TO_'))->keyDefault('dead_line',2147483640)
                ->keyText('template',L('_TEMPLATE_'))
                ->keyText('source',L('_SOURCE_'),L('_SOURCE_ADDRESS_'))
                ->keyCheckBox('position',L('_RECOMMENDATIONS_'),L('_TIP_RECOMMENDATIONS_'),$position_options)
                ->keyStatus()->keyDefault('status',1)

                ->group(L('_BASIS_'),'id,uid,title,cover,category')
                ->group('桌面版','content')
                ->group('移动版','content_mobile')

                ->group(L('_EXTEND_'),'description,view,comment,sort,dead_line,position,source,template,status')

                ->buttonSubmit()->buttonBack()
                ->display();
        }
    }

    public function setDead($ids)
    {
        !is_array($ids)&&$ids=explode(',',$ids);
        $res=$this->newsModel->setDead($ids);
        if($res){
            //发送消息
            $messageModel=D('Common/Message');
            foreach($ids as $val){
                $news=$this->newsModel->getData($val);
                $tip = L('_YOUR_NEWS_').'【'.$news['title'].'】'.L('_SET_TO_EXPIRE_').'。';
                $messageModel->sendMessage($news['uid'],L('_NEWS_TO_EXPIRE_'),  $tip, 'News/Index/detail',array('id'=>$val), is_login(), 2);
            }
            //发送消息 end
            S('news_home_data',null);
            $this->success(L('_SUCCESS_TIP_'),U('News/index'));
        }else{
            $this->error(L('_OPERATE_FAIL_').$this->newsModel->getError());
        }
    }


    private function _checkOk($data=array()){
        if(!mb_strlen($data['title'],'utf-8')){
            $this->error(L('_TIP_TITLE_EMPTY_'));
        }
        if(mb_strlen($data['content'],'utf-8')<20){
            $this->error(L('_TIP_CONTENT_LENGTH_'));
        }
        return true;
    }

    private function _getPositions($type=0)
    {
        $default_position=<<<str
1:系统首页
2:推荐阅读
4:本类推荐
str;
        $positons=modC('NEWS_SHOW_POSITION',$default_position,'News');
        $positons = str_replace("\r", '', $positons);
        $positons = explode("\n", $positons);
        $result=array();
        if($type){
            foreach ($positons as $v) {
                $temp = explode(':', $v);
                $result[] = array('id'=>$temp[0],'value'=>$temp[1]);
            }
        }else{
            foreach ($positons as $v) {
                $temp = explode(':', $v);
                $result[$temp[0]] = $temp[1];
            }
        }

        return $result;
    }
    public function changeCategory(){
        if(IS_POST){
            $aIds=I('post.ids','','op_t');
            $aCid=I('post.cid','','op_t');
            $ids=explode(',',$aIds);
            $map['id']=array('in',$ids);
            $data['category']=$aCid;
            $res=M('news')->where($map)->save($data);
            if($res){
                $this->ajaxReturn(1);
            }else{
                $this->ajaxReturn(0);
            }

        }else{
        $aIds=I('get.ids');
        $ids=implode(',',$aIds);
        $map['id']=array('in',$ids);
        $newsdata=M('news')->where($map)->field('id,title')->select();
        $cat=M('news_category')->where('status=1')->field('id,title')->select();
        $this->assign('cat',$cat);
        $this->assign('data',$newsdata);
        $this->assign('ids',$ids);
        $this->display(T('News@Admin/changecategory'));
    }
    }

}
