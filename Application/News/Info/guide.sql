
delete from `ocenter_menu` where  `module`='News';
INSERT INTO `ocenter_menu` (`title`, `pid`, `sort`, `url`, `hide`, `tip`, `group`, `is_dev`, `icon`, `module`) VALUES
  ('资讯', 0, 0, 'News/News/index', 1, '', '', 0, '', 'News');

SET @tmp_id = 0;
SELECT @tmp_id := id
FROM `ocenter_menu`
WHERE title = '资讯' AND `module`='News';

INSERT INTO `ocenter_menu` (`title`, `pid`, `sort`, `url`, `hide`, `tip`, `group`, `is_dev`, `icon`, `module`) VALUES
('基础配置',@tmp_id, '0', 'News/News/config', '0', '', '资讯配置', '0', '', 'News'),
('编辑、添加资讯', @tmp_id, '0', 'News/News/editNews', '1', '', '', '0', '', 'News'),
('设为到期',@tmp_id, '0', 'News/News/setDead', '1', '', '', '0', '', 'News'),
('资讯列表',@tmp_id, '0', 'News/News/index', '0', '', '资讯管理', '0', 'rss-sign', 'News'),
('设置分类状态',@tmp_id, '0', 'News/News/setStatus', '1', '', '', '0', '', 'News'),
('分类管理', @tmp_id, '0', 'News/News/newsCategory', '0', '', '资讯配置', '0', '', 'News'),
('编辑、添加分类', @tmp_id, '0', 'News/News/add', '1', '', '', '0', '', 'News'),
('审核通过', @tmp_id, '0', 'News/News/setNewsStatus', '1', '', '', '0', '', 'News'),
('资讯审核失败操作', @tmp_id, '0', 'News/News/doAudit', '1', '', '', '0', '', 'News'),
('审核列表', @tmp_id, '0', 'News/News/audit', '0', '', '资讯管理', '0', '', 'News');

DELETE FROM  `ocenter_auth_rule` WHERE `module`='News';
INSERT INTO `ocenter_auth_rule` ( `module`, `type`, `name`, `title`, `status`, `condition`) VALUES
  ( 'News', 1, 'News/News/Index/add', '资讯投稿', 1, ''),
  ( 'News', 1, 'News/News/Index/edit', '编辑资讯（管理）', 1, '');


INSERT INTO `ocenter_menu` (`title`, `pid`, `sort`, `url`, `hide`, `tip`, `group`, `is_dev`, `icon`, `module`) VALUES
  ('数据采集',@tmp_id, '1000', 'News/Collect/qqEduNewsList', '0', '', '新闻采集', '0', '', 'News');
