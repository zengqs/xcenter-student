DELETE FROM `ocenter_news_category` WHERE 1;


INSERT INTO `ocenter_news_category` (`id`, `title`, `pid`, `cover`, `can_post`, `need_audit`, `sort`, `status`) VALUES (1, '喜腾系统', 0, 6, 1, 1, 1, 1);
INSERT INTO `ocenter_news_category` (`id`, `title`, `pid`, `cover`, `can_post`, `need_audit`, `sort`, `status`) VALUES (2, '牛熊动态', 1, 7, 1, 1, 1, 1);
INSERT INTO `ocenter_news_category` (`id`, `title`, `pid`, `cover`, `can_post`, `need_audit`, `sort`, `status`) VALUES (3, '游戏攻略', 1, 8, 1, 1, 1, 1);
INSERT INTO `ocenter_news_category` (`id`, `title`, `pid`, `cover`, `can_post`, `need_audit`, `sort`, `status`) VALUES (4, '联系客服', 1, 9, 1, 1, 1, 1);
INSERT INTO `ocenter_news_category` (`id`, `title`, `pid`, `cover`, `can_post`, `need_audit`, `sort`, `status`) VALUES (5, '客户端下载', 1, 10, 1, 1, 1, 1);
