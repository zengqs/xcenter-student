
DROP TABLE IF EXISTS `ocenter_news`;
-- -----------------------------
-- 表结构 `ocenter_news`
-- -----------------------------
CREATE TABLE IF NOT EXISTS `ocenter_news` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) NOT NULL,
  `title` varchar(50) NOT NULL COMMENT '标题',
  `description` TEXT NOT NULL COMMENT '描述',
  `category` int(11) NOT NULL COMMENT '分类',
  `status` tinyint(2) NOT NULL DEFAULT 1 COMMENT '状态',
  `reason` varchar(100)NOT NULL DEFAULT '没有填写原因' COMMENT '审核失败原因',
  `sort` int(5) NOT NULL  DEFAULT 0  COMMENT '排序',
  `position` int(4) NOT NULL DEFAULT 0 COMMENT '定位，展示位',
  `cover` int(11) NULL COMMENT '封面',
  `cover_url` VARCHAR(500) DEFAULT '' COMMENT '封面图片的链接，如果采集的图片未做本地转存，则使用该链接',
  `view` int(10) NOT NULL DEFAULT 0 COMMENT '阅读量',
  `comment` int(10) NOT NULL DEFAULT 0  COMMENT '评论量',
  `collection` int(10) NOT NULL DEFAULT 0  COMMENT '收藏量',
  `dead_line` int(11) NOT NULL DEFAULT 0  COMMENT '有效期',
  `source` varchar(200) NULL COMMENT '来源url',
  `source_tag` varchar(200) NULL COMMENT 'TAG,多个Tag用逗号分隔',
  `source_name` varchar(200) NULL COMMENT '采集,如腾讯教育',
  `source_channel` varchar(200) NULL COMMENT '采集来源屏道',
  `news_type` int(11) DEFAULT 0 COMMENT '新闻类别：0:系统采集,1:一般新闻',
  `create_time` int(11) NULL,
  `update_time` int(11) NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COMMENT='资讯';


DROP TABLE  IF EXISTS ocenter_news_category;
-- -----------------------------
-- 表结构 `ocenter_news_category`
-- -----------------------------
CREATE TABLE IF NOT EXISTS `ocenter_news_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(20) NOT NULL,
  `pid` int(11) NOT NULL,
  `cover` int(11) NULL COMMENT '封面', # 新增一个封面字段
  `can_post` tinyint(4) NOT NULL  DEFAULT 0  COMMENT '前台可投稿',
  `need_audit` tinyint(4) NOT NULL  DEFAULT 0  COMMENT '前台投稿是否需要审核',
  `sort` tinyint(4) NOT NULL  DEFAULT 0 ,
  `status` tinyint(4) NOT NULL  DEFAULT 1,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=10000 DEFAULT CHARSET=utf8 COMMENT='资讯分类';

DROP TABLE IF EXISTS `ocenter_news_detail`;
-- -----------------------------
-- 表结构 `ocenter_news_detail`
-- -----------------------------
CREATE TABLE IF NOT EXISTS `ocenter_news_detail` (
  `news_id` int(11) NOT NULL,
  `content` text NULL COMMENT '内容',
  `template` varchar(50) NULL COMMENT '模板',
  `content_mobile` text NULL COMMENT '移动版内容',
  `template_mobile` varchar(50) NULL COMMENT '移动版模板',
  PRIMARY KEY (`news_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='资讯详情';

