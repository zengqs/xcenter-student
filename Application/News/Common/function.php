<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2016/12/19
 * Time: 13:30
 */
require_once(APP_PATH . '/Admin/Common/function.php');

if (!function_exists('object_array')) {
    /**
     * @param $array
     * 将PHP Std Class Object转换为 Array对象
     * @return array
     */
    function object_array($array)
    {
        if (is_object($array)) {
            $array = (array)$array;
        }
        if (is_array($array)) {
            foreach ($array as $key => $value) {
                $array[$key] = object_array($value);
            }
        }
        return $array;
    }
}


/**
 * 保存文件到本地
 * @param 文件路径 $url
 * @param 保存本地路径 $savePath
 * @return string
 */
function downloadFile($url,$savePath='')
{
    $ary = parse_url($url);
    $file = basename($ary['path']);
    $ext = explode('.',$file);
    $ext = $ext[1];

    $fileName = rand(0,1000).$ext;
    $file = file_get_contents($url);
    file_put_contents($savePath.'/'.$fileName,$file);
    return $fileName;
}

