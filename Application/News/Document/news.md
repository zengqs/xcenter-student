news 资讯表    
===
咨询模块的主要功能是实现通用的CMS的功能，能够对文章进行分类管理并支持定向采集


# 数据字典

| 字段             | 类型           | 注释      |
|----------------|--------------|---------|
| id             | int(11)      | 自增主键    |
| uid            | int(11)      | 用户id    |
| title          | varchar(50)  | 标题      |
| description    | varchar(200) | 描述      |
| category       | int(11)      | 分类      |
| status         | tinyint(2)   | 状态      |
| reason         | varchar(100) | 审核失败原因  |
| sort           | int(5)       | 排序      |
| position       | int(4)       | 定位，展示位  |
| cover          | int(11)      | 封面      |
| view           | int(10)      | 阅读量     |
| comment        | int(10)      | 评论量     |
| collection     | int(10)      | 收藏量     |
| dead_line      | int(11)      | 有效期     |
| source         | varchar(200) | 来源url   |
| source_tag     | varchar(200) | 采集来源TAG |
| source_name    | varchar(200) | 采集来源    |
| source_channel | varchar(200) | 采集来源屏道  |
| create_time    | int(11)      | 创建时间    |
| update_time    | int(11)      | 更新时间    |



news_category 资讯分类表    

| 字段         | 类型          | 注释         |
|------------|-------------|------------|
| id         | int(11)     | 自增主键       |
| title      | varchar(20) | 分类名称       |
| pid        | int(11)     | 父级id       |
| cover      | int(11)     | 封面的图片ID    |
| can_post   | tinyint(4)  | 前台可投稿      |
| need_audit | tinyint(4)  | 前台投稿是否需要审核 |
| sort       | tinyint(4)  | 排序         |
| status     | tinyint(4)  | 状态         |



news_detail 资讯详情表    

| 字段              | 类型          | 注释          |
|-----------------|-------------|-------------|
| news_id         | int(11)     | 咨讯id        |
| content         | text        | 内容          |
| template        | varchar(50) | 模板          |
| content_mobile  | text        | WAP、微信版本的内容 |
| template_mobile | varchar(50) | WAP、微信版本的模板 |
