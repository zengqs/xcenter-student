<?php

namespace News\Spider;

/**
 * 采集程序接口说明
 * Interface ISpider
 * @package News\Spider
 */
interface ISpider
{
    /**
     * 执行采集动作
     * @return mixed
     */
    function run();
}
