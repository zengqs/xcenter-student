<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/5/20 0020
 * Time: 21:47
 */

namespace News\Spider;


use News\Model\NewsModel;
use Vendor\QL\QueryList;

/**
 * Class Dashuju36Com
 * 采集大数据36（36dsj.com）网站的新闻
 *
 * @package News\Spider
 */
class Eastmoney extends BaseSpider implements ISpider
{

    private $newsModel;

//    function _initialize()
//    {
//
//        $this->categoryId=1; //
//    }

    /**
     * Eastmoney constructor.
     * @param $categoryId
     */
    public function __construct($categoryId)
    {
        parent::__construct($categoryId);

        $this->newsModel = new NewsModel();
    }


    public function run()
    {
        $url = "http://m.eastmoney.com/channel/finance/";
        $this->parse_page($url);
    }

    //36dsj.com
    public function parse_page($url)
    {

        //采集规则
        $rules = array(
            "title"       => array("a", "text"),
            //"thumbnail"   => array("div.focus a span img.thumb", "data-original"),
            "url"         => array("a", "href"),
            //"description" => array("a", "text"),
            //"date"        => array("p.info", "text"),
        );
        $rang = "div.news_list_daodu ul li"; //采集范围
        $ql = QueryList::Query($url, $rules, $rang);

        $ql->getData(function ($item) {

            sdebug($item,'item',__FILE__,__LINE__);

            $news = $this->newsModel->where(array('source' => $item['url']))->find();

            //处理时间字段
//            $tmp=explode(' ',trim($item['date']));
//            $item['date']=$tmp[count($tmp)-1]; //
            if ($news) {
                $uid = is_login();
                $news['source'] = $item['url'];

                $news['title'] = $item['title'];
                //$news['description'] = $item['description'];

                //临时方案，封面图片应该做本地存储
                $news['cover'] = 0; //图片未做本地存储

                //$news['cover_url'] = $item['thumbnail'];
                $news['uid'] = $uid;
                $news['category'] =  $this->categoryId;//腾讯教育
                $news['dead_line'] = time() + 30 * 24 * 3600;

                $news['news_type'] = 0;//系统自动采集
//                $news['create_time'] = $item['date'];
//                $news['update_time'] = $item['date'];

                $detail = $this->getDetail($item['url']);
                $news['content'] = $news['content_mobile'] = $detail;

                //
                $news['source_tag'] = "eastmoney";
                $news['source_name'] = "eastmoney";
                $news['source_channel'] = "eastmoney";

                $this->newsModel->editData($news);
                //中文


            } else {
                $news = array();
                $uid = is_login();
                $news['source'] = $item['url'];

                $news['title'] = $item['title'];
                //$news['description'] = $item['description'];

                //临时方案，封面图片应该做本地存储
                $news['cover'] = 0; //图片未做本地存储

                $news['cover_url'] = $item['thumbnail'];
                $news['uid'] = $uid;
                $news['category'] = $this->categoryId;//默认分类
                $news['dead_line'] = time() + 30 * 24 * 3600;

                $news['news_type'] = 0;//系统自动采集
//                $news['create_time'] = $item['date'];
//                $news['update_time'] = $item['date'];

                $detail = $this->getDetail($item['url']);
                $news['content'] = $news['content_mobile'] = $detail;

                $news['source_tag'] = "eastmoney";
                $news['source_name'] = "eastmoney";
                $news['source_channel'] = "eastmoney";


                $this->newsModel->editData($news);
            }
            $news = null;

            return $item;
        });


        //采集下一页

//        //采集规则
//        $rules = array(
//            "next_page" => array('div.pagination ul li.next-page a', "href"));
//        $rang = "div.content"; //采集范围
//        $ql = QueryList::Query($url, $rules, $rang);
//
//        $ql->getData(function ($item) {
//            //开始下一页的采集
//            if (!empty($item['next_page']) && trim($item['next_page']) != "") {
//                $next_page = $item['next_page'];
//                $this->parse_page($next_page); //采集下一页
//            }
//        });
    }

    function getDetail($url)
    {

        $rules = array(
            'title' => array("div.pagetitle div.atitle", "text"),
            'body'  => array('article.zwtext', 'html'),
        );
        $rang = "div.mbody";
        $ql = QueryList::Query($url, $rules, $rang);
        $data = $ql->getData(function ($item) {
            return $item;
        });

        if (empty($data)) {
            return null;
        } else {
            if (count($data) > 0) {
                $content_data = $data[0];

                return $content_data['title'] . $content_data['body'];
            }

            return null;
        }
    }
}
