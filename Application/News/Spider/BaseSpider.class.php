<?php

namespace News\Spider;

use Think\Model;
use Vendor\QL\QueryList;

abstract class BaseSpider //extends Model
{
    protected $categoryId=0; //DataTable id

    public function __construct($categoryId)
    {
        $this->$categoryId=$$categoryId;
    }

    /**
     * 获取指定url的网页HTML内容
     * @param $url    网页完整链接
     * @return String HTML字符串
     */
    public function getHtml($url)
    {
        $ql = QueryList::Query($url, null);
        $data = $ql->getHtml();
        return $data;
    }
}
