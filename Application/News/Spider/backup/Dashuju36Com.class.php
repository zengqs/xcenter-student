<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/5/20 0020
 * Time: 21:47
 */

namespace News\Spider;


use News\Model\NewsModel;
use Vendor\QL\QueryList;

class Dashuju36Com extends BaseSpider implements ISpider
{

    public function run()
    {

        //只采集一共共有多少页，然后再分页采集数据
        $url = "http://www.36dsj.com/archives/category/ganhuo/page/1";
//        $rules = array("title" => array("a", "text"), "url" => array("a", "href"), "abstract" => array("dd.wz", "text"), "date" => array("dd.ky", "text"));
//        $rang = "#listZone dl";
//        $ql = QueryList::Query($url, $rules, $rang);
//
//        $data = $ql->getData(function ($item) {
//            //http://edu.qq.com/a/20161128/018534.htm
//            $item['url'] = "http://edu.qq.com" . $item['url'];
//
//            return $item;
//        });

        $this->collect_page(array($url));

    }

    //36dsj.com
    public function collect_page($list)
    {
        //多线程扩展
        $cm = QueryList::run('Multi', [
//            //待采集链接集合
//            'list'    => [
//                "http://www.36dsj.com/archives/category/ganhuo/page/1",
//                //更多的采集链接....
//            ],
            'list'    => $list,
            'curl'    => [
                'opt'       => array(
                    CURLOPT_SSL_VERIFYPEER => false,
                    CURLOPT_SSL_VERIFYHOST => false,
                    CURLOPT_FOLLOWLOCATION => true,
                    CURLOPT_AUTOREFERER    => true,
                ),
                //设置线程数
                'maxThread' => 100,
                //设置最大尝试数
                'maxTry'    => 3,
            ],
            //不自动开始线程，默认自动开始
            'start'   => false,
            'success' => function ($a) {
                //采集规则
                $rules = array("title"       => array("h2 a", "text"),
                               "thumbnail"   => array("article.excerpt div.focus a span img", "data-original"),
                               "url"         => array("article.excerpt h2 a", "href"),
                               "description" => array("article.excerpt p.note", "text"),
//                               "date"        => array("span.glyphicon.glyphicon-time", "text"),
                               "date"        => array("article.excerpt span.glyphicon.glyphicon-time", "text"),
                               "next"   => array('article.excerpt div.pagination ul li.next-page', "text"),
                               "next_page"   => array('div.pagination ul li.next-page a', "href"));
                $rang = "div.content article.excerpt"; //采集范围
                $ql = QueryList::Query($a['content'], $rules, $rang);

                $data = $ql->getData(function ($item) {
                    $newModel = new NewsModel();

                    $news = $newModel->where(array('source' => $item['url']))->find();
                    if ($news) {
                        $uid = is_login();
                        $news['source'] = $item['url'];

                        $news['title'] = $item['title'];
                        $news['description'] = $item['description'];

                        //临时方案，封面图片应该做本地存储
                        $news['cover'] = 0; //图片未做本地存储

                        $news['cover_url'] = $item['thumbnail'];
                        $news['uid'] = $uid;
                        $news['category'] = '202';//腾讯教育
                        $news['dead_line'] = time() + 30 * 24 * 3600;

                        $news['news_type'] = 0;//系统自动采集
                        $news['create_time'] = strtotime($item['date']);
                        $news['update_time'] = strtotime($item['date']);

                        $detail = $this->getDetail($item['url']);
                        $news['content'] = $news['content_mobile'] = $detail;
                        $newModel->editData($news);


                    } else {
                        $news = array();
                        $uid = is_login();
                        $news['source'] = $item['url'];

                        $news['title'] = $item['title'];
                        $news['description'] = $item['description'];

                        //临时方案，封面图片应该做本地存储
                        $news['cover'] = 0; //图片未做本地存储

                        $news['cover_url'] = $item['thumbnail'];
                        $news['uid'] = $uid;
                        $news['category'] = '202';//腾讯教育
                        $news['dead_line'] = time() + 30 * 24 * 3600;

                        $news['news_type'] = 0;//系统自动采集
                        $news['create_time'] = strtotime($item['date']);
                        $news['update_time'] = strtotime($item['date']);

                        $detail = $this->getDetail($item['url']);
                        $news['content'] = $news['content_mobile'] = $detail;
                        $newModel->editData($news);

                    }

                    if (!empty($item['next_page']) && trim($item['next_page'])!=""){
                        $next_page=$item['next_page'];
                        $this->collect_page(array($next_page)); //采集下一页
                    }
                    return $item;
                });
            },
            'error'   => function () {
                //出错处理
            },
        ]);

        //开始采集
        $cm->start();
    }

    function getDetail($url)
    {
//            $ql = QueryList::Query($url);

//            $rules = array("title" => array("div.text-title", "text"), "url" => array("a", "href"), "abstract" => array("dd.wz", "text"), "date" => array("dd.ky", "text"));
        $rules = array('title' => array("div.text-title", "html"), 'body' => array('article.article-content', 'html'));
        $rang = "div.content";
        $ql = QueryList::Query($url, $rules, $rang);
        $data = $ql->getData(function ($item) {
            return $item;
        });

//            dump($data);

        if (empty($data)) {
            return null;
        } else {
            if (count($data) > 0) {
                $content_data = $data[0];

                return $content_data['title'] . $content_data['body'];
            }

            return null;
        }


    }

}
