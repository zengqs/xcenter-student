<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/5/20 0020
 * Time: 21:47
 */

namespace News\Spider;


use News\Model\NewsModel;
use phpQuery;
use Vendor\QL\QueryList;


/**
 * 采集“http://www.36dsj.com/”网站的全部栏目的文章，保存到本地，包含以下信息：
 * 文章标题，发布时间，摘要，内容，封面图片，文章的原始链接，栏目
 * Class Dashuju36Multi
 *
 * @package News\Spider
 */
//多线程采集
class Dashuju36Multi extends BaseSpider implements ISpider
{
    private $newsModel;

    function _initialize()
    {
        $this->newsModel = new NewsModel();
    }

    public function run()
    {
        set_time_limit(0); //不限制超时

        //https://querylist.cc/

        //多线程扩展
        $cm = QueryList::run('Multi', [
            //待采集链接集合
            'list'    => [
                'http://www.36dsj.com/archives/category/bigdata', //大数据动向
                'http://www.36dsj.com/archives/category/bigdata/baike',//大数据百科
                "http://www.36dsj.com/archives/category/ganhuo", //干货教程
                //更多的采集链接....
            ],
            'curl'    => [
                'opt'       => array(
                    CURLOPT_SSL_VERIFYPEER => false,
                    CURLOPT_SSL_VERIFYHOST => false,
                    CURLOPT_FOLLOWLOCATION => true,
                    CURLOPT_AUTOREFERER    => true,
                ),
                //设置线程数
                'maxThread' => 100,
                //设置最大尝试数
                'maxTry'    => 3,
            ],
            //不自动开始线程，默认自动开始
            'start'   => false,
            'success' => function ($a) {
                //实际上此时已经执行过一次网页内容获取，如何避免重复获取内容
                $this->parse_page($a['info']['url']);
            },
            'error'   => function () {
                //出错处理
            },
        ]);

        //开始采集
        $cm->start();
    }


    /**
     * @param      $url
     * @param bool $is_start_page 是否是第一页，因为第一页要采集栏目等信息
     */
    public function parse_page($url)
    {

        //采集分栏目、下一页的链接等基础信息

        //采集规则
        $rules = array(
            "channel"     => array('h1.title strong a', "text"),
            "channel_url" => array('h1.title strong a', "href"),
            "next_page"   => array('div.pagination ul li.next-page a', "href"),
        );
        $rang = "div.content"; //采集范围
        $ql = QueryList::Query($url, $rules, $rang);

        $meta['next_page'] = null;
        $ql->getData(function ($item) {
            global $meta;
            //提取下一页的信息
            if (!empty($item['next_page']) && trim($item['next_page']) != "") {
                $meta['next_page'] = $item['next_page'];
            }
            $meta['channel'] = $item['channel'];
            $meta['channel_url'] = $item['channel_url'];

            return $item;
        });


        //参考：https://querylist.cc/
        //采集规则
        $rules = array(
            "title"       => array("h2 a", "text"),
            "thumbnail"   => array("div.focus a span img", "data-original"),
            //采用延迟加载图像的技术，所以获取不到src属性的值
            // "thumbnail"   => array("div.focus a span img", "src"),
            "url"         => array("h2 a", "href"),
            "description" => array("p.note", "text"),
            //过滤掉span标签
            "date"        => array("p.info", "text", '-span',
                function ($content) {
                    //利用回调函数，进一步处理时间字段
                    $tmp = explode(' ', trim($content));

                    return $tmp[count($tmp) - 1];
                },
            ),
        );
        $rang = "div.content article.excerpt"; //采集范围
        $ql = QueryList::Query($url, $rules, $rang);

        $ql->getData(function ($item) {
            $news = $this->newsModel->where(array('source' => $item['url']))->find();
            if ($news) {
                $uid = is_login();
                $news['source'] = $item['url'];

                $news['title'] = $item['title'];
                $news['description'] = $item['description'];

                //临时方案，封面图片应该做本地存储
                $news['cover'] = 0; //图片未做本地存储

                $news['cover_url'] = $item['thumbnail'];
                $news['uid'] = $uid;
                $news['category'] = 1;//腾讯教育
                $news['dead_line'] = time() + 30 * 24 * 3600;

                $news['news_type'] = 0;//系统自动采集
                $news['create_time'] = strtotime($item['date']);
                $news['update_time'] = strtotime($item['date']);

                $detail = $this->getDetail($item['url']);
                $news['content'] = $news['content_mobile'] = $detail;

                //
                $news['source_tag'] = "大数据"; //标签
                $news['source_name'] = '36大数据'; //采集的数据源
//                $news['source_channel'] = $meta['channel']; //数据源原始分类
                $news['source_channel'] = '大数据';

                $this->newsModel->editData($news);


            } else {
                $news = array();
                $uid = is_login();
                $news['source'] = $item['url'];

                $news['title'] = $item['title'];
                $news['description'] = $item['description'];

                //临时方案，封面图片应该做本地存储
                $news['cover'] = 0; //图片未做本地存储

                $news['cover_url'] = $item['thumbnail'];
                $news['uid'] = $uid;
                $news['category'] = 1;//默认分类
                $news['dead_line'] = time() + 30 * 24 * 3600;

                $news['news_type'] = 0;//系统自动采集
                $news['create_time'] = strtotime($item['date']);
                $news['update_time'] = strtotime($item['date']);

                $detail = $this->getDetail($item['url']);
                $news['content'] = $news['content_mobile'] = $detail;

                $news['source_tag'] = "大数据"; //标签
                $news['source_name'] = '36大数据'; //采集的数据源
                $news['source_channel'] = ''; //数据源原始分类


                $this->newsModel->editData($news);
            }
            $news = null;

            return $item;
        });


        //采集下一页
        //采集规则
        $rules = array(
            "next_page" => array('div.pagination ul li.next-page a', "href"));
        $rang = "div.content"; //采集范围
        $ql = QueryList::Query($url, $rules, $rang);

        $ql->getData(function ($item) {
            //开始下一页的采集
            if (!empty($item['next_page']) && trim($item['next_page']) != "") {
                $next_page = $item['next_page'];
                $this->parse_page($next_page); //采集下一页
            }
        });
    }


    function getDetail($url)
    {

        $rules = array(
            'title' => array("header.article-header h1.article-title", "html", 'a'),
            ////采集文章正文内容,利用过滤功能去掉文章中的超链接，但保留超链接的文字，并去掉版权、JS代码等无用信息
            'body'  => array('article.article-content', 'html', 'a -script', function ($content) {
                //利用回调函数下载文章中的图片并替换图片路径为本地路径
                //使用本例请确保当前目录下有image文件夹，并有写入权限
                //由于QueryList是基于phpQuery的，所以可以随时随地使用phpQuery，当然在这里也可以使用正则或者其它方式达到同样的目的
                $doc = phpQuery::newDocumentHTML($content);
                $imgs = pq($doc)->find('img');
                foreach ($imgs as $img) {
                    $src = pq($img)->attr('src');
                    $localSrc =  'Uploads/Picture/Spider/' . md5($src) . '.jpg';
                    $stream = file_get_contents($src);
                    file_put_contents($localSrc, $stream);
                    //转换为完整路径，方便API调用
                    pq($img)->attr('src', ext_get_site_root_url(true, $localSrc));
                }

                return $doc->htmlOuter();
            }),
        );
        $rang = "div.content";
        $ql = QueryList::Query($url, $rules, $rang);
        $data = $ql->getData(function ($item) {
            //如果需要进一步的处理

            return $item;
        });

        if (empty($data)) {
            return null;
        } else {
            if (count($data) > 0) {
                $content_data = $data[0];
                return $content_data['body'];
//                return $content_data['title'] . $content_data['body'];
            }

            return null;
        }
    }

}
