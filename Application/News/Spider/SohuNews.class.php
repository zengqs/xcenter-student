<?php

namespace News\Spider;


use News\Model\NewsModel;
use phpQuery;
use Vendor\QL\QueryList;

class SohuNews extends BaseSpider implements ISpider
{
    public function run()
    {
        //数据异步加载的，采集未能成功

        //$url = "http://mp.sohu.com/profile?xpt=Y2VkaWN0b2xAc29odS5jb20=";
        //需要采集一共有多少页数据，然后循环采集
        $url = "http://mp.sohu.com/apiV2/profile/newsListAjax?xpt=Y2VkaWN0b2xAc29odS5jb20=&pageNumber=1&pageSize=50";
        $ql = QueryList::Query($url, null);

        $data = $ql->getHtml();
        $data = urldecode($data);
        $json = json_decode($data);
        $json = object_array(json_decode($json));
        $data = $json['data'];

        //要将图像采集到本地存储，转换为系统图片表保存


        foreach ($data as $item) {
            $newModel = new NewsModel();
            $url = 'http:' . $item['url'];
            $news = $newModel->where(array('source' => $url))->find();
            if ($news) {
                $uid = is_login();
                $news['source'] = $url;

                $news['title'] = $item['title'];
                $news['description'] = $item['brief'];

                //临时方案，封面图片应该做本地存储
                $news['cover'] = 0; //图片未做本地存储

                $news['cover_url'] = $item['thumbnail'];
                $news['uid'] = $uid;
                $news['category'] = $this->categoryId;
                $news['dead_line'] = time() + 30 * 24 * 3600;

                $news['news_type'] = 0;//系统自动采集
                $news['create_time'] = $item['postTime'];
                $news['update_time'] = $item['postTime'];

                $detail = $this->getDetail($url);
                $news['content'] = $news['content_mobile'] = $detail;
                $newModel->editData($news);


            } else {

                $uid = is_login();
                $news['source'] = $url;
                $news['title'] = $item['title'];
                $news['description'] = $item['brief'];
                $news['cover_url'] = $item['thumbnail'];
                $news['uid'] = $uid;
                $news['category'] = $this->categoryId;//腾讯教育
                $news['dead_line'] = time() + 30 * 24 * 3600;

                $news['news_type'] = 0;//系统自动采集
                $news['create_time'] = $item['postTime'];
                $news['update_time'] = $item['postTime'];


                $detail = $this->getDetail($url);
                $news['content'] = $news['content_mobile'] = $detail;

                $newModel->editData($news);

            }
        }

        return $data;
    }

    function getDetail($url)
    {
//            $ql = QueryList::Query($url);

//            $rules = array("title" => array("div.text-title", "text"), "url" => array("a", "href"), "abstract" => array("dd.wz", "text"), "date" => array("dd.ky", "text"));
        $rules = array('title' => array("div.text-title", "html"),
            'body' => array('article.article', 'html', 'a -script', function ($content) {
                //利用回调函数下载文章中的图片并替换图片路径为本地路径
                //使用本例请确保当前目录下有Spider文件夹，并有写入权限
                //由于QueryList是基于phpQuery的，所以可以随时随地使用phpQuery，当然在这里也可以使用正则或者其它方式达到同样的目的
                $doc = phpQuery::newDocumentHTML($content);
                $imgs = pq($doc)->find('img');
                foreach ($imgs as $img) {
                    //<p><img class="aligncenter size-full wp-image-31669" src="" alt="大数据" width="640" height="480" srcset="http://www.36dsj.com/wp-content/uploads/2015/08/217.jpg 640w, http://www.36dsj.com/wp-content/uploads/2015/08/217-572x429.jpg 572w" sizes="(max-width: 640px) 100vw, 640px"></p>
                    $src = pq($img)->attr('src');

                    $localSrc = 'Uploads/Picture/Spider/' . md5($src) . '.jpg';
                    $stream = file_get_contents($src);
                    file_put_contents($localSrc, $stream);
                    //转换为完整路径，方便API调用
                    pq($img)->attr('src', ext_get_site_root_url(true, $localSrc));

                    //移除多余的属性，APP统一控制格式
                    pq($img)->removeAttr('class');
                    pq($img)->removeAttr('width');
                    pq($img)->removeAttr('height');
                }

                //输出处理后的html文档
                return $doc->htmlOuter();
            }),
        );
        $rang = "div.text";
        $ql = QueryList::Query($url, $rules, $rang);
        $data = $ql->getData(function ($item) {
            return $item;
        });

//            dump($data);

        if (empty($data)) {
            return null;
        } else {
            if (count($data) > 0) {
                $content_data = $data[0];

                return $content_data['title'] . $content_data['body'];
            }

            return null;
        }


    }


}
