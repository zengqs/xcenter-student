<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/5/20 0020
 * Time: 21:47
 */

namespace News\Spider;


use News\Model\NewsModel;
use Vendor\QL\QueryList;

/**
 * Class Dashuju36Com
 * 采集大数据36（36dsj.com）网站的新闻
 *
 * @package News\Spider
 */
class Dashuju36Com extends BaseSpider implements ISpider
{

    private $newsModel;

    function _initialize()
    {
        $this->newsModel = new NewsModel();
        $this->categoryId=1; //
    }

    public function run()
    {
        $url = "http://www.36dsj.com/archives/category/ganhuo/page/1";
        $this->parse_page($url);
    }

    //36dsj.com
    public function parse_page($url)
    {

        //采集规则
        $rules = array(
            "title"       => array("h2 a", "text"),
            "thumbnail"   => array("div.focus a span img.thumb", "data-original"),
            "url"         => array("h2 a", "href"),
            "description" => array("p.note", "text"),
            "date"        => array("p.info", "text"),
        );
        $rang = "div.content article.excerpt"; //采集范围
        $ql = QueryList::Query($url, $rules, $rang);

        $ql->getData(function ($item) {
            $news = $this->newsModel->where(array('source' => $item['url']))->find();

            //处理时间字段
            $tmp=explode(' ',trim($item['date']));
            $item['date']=$tmp[count($tmp)-1]; //
            if ($news) {
                $uid = is_login();
                $news['source'] = $item['url'];

                $news['title'] = $item['title'];
                $news['description'] = $item['description'];

                //临时方案，封面图片应该做本地存储
                $news['cover'] = 0; //图片未做本地存储

                $news['cover_url'] = $item['thumbnail'];
                $news['uid'] = $uid;
                $news['category'] =  $this->categoryId;//腾讯教育
                $news['dead_line'] = time() + 30 * 24 * 3600;

                $news['news_type'] = 0;//系统自动采集
                $news['create_time'] = strtotime($item['date']);
                $news['update_time'] = strtotime($item['date']);

                $detail = $this->getDetail($item['url']);
                $news['content'] = $news['content_mobile'] = $detail;

                //
                $news['source_tag'] = "大数据";
                $news['source_name'] = "36大数据";
                $news['source_channel'] = "干货教程";

                $this->newsModel->editData($news);


            } else {
                $news = array();
                $uid = is_login();
                $news['source'] = $item['url'];

                $news['title'] = $item['title'];
                $news['description'] = $item['description'];

                //临时方案，封面图片应该做本地存储
                $news['cover'] = 0; //图片未做本地存储

                $news['cover_url'] = $item['thumbnail'];
                $news['uid'] = $uid;
                $news['category'] = $this->categoryId;//默认分类
                $news['dead_line'] = time() + 30 * 24 * 3600;

                $news['news_type'] = 0;//系统自动采集
                $news['create_time'] = strtotime($item['date']);
                $news['update_time'] = strtotime($item['date']);

                $detail = $this->getDetail($item['url']);
                $news['content'] = $news['content_mobile'] = $detail;

                $news['source_tag'] = "大数据";
                $news['source_name'] = "36大数据";
                $news['source_channel'] = "干货教程";


                $this->newsModel->editData($news);
            }
            $news = null;

            return $item;
        });


        //采集下一页

        //采集规则
        $rules = array(
            "next_page" => array('div.pagination ul li.next-page a', "href"));
        $rang = "div.content"; //采集范围
        $ql = QueryList::Query($url, $rules, $rang);

        $ql->getData(function ($item) {
            //开始下一页的采集
            if (!empty($item['next_page']) && trim($item['next_page']) != "") {
                $next_page = $item['next_page'];
                $this->parse_page($next_page); //采集下一页
            }
        });
    }

    function getDetail($url)
    {

        $rules = array(
            'title' => array("div.text-title", "html"),
            'body'  => array('article.article-content', 'html'),
        );
        $rang = "div.content";
        $ql = QueryList::Query($url, $rules, $rang);
        $data = $ql->getData(function ($item) {
            return $item;
        });

        if (empty($data)) {
            return null;
        } else {
            if (count($data) > 0) {
                $content_data = $data[0];

                return $content_data['title'] . $content_data['body'];
            }

            return null;
        }
    }
}
