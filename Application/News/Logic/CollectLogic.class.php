<?php

namespace News\Logic {

    use News\Model\NewsModel;

    use News\Spider\Dashuju36Com;
    use News\Spider\Dashuju36Multi;
    use News\Spider\Eastmoney;
    use News\Spider\SohuNews;
    use Vendor\QL\QueryList;

    class CollectLogic
    {

        function collectAllNews()
        {
            //解决超时的问题
            ini_set('max_execution_time', '0');
            /*
            $spiders=[
                new SohuNews(),
                new Dashuju36Com()
            ];

            foreach ($spiders as $spider){
                $spider->run();
            }
            */

//            //FOR DEBUG
//            $spider=new SohuNews();
            $spider=new Eastmoney(2);
            $spider->run();

//            //FOR DEBUG
//            $spider=new Dashuju36Multi();
//            $spider->run();
        }


        public function collectQQEduNews()
        {

//        header("Content-type:text/html;charset=UTF-8");
//        <dl>
//<dt><a target="_blank" href="/a/20161128/021026.htm">2017年度国家公务员考试行测试题及答案解析（地市以下）</a></dt>
//<dd class="wz">2017年度国家公务员考试行测试题及答案解析（地市以下）</dd>
//<dd class="ky">2016-11-28 11:53:49</dd>
//</dl>
            //采集腾讯教育新闻的标题、链接和日期属性
            $url = "http://edu.qq.com/edunew/";
            $rules = array("title" => array("a", "text"), "url" => array("a", "href"), "abstract" => array("dd.wz", "text"), "date" => array("dd.ky", "text"));
            $rang = "#listZone dl";
            $ql = QueryList::Query($url, $rules, $rang, 'UTF-8', 'GB2312', true);

            $data = $ql->getData(function ($item) {
                //http://edu.qq.com/a/20161128/018534.htm
                $item['url'] = "http://edu.qq.com" . $item['url'];
                $newModel = new NewsModel();

                $news = $newModel->where(array('source' => $item['url']))->find();
                if ($news) {
                    $uid = is_login();
                    $news['source'] = $item['url'];
                    $news['title'] = $item['title'];
                    $news['description'] = $item['abstract'];
                    $news['cover'] = 17;
                    $news['uid'] = $uid;
                    $news['category'] = '202';//腾讯教育
                    $news['dead_line'] = time() + 30 * 24 * 3600;

                    $news['news_type'] = 0;//系统自动采集
                    $news['create_time'] = strtotime($item['date']);
                    $news['update_time'] = strtotime($item['date']);

                    $newModel->save($news);
                } else {

                    $uid = is_login();
                    $news['source'] = $item['url'];
                    $news['description'] = $item['abstract'];
                    $news['cover'] = 17;

                    $news['title'] = $item['title'];
                    $news['uid'] = $uid;
                    $news['category'] = '202';//腾讯教育
                    $news['dead_line'] = time() + 12 * 30 * 24 * 3600;

                    $news['news_type'] = 0;//系统自动采集
                    $news['create_time'] = strtotime($item['date']);
                    $news['update_time'] = strtotime($item['date']);
                    $newModel->add($news);
                }

                return $item;
            });

            return $data;
        }


        //采集腾讯教育新闻

        /**
         * 采集广州番禺职业技术学院校园网站的新闻
         */
        public function collectGzpypEduNews()
        {

            //多线程采集

//            header("Content-type:text/html;charset=UTF-8");

            //多线程扩展
            $cm = QueryList::run('Multi', [
                //待采集链接集合
                'list'    => [

                    "http://www.gzpyp.edu.cn/news/newsList.do?pageType=2&pageCol=1",//
                    "http://www.gzpyp.edu.cn/news/newsList.do?pageType=2&pageCol=1",
                    "http://www.gzpyp.edu.cn/news/newsList.do?pageType=3&pageCol=1",
                    "http://www.gzpyp.edu.cn/news/newsList.do?pageType=3&pageCol=2",
                    "http://www.gzpyp.edu.cn/news/newsList.do?pageType=3&pageCol=3",
                    "http://www.gzpyp.edu.cn/news/newsList.do?pageType=3&pageCol=4",
                    //更多的采集链接....
                ],
                'curl'    => [
                    'opt'       => array(
                        CURLOPT_SSL_VERIFYPEER => false,
                        CURLOPT_SSL_VERIFYHOST => false,
                        CURLOPT_FOLLOWLOCATION => true,
                        CURLOPT_AUTOREFERER    => true,
                    ),
                    //设置线程数
                    'maxThread' => 100,
                    //设置最大尝试数
                    'maxTry'    => 3,
                ],
                //不自动开始线程，默认自动开始
                'start'   => false,
                'success' => function ($a) {
                    //采集操作....
                    $rules = array("title" => array("a.link09", "text"), "url" => array("a.link09", "href"), "abstract" => array("a.link09", "text"), "date" => array("a.date2", "text"));
                    $rang = "#list li";
                    $ql = QueryList::Query($a['content'], $rules, $rang, 'UTF-8', 'UTF-8', true);

                    $data = $ql->getData(function ($item) {
                        $item['url'] = "http://www.gzpyp.edu.cn" . $item['url'];
                        $newModel = new NewsModel();

                        $news = $newModel->where(array('source' => $item['url']))->find();
                        if ($news) {
                            $uid = is_login();
                            $news['source'] = $item['url'];
                            $news['title'] = $item['title'];
                            $news['description'] = $item['abstract'];
                            $news['cover'] = 17;
                            $news['uid'] = $uid;
                            $news['category'] = '201';
                            $news['dead_line'] = time() + 30 * 24 * 3600;

                            $news['news_type'] = 0;//系统自动采集
                            $news['create_time'] = strtotime($item['date']);
                            $news['update_time'] = strtotime($item['date']);

                            $newModel->save($news);
                        } else {

                            $uid = is_login();
                            $news['source'] = $item['url'];
                            $news['description'] = $item['abstract'];
                            $news['cover'] = 17;

                            $news['title'] = $item['title'];
                            $news['uid'] = $uid;
                            $news['category'] = '201';
                            $news['dead_line'] = time() + 12 * 30 * 24 * 3600;

                            $news['news_type'] = 0;//系统自动采集
                            $news['create_time'] = strtotime($item['date']);
                            $news['update_time'] = strtotime($item['date']);
                            $newModel->add($news);
                        }

                        return $item;
                    });
                },
                'error'   => function () {
                    //出错处理
                },
            ]);


            //再额外添加一些采集链接
            $cm->add([
                "http://www.gzpyp.edu.cn/news/linksList.do?pageType=11&pageCol=2",//近期通知
                "http://www.gzpyp.edu.cn/news/linksList.do?pageType=11&pageCol=1",//时政要闻
            ], function ($a) {
                //采集操作....
                $rules = array("title" => array("a.link09", "text"), "url" => array("a.link09", "href"), "abstract" => array("a.link09", "text"), "date" => array("a.date2", "text"));
                $rang = "#list li";
                $ql = QueryList::Query($a['content'], $rules, $rang, 'UTF-8', 'UTF-8', true);

                $data = $ql->getData(function ($item) {
//                    $item['url'] = "http://www.gzpyp.edu.cn" . $item['url'];
                    $newModel = new NewsModel();

                    $news = $newModel->where(array('source' => $item['url']))->find();
                    if ($news) {
                        $uid = is_login();
                        $news['source'] = $item['url'];
                        $news['title'] = $item['title'];
                        $news['description'] = $item['abstract'];
                        $news['cover'] = 17;
                        $news['uid'] = $uid;
                        $news['category'] = '201';
                        $news['dead_line'] = time() + 30 * 24 * 3600;

                        $news['news_type'] = 0;//系统自动采集
                        $news['create_time'] = strtotime($item['date']);
                        $news['update_time'] = strtotime($item['date']);

                        $newModel->save($news);
                    } else {

                        $uid = is_login();
                        $news['source'] = $item['url'];
                        $news['description'] = $item['abstract'];
                        $news['cover'] = 17;

                        $news['title'] = $item['title'];
                        $news['uid'] = $uid;
                        $news['category'] = '201';
                        $news['dead_line'] = time() + 12 * 30 * 24 * 3600;

                        $news['news_type'] = 0;//系统自动采集
                        $news['create_time'] = strtotime($item['date']);
                        $news['update_time'] = strtotime($item['date']);
                        $newModel->add($news);
                    }

                    return $item;
                });
            },
                function () {
                    //error
                    //可选的，不同的出错处理
                });

            //开始采集
            $cm->start();
        }
    }
}
