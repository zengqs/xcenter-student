<?php

namespace Markdoc\Model;


use Think\Model;

class MarkdocModel extends Model{


    protected $_auto = array(
        array('status', '1', self::MODEL_INSERT),
    );

    // 数据表前缀
//    protected $tablePrefix = 'center_;
//    protected $tableName = 'markdoc'; //ocenter_markdoc
    // 数据表名（不包含表前缀）
//    protected $tableName = 'markdoc';
    // 实际数据表名（包含表前缀）
//    protected $trueTableName = 'ocenter_markdoc';

    public function editData($data)
    {
        if($data['id']){
            $data['update_time']=time();
            $res=$this->save($data);
        }else{
            $data['create_time']=$data['update_time']=time();
            $res=$this->add($data);
        }
        return $res;
    }

    public function getData($id){
        $data=$this->find($id);
        if ($data) {
            $this->_convertItem($data);
        }
        return $data;
    }

    public function getListByPage($map,$page=1,$order='sort asc,update_time desc',$field='*',$r=20)
    {
        $totalCount=$this->where($map)->count();
        if($totalCount){
            $list=$this->where($map)->page($page,$r)->order($order)->field($field)->select();
            foreach ($list as &$item) {
                //文档类型,0:html, 1:本地markdoc，2：数据库保存的markdoc
                $this->_convertItem($item);
            }
        }
        return array($list,$totalCount);
//        return array('list'=>$list,'count'=>$totalCount);
    }

    private function _convertItem(&$item){
        if ($item['doc_type']==1) {
            //文档类型,0:html, 1:本地markdoc，2：数据库保存的markdoc
            //只有本地文件保存的markdown才需要解析文件内容
            $file = APP_ROOT . $item['doc_root'] . DIRECTORY_SEPARATOR . $item['doc_filename'];
            $atLinkBase = ext_get_site_root_url(true, $item['doc_root']) . '/';
            $content = file_get_contents($file);
            $content = htmlentities($content);//编码HTML标签，以免错乱
            $item['content'] = $content;
            $item['atLinkBase'] = $atLinkBase; //支持@atlink标签，用于解析本地文件路径转换为网络路径
        }
        return $item;
    }

    public function getList($map,$field='*',$order='sort asc')
    {
//        $map=array('Title'=>'HTML');
        //select * from ocenter_markdoc where title='html order by sort asc'
        $lists = $this->where($map)->field($field)->order($order)->select();
        foreach ($lists as &$item) {
            //文档类型,0:html, 1:本地markdoc，2：数据库保存的markdoc
           $this->_convertItem($item);
        }
        return $lists;
    }
}
