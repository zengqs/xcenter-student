<?php

namespace Markdoc\Controller;

use Admin\Builder\AdminConfigBuilder;
use Admin\Builder\AdminListBuilder;
use Admin\Controller\AdminController;

//后台管理界面必须引入这个文件
require_once(APP_PATH . MODULE_NAME . '/Common/admin.php');


class MarkdocController extends AdminController
{

    protected $markdocModel;
    protected $markdocCategoryModel;

    function _initialize()
    {
        parent::_initialize();
        $this->markdocModel = D('Markdoc/Markdoc');
        $this->markdocCategoryModel = D('Markdoc/MarkdocCategory');
    }

    /**
     * 单页分类
     *
     * @author 郑钟良<zzl@ourstu.com>
     */
    public function markdocCategory()
    {
        //显示页面
        $builder = new AdminListBuilder();

        $list = $this->markdocCategoryModel->getCategoryList(array('status' => array('egt', 0)));

        $builder->title('文章Markdoc分类管理')
            ->suggest('删除分类时会将分类下的文章转移到默认分类(id为1)下')
            ->setStatusUrl(U('Markdoc/Markdoc/setCategoryStatus'))
            ->buttonNew(U('Markdoc/Markdoc/editCategory'))
            ->buttonEnable()->buttonDisable()->buttonDelete()
            ->keyId()
            ->keyText('title', '分类名')
            ->keyText('sort', '排序')
            ->keyStatus('status', '状态')
            ->keyDoActionEdit('Markdoc/Markdoc/editCategory?id=###')
            ->data($list)
            ->display();
    }

    /**分类编辑
     *
     * @param int $id
     *
     * @author 郑钟良<zzl@ourstu.com>
     */
    public function editCategory($id = 0)
    {
        $title = $id ? "编辑" : "新增";
        if (IS_POST) {
            if ($this->markdocCategoryModel->editData()) {
                $this->success($title . '成功。', U('Markdoc/Markdoc/markdocCategory'));
            } else {
                $this->error($title . '失败!' . $this->markdocCategoryModel->getError());
            }
        } else {
            $builder = new AdminConfigBuilder();

            if ($id != 0) {
                $data = $this->markdocCategoryModel->find($id);
            }
            $builder->title($title . '分类')
                ->data($data)
                ->keyId()->keyText('title', '标题')
                ->keyInteger('sort', '排序')->keyDefault('sort', 0)
                ->keyStatus()->keyDefault('status', 1)
                ->buttonSubmit(U('Markdoc/Markdoc/editCategory'))->buttonBack()
                ->display();
        }

    }

    /**
     * 设置文章分类状态：删除=-1，禁用=0，启用=1
     *
     * @param $ids
     * @param $status
     *
     * @author 郑钟良<zzl@ourstu.com>
     */
    public function setCategoryStatus($ids, $status)
    {
        !is_array($ids) && $ids = explode(',', $ids);
        if ($status == -1) {
            if (in_array(1, $ids)) {
                $this->error('id为 1 的分类是基础分类，不能被删除！');
            }
            $map['category'] = array('in', $ids);
            $this->markdocModel->where($map)->setField('category', 1);
        }
        $builder = new AdminListBuilder();
        $builder->doSetStatus('MarkdocCategory', $ids, $status);
    }

    //分类管理end

    /**
     * 单页配置
     *
     * @author 郑钟良<zzl@ourstu.com>\
     */
    public function config()
    {
        $builder = new AdminConfigBuilder();
        $data = $builder->handleConfig();

        $builder->title('文章基础设置')
            ->data($data);

        $builder->keyText('MARKDOC_CATEGORY_TITLE', '文章顶部标题')->keyDefault('MARKDOC_CATEGORY_TITLE', '关于')
            ->buttonSubmit()->buttonBack()
            ->display();
    }


    //文章文章列表start
    public function index($page = 1, $r = 20)
    {
        $aCate = I('cate', 0, 'intval');
        if ($aCate == -1) {
            $map['category'] = 0;
        } else if ($aCate != 0) {
            $map['category'] = $aCate;
        }
        $map['status'] = array('neq', -1);

        list($list, $totalCount) = $this->markdocModel->getListByPage($map, $page, 'sort asc,update_time desc', '*', $r);
        $category = $this->markdocCategoryModel->getCategoryList(array('status' => array('egt', 0)));
        $category = array_combine(array_column($category, 'id'), $category);
        foreach ($list as &$val) {
            if ($val['category']) {
                $val['category'] = '[' . $val['category'] . '] ' . $category[$val['category']]['title'];
            } else {
                $val['category'] = '未分类';
            }
        }
        unset($val);

        $optCategory = $category;
        foreach ($optCategory as &$val) {
            $val['value'] = $val['title'];
        }
        unset($val);

        $builder = new AdminListBuilder();
        $builder->title('文章列表')
            ->data($list)
            ->buttonNew(U('Markdoc/Markdoc/editMarkdoc'))
            ->setStatusUrl(U('Markdoc/Markdoc/setMarkdocStatus'))
            ->buttonEnable()->buttonDisable()->buttonDelete()
            ->setSelectPostUrl(U('Markdoc/Markdoc/index'))
            ->select('', 'cate', 'select', '', '', '', array_merge(array(array('id' => 0, 'value' => '全部')), $optCategory, array(array('id' => -1, 'value' => '未分类'))))
            ->keyId()->keyUid()->keyLink('title', '标题', 'Markdoc/Index/index?id=###')->keyText('category', '分类', '可选')->keyText('sort', '排序')
            ->keyStatus()->keyCreateTime()->keyUpdateTime()
            ->keyDoActionEdit('Markdoc/Markdoc/editMarkdoc?id=###')
            ->pagination($totalCount, $r)
            ->display();
    }

    public function setMarkdocStatus($ids, $status = 1)
    {
        !is_array($ids) && $ids = explode(',', $ids);
        $builder = new AdminListBuilder();
        $builder->doSetStatus('Markdoc', $ids, $status);
    }

    /**
     * 编辑单页文章
     *
     * @author 郑钟良<zzl@ourstu.com>
     */
    public function editMarkdoc()
    {
        $aId = I('id', 0, 'intval');
        $title = $aId ? "编辑" : "新增";
        if (IS_POST) {
            $aId && $data['id'] = $aId;
            $data['uid'] = I('post.uid', get_uid(), 'intval');
            $data['title'] = I('post.title', '', 'text');
            $data['content'] = I('post.content', '', 'html');
            $data['category'] = I('post.category', 0, 'intval');
            $data['sort'] = I('post.sort', 0, 'intval');
            $data['status'] = I('post.status', 1, 'intval');
            if (!mb_strlen($data['title'], 'utf-8')) {
                $this->error('标题不能为空！');
            }
            $result = $this->markdocModel->editData($data);
            if ($result) {
                $aId = $aId ? $aId : $result;
                $this->success($title . '成功！', U('Markdoc/editMarkdoc', array('id' => $aId)));
            } else {
                $this->error($title . '失败！', $this->markdocModel->getError());
            }
        } else {
            if ($aId) {
                $data = $this->markdocModel->find($aId);
            }
            $category = $this->markdocCategoryModel->getCategoryList(array('status' => array('egt', -1)));
            $options = array(0 => '无分类');
            foreach ($category as $val) {
                $options[$val['id']] = $val['title'];
            }
            $builder = new AdminConfigBuilder();
            $builder->title($title . '资讯')
                ->data($data)
                ->keyId()
                ->keyReadOnly('uid', '发布者')->keyDefault('uid', get_uid())
                ->keyText('title', '标题')
                ->keySelect('doc_type', '文档类型', '', array('1' => '本地文件保存的Markdoc', '2' => '数据库保存的Markdoc'))
                ->keyText('doc_root', 'Markdoc文件存放的目录')
                ->keyText('doc_filename', 'Markdoc文件存放的文件名')
                // ->keyEditor('content', '内容', '', 'all', array('width' => '850px', 'height' => '600px'))
                ->keyEditorMarkdown('content', '内容', '如果是本地文件，则不需要填写内容', 'mini', array('width' => '850px', 'height' => '600px'))
                ->keySelect('category', '分类', '', $options)
                ->keyInteger('sort', '排序')->keyDefault('sort', 0)
                ->keyStatus()->keyDefault('status', 1)
                ->buttonSubmit()->buttonBack()
                ->display();
        }
    }
}
