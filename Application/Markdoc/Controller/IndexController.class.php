<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 15-5-28
 * Time: 下午01:33
 *
 * @author 郑钟良<zzl@ourstu.com>
 */

namespace Markdoc\Controller;


use Think\Controller;

class IndexController extends Controller
{

    protected $paperModel;
    protected $paperCategoryModel;

    function _initialize()
    {
        $this->paperModel = D('Markdoc/Markdoc');
        $this->paperCategoryModel = D('Markdoc/MarkdocCategory');

//        $catTitle = modC('PAPER_CATEGORY_TITLE', '帮助手册', 'Markdoc');
//
//        $sub_menu['left'][] = array('tab' => 'home', 'title' => $catTitle, 'href' => U('index'));
//        $this->assign('sub_menu', $sub_menu);
//        $this->assign('current', 'home');
    }

    public function index_old()
    {
        $catList = $this->paperCategoryModel->getCategoryList(array('status' => 1));
        if (count($catList)) {
            $cat_ids = array_column($catList, 'id');
            $catList = array_combine($cat_ids, $catList);
            $map['category'] = array('in', array_merge($cat_ids, array(0)));
        } else {
            $map['category'] = 0;
            $catList = array();
        }
        $map['status'] = 1;
        $pageArtiles = $this->paperModel->getList($map, 'id,title,sort,category');
        foreach ($pageArtiles as $val) {
            $val['type'] = 'article';
            if ($val['category'] == 0) {
                $catList[] = $val;
            } else {
                $catList[$val['category']]['children'][] = $val;
            }
        }
        $catListSort = list_sort_by($catList, 'sort');
        $this->assign('cat_list', $catListSort);

        $aId = I('id', 0, 'intval');
        if ($aId == 0) {
            foreach ($catList as $val) {
                if ($val['type'] == 'article') {
                    $aId = $val['id'];
                    break;
                } else {
                    if ($val['children'][0]['id']) {
                        $aId = $val['children'][0]['id'];
                        break;
                    }
                }
            }
        }
        if ($aId) {
            $pageArtiles = array_combine(array_column($pageArtiles, 'id'), $pageArtiles);
            $contentTitle = $pageArtiles[$aId];
            $this->assign('content_title', $contentTitle);
            if ($pageArtiles[$aId]['category'] != 0) {
                $cate = $catList[$pageArtiles[$aId]['category']];
                $this->assign('cate', $cate);
                $this->assign('top_id', $cate['id']);
            } else {
                $this->assign('top_id', 0);
                $this->assign('id', $aId);
            }
        }

        $data = $this->paperModel->getData($aId);
        $this->assign('data', $data);
        $this->display();
    }

    /**
     * 获取目录列表
     */
    public function getIndexTreeData()
    {
        //getTree($id = 0, $field = true, $map = array('status' => array('gt', -1)))
        $catList = $this->paperCategoryModel->getTree(0, 'id,pid,title as name');

        foreach ($catList as &$item) {

            if (!array_key_exists('children', $item)) {
                //获取文章列表
                $item['type'] = 'category';
                $map['status'] = 1;
                $map['category'] = $item['id'];
                $pageArtiles = $this->paperModel->getList($map, 'id,title as name,category');
                foreach ($pageArtiles as &$article) {
                    $article['type'] = 'article';
                }
                $item['children'] = $pageArtiles;
            }
        }
        sdebug($catList, 'getIndexTreeData');
        $this->ajaxReturn($catList);
    }


    public function index()
    {
        $catList = $this->paperCategoryModel->getCategoryList(array('status' => 1));
        if (count($catList)) {
            $cat_ids = array_column($catList, 'id');
            $catList = array_combine($cat_ids, $catList);
            $map['category'] = array('in', array_merge($cat_ids, array(0)));
        } else {
            $map['category'] = 0;
            $catList = array();
        }
        $map['status'] = 1;
        $pageArtiles = $this->paperModel->getList($map, 'id,title,sort,category');
        foreach ($pageArtiles as $val) {
            $val['type'] = 'article';
            if ($val['category'] == 0) {
                $catList[] = $val;
            } else {
                $catList[$val['category']]['children'][] = $val;
            }
        }
        $catListSort = list_sort_by($catList, 'sort');
        $this->assign('cat_list', $catListSort);

        $aId = I('id', 0, 'intval');
        if ($aId == 0) {
            foreach ($catList as $val) {
                if ($val['type'] == 'article') {
                    $aId = $val['id'];
                    break;
                } else {
                    if ($val['children'][0]['id']) {
                        $aId = $val['children'][0]['id'];
                        break;
                    }
                }
            }
        }
        if ($aId) {
            $pageArtiles = array_combine(array_column($pageArtiles, 'id'), $pageArtiles);
            $contentTitle = $pageArtiles[$aId];
            $this->assign('content_title', $contentTitle);
            if ($pageArtiles[$aId]['category'] != 0) {
                $cate = $catList[$pageArtiles[$aId]['category']];
                $this->assign('cate', $cate);
                $this->assign('top_id', $cate['id']);
            } else {
                $this->assign('top_id', 0);
                $this->assign('id', $aId);
            }
        }

//        $data = $this->paperModel->getData($aId);
//        $this->assign('data', $data);
//        $this->display('markdown');
        $this->display('markdown_ztree');
    }


    //显示单页
    public function page($id = 1)
    {
        $data = $this->paperModel->getData($id);
        $this->assign('data', $data);
        $this->display('single_page');
    }

    public function ajaxGetMarkdocHtml()
    {
        $id = I('get.id','1','intval');
        $paper = M('markdoc')->find($id); //ocenter_markdoc
        if ($paper) {

            $file = APP_ROOT . $paper['doc_root'] . DIRECTORY_SEPARATOR . $paper['doc_filename'];
            $atLinkBase = ext_get_site_root_url(true, $paper['doc_root']) . '/';
            $content = file_get_contents($file);
            $content = htmlentities($content);
            $data = array(
                'atLinkBase' => $atLinkBase,
                'markdoc'    => $content,
            );

            sdebug($data,'ajaxGetMarkdocHtml',__FILE__,__LINE__);

            $this->ajaxReturn($data);

//            $file = APP_ROOT . 'Document/developer/dev_basic.md';
//            $atLinkBase = ext_get_site_root_url(true, '/Document/developer') . '/';
//            $content = file_get_contents($file);
//            $content = htmlentities($content);
//            $data = array(
//                'atLinkBase' => $atLinkBase,
//                'markdoc'    => $content,
//            );
//            $this->ajaxReturn($data);
        }
    }
}
