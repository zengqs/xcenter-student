<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2016/12/22
 * Time: 21:07
 */


/**
 * @param $name
 * @param $current
 *
 * @return string
 *
 * 显示顶部导航菜单是否为当前菜单
 */
function set_active_style($name,$current){
    if (is_null($current)){
        return '';
    }

    if (strtolower($name)==strtolower($current)){
        return 'active';
    }else{
        return '';
    }
}
