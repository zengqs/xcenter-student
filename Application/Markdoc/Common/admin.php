<?php
/**
 * 后台管理模块的通用函数，需要在后台管理模块中手动引入
 * require_once(APP_PATH . MODULE_NAME . '/Common/admin.php');
 *
 */
require_once(APP_PATH . 'Admin/Common/function.php');


/**
 * 获取当前时间戳，精确到毫秒
 *
 * @return float  等价于microtime(true)
 */
if (!function_exists('microtime_float')) {
    function microtime_float() //等价于microtime(true)
    {
        list($usec, $sec) = explode(" ", microtime());

        return ((float)$usec + (float)$sec);
    }
}


/**
 * 格式化时间戳，精确到毫秒，x代表毫秒
 * $micro_date = microtime(true);
 * $date= microtime_format('Y:m:d H:i:s.x', $micro_date);
 */
if (!function_exists('microtime_format')) {
    function microtime_format($tag, $time)
    {
        list($usec, $sec) = explode(".", $time);
        $date = date($tag, $usec);

        return str_replace('x', $sec, $date);
    }
}