DROP TABLE IF EXISTS `ocenter_markdoc`;
DROP TABLE IF EXISTS `ocenter_markdoc_category`;


-- -----------------------------
-- 表结构 `ocenter_markdoc`
-- -----------------------------
CREATE TABLE IF NOT EXISTS `ocenter_markdoc` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) NOT NULL,
  `title` varchar(25) NOT NULL,
  `description` TEXT DEFAULT NULL COMMENT '介绍',
  `cover` int(11) DEFAULT NULL COMMENT '封面图片的ID',
  `content` text NOT NULL COMMENT '文件的内容',
  `doc_type` INT(11) NOT NULL DEFAULT 0 COMMENT '文档类型,0:html, 1:本地markdoc，2：数据库保存的markdoc',
  `doc_root` varchar(500) NULL COMMENT 'Markdoc文件存放的目录',
  `doc_filename` varchar(500) NULL COMMENT 'Markdoc文件存放的文件名',
  `driver` varchar(500) NOT NULL DEFAULT 'local'COMMENT '文件上传驱动程序',
  `status` tinyint(2) NOT NULL DEFAULT 1,
  `sort` int(6) NOT NULL DEFAULT 0,
  `update_time` int(11) NOT NULL DEFAULT 0,
  `create_time` int(11) NOT NULL DEFAULT 0,
  `category` int(11) NOT NULL COMMENT '分类id',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='文章文章表';


-- -----------------------------
-- 表结构 `ocenter_markdoc_category`
-- -----------------------------
CREATE TABLE IF NOT EXISTS `ocenter_markdoc_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pid` int(11) DEFAULT NULL COMMENT '分类的父目录',
  `title` varchar(25) NOT NULL,
  `description` TEXT DEFAULT NULL COMMENT '目录介绍',
  `cover` int(11) DEFAULT NULL COMMENT '封面图片的ID',
  `sort` int(6) NOT NULL,
  `status` tinyint(2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='文章分类';

INSERT INTO `ocenter_markdoc_category` (id,title,sort,status,description) VALUES (1, '开发人员指南', 0, 1,'框架开发指南');
INSERT INTO `ocenter_markdoc_category` (id,title,sort,status,description) VALUES (2, '常见问题', 1, 1,'');
INSERT INTO `ocenter_markdoc_category` (id,title,sort,status,description) VALUES (3, 'HTML5', 2, 1,'包含HTML5的基本标签的介绍和常见技巧');
INSERT INTO `ocenter_markdoc_category` (id,title,sort,status,description) VALUES (4, 'jQuery', 2, 1,'介绍jQuery的入门知识');


INSERT INTO `ocenter_markdoc`(uid,title,content,`doc_type`,doc_root,doc_filename,status,sort,category) VALUES
  (1,'框架开发基础','',1,'Document/developer','basic.md',1,10,1),
  (1,'模块开发基础','',1,'Document/developer','module.md',1,20,1),
  (1,'插件开发基础','',1,'Document/developer','addon.md',1,30,1),
  (1,'模块前端开发基础','',1,'Document/developer','qianduan.md',1,30,1),
  (1,'模块后台管理程序开发基础','',1,'Document/developer','houduan.md',1,30,1),
  (1,'开发环境配置','',1,'Document/developer','dev_tool_opt.md',1,0,1),

  (1,'HTML标签','',1,'Document/jquery/example/html5','html5.md',1,0,3),
  (1,'使用Web workers处理线程','',1,'Document/jquery/example/html5','webworker.md',1,0,3),
  (1,'Server sent event API','',1,'Document/jquery/example/html5','sse.md',1,0,3),

  (1,'jQuery基础','',1,'Document/jquery/example/jquery','jquery.md',1,0,4),


  (1,'Markdown编辑器介绍','',1,'Document/markdown','readme.md',1,10,2),
  (1,'Markdown实例','',1,'Document/markdown','example.md',1,20,2);
