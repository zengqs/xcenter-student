delete from `ocenter_menu` where  `module`='Markdoc';
INSERT INTO `ocenter_menu` (`title`, `pid`, `sort`, `url`, `hide`, `tip`, `group`, `is_dev`, `icon`, `module`) VALUES
  ('马克文档', 0, 0, 'Markdoc/Markdoc/index', 1, '', '', 0, '', 'Markdoc');

SET @tmp_id = 0;
SELECT @tmp_id := id
FROM `ocenter_menu`
WHERE title = '马克文档' AND `module`='Markdoc';

INSERT INTO `ocenter_menu` (`title`, `pid`, `sort`, `url`, `hide`, `tip`, `group`, `is_dev`, `icon`, `module`) VALUES
('文章列表',@tmp_id, '0', 'Markdoc/Markdoc/index', '0', '', '文章管理', '0', '', 'Markdoc'),
('编辑单页文章',@tmp_id, '0', 'Markdoc/Markdoc/editMarkdoc', '1', '', '', '0', '', 'Markdoc'),
('设置单页文章状态',@tmp_id, '0', 'Markdoc/Markdoc/setMarkdocStatus', '1', '', '', '0', '', 'Markdoc'),
('单页配置',@tmp_id, '0', 'Markdoc/Markdoc/config', '0', '', '配置管理', '0', '', 'Markdoc'),
('单页分类',@tmp_id, '0', 'Markdoc/Markdoc/markdocCategory', '0', '', '配置管理', '0', '', 'Markdoc'),
('编辑分类', @tmp_id, '0', 'Markdoc/Markdoc/editCategory', '1', '', '', '0', '', 'Markdoc'),
('设置分类状态',@tmp_id, '0', 'Markdoc/Markdoc/setCategoryStatus', '1', '', '', '0', '', 'Markdoc');


DELETE FROM  `ocenter_auth_rule` WHERE `module`='Markdoc';
INSERT INTO `ocenter_auth_rule` ( `module`, `type`, `name`, `title`, `status`, `condition`) VALUES
  ( 'Markdoc', 1, 'Markdoc/Markdoc/setCategoryStatus', '设置分类状态', 1, ''),
  ( 'Markdoc', 1, 'Markdoc/Markdoc/editCategory', '编辑分类', 1, ''),
  ( 'Markdoc', 1, 'Markdoc/Markdoc/editMarkdoc', '文档编辑', 1, '');
