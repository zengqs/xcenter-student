<?php
/**
 * 所属项目 OpenSNS开源免费版.
 * 开发者: 陈一枭
 * 创建日期: 2015-03-27
 * 创建时间: 15:48
 * 版权所有 想天软件工作室(www.ourstu.com)
 */
namespace Markdoc\Widget;

use Think\Controller;

/**
 * Class HotCourseListWidget
 *
 * @package Edusoho\Widget
 *          热门课程列表
 */
class MenuWidget extends Controller
{
    public function render($current='')
    {
        //输出当前菜单选项
        $this->assign('current',$current);

        $this->assignData();
        $this->display(T('Application://Markdoc@Widget/menu'));
    }

    public function assignData()
    {
        $data=null;
        $this->assign('MenuContents', $data);
    }
}
