<?php
/**
 * 所属项目 OpenCollege.
 * 开发者: 曾青松
 * 创建日期: 2016-11-25
 * 创建时间: 23:01
 * 版权所有 广州番禺职业技术学院创新软件开发团队(www.zengqs.com)
 */
namespace Markdoc\Widget;

use Think\Controller;

//页脚
class FooterWidget extends Controller
{
    public function render()
    {
        $this->assignData();
        $this->display(T('Application://Markdoc@Widget/footer'));
    }

    public function assignData()
    {
        $data = null;
        $this->assign('FooterContents', $data);
    }
}
