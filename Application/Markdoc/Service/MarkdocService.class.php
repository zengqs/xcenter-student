<?php
namespace Markdoc\Service;


use Markdoc\Model\MarkdocCategoryModel;
use Markdoc\Model\MarkdocModel;

class MarkdocService
{
    public function getCategories()
    {
        $catModel = new MarkdocCategoryModel();
        $map = array();
        $map['status'] = 1;

        $list = $catModel->where($map)->field('id,title,description,cover,sort')->order('sort asc, id desc')->select();

        //数据转换
        foreach ($list as &$item) {
            $item['cover'] = get_image_by_id($item['cover']);
        }

        return $list;
    }

    public function getArticleList($category_id = 0, $page = 1)
    {
        $model = new MarkdocModel();
        $map['category'] = $category_id;
//        $list = $model->getListByPage($map, $page);
        list($list, $totalCount) =$model->getListByPage($map, $page);
        return array('list'=>$list,'count'=>$totalCount);
    }

    public function getArticle($id)
    {
        $model = new MarkdocModel();
        $data = $model->getData($id);

        return $data;
    }
}
