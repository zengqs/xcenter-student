项目说明
===

XCENTER是一个开原的快速应用开发框架，基于ThinkPHP3.2.3框架技术,以OpenSNS V3.3.0版本为基础进行二次开发，
并增加多个扩展模块。


# 安装说明

安装过程中如报错，可尝试检查文件目录是否齐全。如Conf、Uploads及其子目录、Runtime，
> 正常情况会自动创建，但有些环境下无权限创建。

# 升级说明
基于原始OpenSNS V3.3.0版本修改了安装脚本和Admin管理模块，
新增功能：用户模块可以支持多个后台管理管理控制器，方便业务功能的组织。
增加了基于Markdoc的文档模块，增加了系统开发文档。

# 安装指引
> 曾青松

## 目录权限（Linux）

> 以下目录确保具有读写权限，需要手动创建Runtime目录，并设置读写权限

~~~~
Uploads
Conf
Runtime
Data
~~~~

> 使用以下命令设置,注意子目录也要递归的设置读写权限

~~~~
chmod 777 -R Uploads
chmod 777 -R Conf
chmod 777 -R Runtime
chmod 777 -R Data
~~~~

如果通过FTP上传到服务器的根目录下，需要手动设置整个项目文件的读的权限,
例如安装在网站的根目录为`/home/zengqs/www`

~~~~
chmod +r -R /home/zengqs/www
~~~~


## 自动结算机器人程序

编写脚本（cron）
> 脚本已经写好，需要按照实际部署情况修改路径，并设置可执行权限

~~~~
sudo chmod +x /home/zengqs/www/dev/cron
sh /home/zengqs/www/dev/cron 执行自动结算任务
~~~~

脚本内容如下

~~~~shell
#!/bin/bash
echo "Cron task runing..."
php "/home/zengqs/www/dev/cli.php" Duobao/Crontask/index
~~~~



