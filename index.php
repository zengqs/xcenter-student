<?php
//曾青松 2016-8-13
//开启Socket log功能
//数据库脚本已经在驱动程序层面跟踪日志，只要是调试模式，会输出SQL语句
//安装服务端npm install -g socketlog-server , 运行命令 socketlog-server 即可启动服务。 将会在本地起一个websocket服务 ，监听端口是1229 。 如果想服务后台运行： socketlog-server > /dev/null & 我们提供公用的服务端，需要去申请client_id : http://slog.thinkphp.cn/
//如果你的服务器有防火墙，请开启1229和1116两个端口，这两个端口是socketlog要使用的。

define('SOCKET_LOG', true);//启用slog，sdebug函数,主要用于兼容代码，因为代码中如果引用了slog函数，但是没有包含slog.function.php文件会出错
include './slog.function.php';
slog(array(
    'enable' => true,//是否打印日志的开关
//    ws://119.147.44.180:1229
    'host' => 'localhost',//websocket服务器地址，默认localhost
    'port' => '1229',//websocket服务器端口，默认端口是1229
    'error_handler' => true,
    'optimize' => true,
    'show_included_files' => false,
    //调试微信和API必须设置 force_client_ids，否则浏览器收不到监听日志
    'force_client_ids' => array('zengqingsong'),//日志强制记录到配置的client_id,可以发送到多个用户,默认为空
    'allow_client_ids'=>array('zengqingsong') //限制允许读取日志的client_id，默认为空,表示所有人都可以获得日志。
), 'config');

/**
 * 缓存目录设置
 * 此目录必须可写，建议移动到非WEB目录
 */
define ('RUNTIME_PATH', './Runtime/');



function reset_session_path()
{
    $root = str_replace("\\", '/', dirname(__FILE__));
    $savePath = $root . "/Runtime/tmp/";
    if (!file_exists($savePath))
       @mkdir($savePath, 0777);
    session_save_path($savePath);
}

//reset_session_path();  //如果您的服务器无法安装或者无法登陆，又或者后台验证码无限错误，请尝试取消本行起始两条左斜杠，让本行代码生效，以修改session存储的路径


if (version_compare(PHP_VERSION, '5.4.0', '<')) die('require PHP > 5.5.0 !');

/*移除magic_quotes_gpc参数影响*/
if (get_magic_quotes_gpc()) {
    function stripslashes_deep($value)
    {
        $value = is_array($value) ? array_map('stripslashes_deep', $value) : stripslashes($value);
        return $value;
    }

    $_POST = array_map('stripslashes_deep', $_POST);
    $_GET = array_map('stripslashes_deep', $_GET);
    $_COOKIE = array_map('stripslashes_deep', $_COOKIE);
    $_REQUEST = array_map('stripslashes_deep', $_REQUEST);
}
/*移除magic_quotes_gpc参数影响end*/


/**
 * mp.weixin.qq.com，开发者中心，服务器配置，Token(令牌)
 */
define('APP_TOKEN', 'weixin');

/**
 * 系统调试设置
 * 项目正式部署后请设置为false
 */
define ('APP_DEBUG', true);

define ('APP_ID', 1);
/**
 * 应用目录设置
 * 安全期间，建议安装调试完成后移动到非WEB目录
 */
//define('APP_ROOT', str_replace("\\", '/', dirname(__FILE__)) . '/'); //增加，内部修配合PHPUnit进行单元测试
define('APP_ROOT', dirname(__FILE__) . DIRECTORY_SEPARATOR); //增加，内部修配合PHPUnit进行单元测试
define('APP_PATH', APP_ROOT . 'Application/'); //应用安装的目录


/**
 *  主题目录 OpenSNS模板地址 （与ThinkPHP中的THEME_PATH不同）
 *  @author 郑钟良<zzl@ourstu.com>
 */
define ('OS_THEME_PATH', './Theme/');

if (!is_file( 'Conf/user.php')) {
    header('Location: ./install.php');
    exit;
}


/**
 * 引入核心入口
 * ThinkPHP亦可移动到WEB以外的目录
 */

try{
    require './ThinkPHP/ThinkPHP.php';
}catch (\Exception $exception){
    if($exception->getCode()==815){
        send_http_status(404);
        $string=file_get_contents('./Public/404/404.html');
        $string=str_replace('$ERROR_MESSAGE',$exception->getMessage(),$string);
        $string=str_replace('HTTP_HOST','http://'.$_SERVER['HTTP_HOST'],$string);
        echo $string;
    }else{
        E($exception->getMessage(),$exception->getCode());
    }
}
