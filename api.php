<?php
/**
 * Created by PhpStorm.
 * User: caipeichao
 * Date: 1/14/14
 * Time: 8:12 PM
 */


//曾青松 2016-8-13
//开启Socket log功能
//数据库脚本已经在驱动程序层面跟踪日志，只要是调试模式，会输出SQL语句
//安装服务端npm install -g socketlog-server , 运行命令 socketlog-server 即可启动服务。 将会在本地起一个websocket服务 ，监听端口是1229 。 如果想服务后台运行： socketlog-server > /dev/null & 我们提供公用的服务端，需要去申请client_id : http://slog.thinkphp.cn/
//如果你的服务器有防火墙，请开启1229和1116两个端口，这两个端口是socketlog要使用的。

define('SOCKET_LOG', true);//启用slog，sdebug函数,主要用于兼容代码，因为代码中如果引用了slog函数，但是没有包含slog.function.php文件会出错
include './slog.function.php';
slog(array(
    'enable' => true,//是否打印日志的开关
//    ws://119.147.44.180:1229
    'host' => 'localhost',//websocket服务器地址，默认localhost
    'port' => '1229',//websocket服务器端口，默认端口是1229
    'error_handler' => true,
    'optimize' => true,
    'show_included_files' => false,
    //调试微信和API必须设置 force_client_ids，否则浏览器收不到监听日志
    'force_client_ids' => array('zengqingsong'),//日志强制记录到配置的client_id,可以发送到多个用户,默认为空
    'allow_client_ids'=>array('zengqingsong') //限制允许读取日志的client_id，默认为空,表示所有人都可以获得日志。
), 'config');



/**
 * 调试开关
 * 项目正式部署后请设置为false
 */
define ( 'APP_DEBUG', true );

////从URL获取SESSION编号
//ini_set("session.use_cookies",0);
//ini_set("session.use_trans_sid",1);
//if($_REQUEST['session_id']) {
//    session_id($_REQUEST['session_id']);
//    session_start();
//}

//调用Application/Api应用
// 绑定访问Admin模块
define('BIND_MODULE','Api');
//define('APP_ROOT', str_replace("\\", '/', dirname(__FILE__)) . '/'); //增加，内部修配合PHPUnit进行单元测试
define('APP_ROOT', dirname(__FILE__) . DIRECTORY_SEPARATOR); //增加，内部修配合PHPUnit进行单元测试
define('APP_PATH', APP_ROOT . 'Application/'); //应用安装的目录


/**
 *  主题目录 OpenSNS模板地址 （与ThinkPHP中的THEME_PATH不同）
 *  @author 郑钟良<zzl@ourstu.com>
 */
define ('OS_THEME_PATH', './Theme/');

define ( 'RUNTIME_PATH', './Runtime/' );
require './ThinkPHP/ThinkPHP.php';
